<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSocialUserTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Mặc định bằng 1 chưa check, = 2 đã check, =0 không tồn tại

        Schema::table('social_user', function(Blueprint $table){
            $table->integer('verify')->default(1)->after('facebook_id');
            $table->text('fb_data')->after('facebook_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('social_user', function(Blueprint $table){
            $table->dropColumn(['verify']);
            $table->dropColumn(['fb_data']);
        });
    }
}
