<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaiDuThiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('post_id', 50);
            $table->string('title', 240);
            $table->text('bref_description');
            $table->longText('content');
            $table->string('username', 50);
            $table->string('email', 50);
            $table->string('image1');
            $table->string('image2');
            $table->string('image3');
            $table->string('link_video');
            $table->integer('like')->default(0);
            $table->integer('share')->default(0);
            $table->integer('vote')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('posts');
    }
}
