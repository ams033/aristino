<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function(Blueprint $table){
            $table->string('image')->after('email');
            $table->date('birthday_at')->after('image')->nullable();
            $table->tinyInteger('gender')->after('birthday_at')->default(0);
            $table->string('address')->after('gender')->default('');
            $table->string('phone')->after('address')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['image', 'birthday_at', 'gender', 'address', 'phone']);
        });
    }
}
