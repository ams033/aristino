<?php

use Illuminate\Database\Seeder;

class UserFirstTableSeeder extends Seeder
{
    public function run()
    {
        $username = env('USERNAME_DEMO', 'AMS');
        $pass = env('PASSWORD_DEMO', 'ams123789');
        $email = env('EMAIL_DEMO', 'demo@ams.net.vn');

        $check = DB::table('users')->select('email')->where('email', $email)->first();
        if ($check == null) {
            DB::table('users')->insert([
                'name' => $username,
                'email' => $email,
                'password' => bcrypt($pass),
                'image' => '',
                'birthday_at' => null,
                'gender' => 0,
                'address' => '',
                'phone' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            $user_id = DB::getPdo()->lastInsertId();

            DB::table('roles')->insert([
                'name' => 'admin',
                'display_name' => 'Admin',
                'description' => 'Nhóm người dùng có quyền quản trị cao nhất hệ thống',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            $roles_id = DB::getPdo()->lastInsertId();

            DB::table('role_user')->insert([
                'user_id' => $user_id,
                'role_id' => $roles_id
            ]);
        }
    }
}
