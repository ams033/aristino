<?php
/**
 * @todo trả về các danh sách users nhận được thông báo
 * @return mixed
 */
function get_user_receive_notification(){
    $userId = [1];
    return \App\User::find($userId);
}

/**
 * @todo Gửi thông báo
 * @param array $notification
 */
function send_notification($notification = []){
    \Illuminate\Support\Facades\Notification::send(get_user_receive_notification(), new \App\Notifications\NotificationSystem($notification));
}