<?php
/**
 * @todo: active url
 * @param $path
 * @return string
 */
function set_active($path){
    return Request::is($path . '*') ? 'active' :  '';
}

/**
 * @todo: get current url
 * @return mixed
 */
function get_current_url(){
    return str_replace(".php","",basename($_SERVER['SCRIPT_NAME']));
}

/**
 * @todo: convert a string to a number
 * @param $str
 * @return float
 */
function string_to_number($str){
    return floatval(str_replace(",", ".", str_replace(".", "", $str)));
}

function sort_2array_desc($top,$key="value"){
    $size   =   sizeof($top);
    for($i=0;$i<$size-1;$i++){
        for($j=$i+1;$j<$size;$j++){

            if($top[$i][$key] < $top[$j][$key]){

                $cache      =   $top[$i];
                $top[$i]    =   $top[$j];
                $top[$j]    =   $cache;

            }
        }
    }
    return $top;
}

function sort_2array_asc($top,$key="value"){
    $size   =   sizeof($top);
    for($i=0;$i<$size-1;$i++){
        for($j=$i+1;$j<$size;$j++){

            if($top[$i][$key] > $top[$j][$key]){

                $cache      =   $top[$i];
                $top[$i]    =   $top[$j];
                $top[$j]    =   $cache;

            }
        }
    }

    return $top;

}

function has_empty_element($array){
    $check = false;
    for ($i = 0; $i < count($array); $i++) {
        if ($array[$i] == '') {
            $check = true;
            break;
        }
    }
    return $check;
}

function get_post_vote($id){
    return App\Post::select('vote')->where('id', $id)->first()->vote;
}

function curlfb($url)
{
    $ch = curl_init();
    $timeout = 15;
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
    $data = curl_exec($ch);

    if(!curl_errno($ch)){
        return json_decode($data,true);
    }else{
        return array('error'=>'curl');
    }

    curl_close($ch);
}