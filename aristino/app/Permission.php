<?php

namespace App;

use Laratrust\LaratrustPermission;

class Permission extends LaratrustPermission
{
    /**
     * Khai báo quan hệ với user
     *
     * @return mixed
     */
    public function users(){
        return $this->belongsToMany('App\User', 'user_permission', 'permission_id', 'user_id');
    }
}
