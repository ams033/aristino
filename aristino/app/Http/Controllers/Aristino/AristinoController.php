<?php

namespace App\Http\Controllers\Aristino;

use App\Post;
use App\SocialUser;
use App\UserVote;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Laravel\Socialite\Facades\Socialite;

class AristinoController extends Controller
{
    private $data_on_page   = 10;



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_list_post(){
        $posts  = Post::select('id', 'post_id', 'title', 'username', 'email', 'like', 'vote', 'fb_user_id')->paginate($this->data_on_page);

        if(isset($_GET['orderVote'])){
            $posts  = Post::select('id', 'post_id', 'title', 'username', 'email', 'like', 'vote', 'fb_user_id')->orderBy('vote', 'DESC')->paginate($this->data_on_page);
        }

        if(isset($_GET['orderSocial'])){
            $posts  = Post::select('id', 'post_id', 'title', 'username', 'email', 'like', 'vote', 'fb_user_id')->orderBy('like', 'DESC')->paginate($this->data_on_page);
        }

        return view('admin.aristino.post.list', compact('posts'));
    }

    public function getParticipantList(){
        $participants = SocialUser::select('facebook_id', 'name', 'email', 'phone', 'birthday', 'cmnd')->paginate(30);
        return view('admin.aristino.participant.list', compact('participants'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function hien_thi_trang_chu(){
        return view('aristino.pages.home');
    }

    /**
     * @param $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function hien_thi_trang_dang_ky($userId){

        $login_facebook_token   =   session('login_facebook_token');
        if(isset($login_facebook_token) ) {

            $Curl_Session = 'https://graph.facebook.com/v2.2/me?fields=name,picture&access_token=' . $login_facebook_token;
            $user_fb = curlfb($Curl_Session);

            if (isset($user_fb['id']) AND $user_fb['id'] == $userId) {

                $user   = SocialUser::where('facebook_id',$userId)->first();

                return view('aristino.pages.dangky', compact('user'));
            }
            else if (isset($user_fb['id'])){

                return redirect()->to('aristino/dang-ky/' . $user_fb['id']);


            }

        }

        return redirect()->to('aristino/danh-sach-bai-thi');

    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dang_ky(Request $request, $id){

        $login_facebook_token   =   session('login_facebook_token');
        if(isset($login_facebook_token) ) {

            $Curl_Session = 'https://graph.facebook.com/v2.2/me?fields=name,picture&access_token=' . $login_facebook_token;
            $user_fb = curlfb($Curl_Session);

            if (isset($user_fb['id']) AND  $user_fb['id']==$id) {

                $birthday = $request->input('txtYear') . '-' . $request->input('txtMonth') . '-' . $request->input('txtDate');

                DB::table('social_user')->where('facebook_id', $id)->update([
                    'name' => $request->input('txtUserName'),
                    'email' => $request->input('txtEmail'),
                    'phone' => $request->input('txtPhone'),
                    'birthday' => date('Y-m-d', strtotime($birthday)),
                    'cmnd' => $request->input('txtCmnd'),
                ]);
                $user = SocialUser::where('facebook_id', $id)->first();

                $count_post = Post::where('fb_user_id', $id)->count();

                if ($count_post > 0) {
                    $post = Post::where('fb_user_id', $id)->first();
                    return redirect()->to('aristino/bai-du-thi/' . $post['id']);
                } else {
                    return view('aristino.pages.thamgia', compact('user'));
                }

            }
            else if (isset($user_fb['id'])){

                return redirect()->to('aristino/dang-ky/' . $user_fb['id']);


            }
            else {
                return redirect()->to('aristino/danh-sach-bai-thi');
            }
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function hien_thi_trang_danh_sach_bai_thi(){
        $postTop    = Post::select('id', 'post_id', 'title', 'image2','image3', 'username', 'vote', 'like', 'share', 'bref_description')->where('status',1)->orderBy('vote', 'DESC')->orderBy('like', 'DESC')->limit(10)->get()->toArray();

        $posts      = Post::select('id', 'post_id', 'title', 'image2','image3', 'username', 'vote', 'like', 'share', 'bref_description')->where('status',1)->orderBy('id', 'DESC')->paginate($this->data_on_page);

        $exposts      = Post::select('id', 'post_id', 'title', 'image2','image3', 'username', 'vote', 'like', 'share', 'bref_description')->where('status',2)->orderBy('id', 'DESC')->get()->toArray();


        $totalPost  = Post::where('status',1)->count();




        return view('aristino.pages.danhsachbaithi', compact('postTop', 'totalPost','posts','exposts'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function hien_thi_trang_tham_gia(){
        return view('aristino.pages.thamgia');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function xu_ly_tham_gia(Request $request){

        $post           = new Post();
        $post->post_id  = '';
        $post->fb_user_id  = $request->input('facebook_user_id');
        $post->title    = $request->input('txtTitle');
        $post->bref_description = $request->input('txtFeelShort');
        $post->content  = $request->input('txtPost');
        $post->image1   = $request->input('image1');
        $post->image2   = $request->input('image2');
        $post->image3   = $request->input('image3');
        $post->link_video   = $request->input('video');
        $post->username = $request->input('username');
        $post->email    = $request->input('email');
        $post->save();
        return redirect()->to('aristino/bai-du-thi/'. $post->id);

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function hien_thi_trang_the_le(){
        return view('aristino.pages.thele');
    }



    public function check_social_user($postId=0)
    {

        if($postId>0){


            $users      = SocialUser::select('social_user.id', 'social_user.facebook_id','social_user.token')
                ->leftJoin('user_vote', 'social_user.facebook_id', '=', 'user_vote.social_user_id')
                ->where('user_vote.post_vote_id',$postId)
                ->where('social_user.verify',1)->whereDate('social_user.created_at','<=',date("Y-m-d H:i:s",time()-60*60*48))->orderBy('social_user.id', 'DESC')->paginate(50);

        }
        else {

            $users      = SocialUser::select('social_user.id', 'social_user.facebook_id','social_user.token')
                ->where('social_user.verify',1)->whereDate('social_user.created_at','<=',date("Y-m-d H:i:s",time()-60*60*48))->orderBy('social_user.id', 'DESC')->paginate(50);
        }


        $token      = env('FACEBOOK_USER_TOKEN');

        foreach($users as $user)
        {

            $Curl_Session   =   'https://graph.facebook.com/v2.2/'. urlencode($user['facebook_id'])."?fields=likes,email,id,name,birthday&access_token=".$user['token'];
            $fb             =   curlfb($Curl_Session);
            $update_social    =   SocialUser::find($user['id']);

            if(isset( $fb['id']))
            {
                $update_social->verify    =   2;
                $update_social->fb_data   =   json_encode($fb);

                $token                    =   $user['token'];
                $update_social->save();
            }
            else if(isset( $fb['error']['code']) )
            {

                if( $fb['error']['code']==190)
                {
                    $Curl_Session   =   'https://graph.facebook.com/v2.2/'. urlencode($user['facebook_id'])."?fields=likes,email,id,name,birthday&access_token=".$token;
                    $fb             =   curlfb($Curl_Session);
                }

                $update_social      =   SocialUser::find($user['id']);


                if(isset( $fb['error']['code']) AND $fb['error']['code']==100)
                {
                    $update_social->verify    =   0;
                    $update_social->fb_data   =   json_encode($fb['error']);
                    $update_social->save();

                    UserVote::where('social_user_id', $user['facebook_id'])->update(['status' => 0]);

                }
                else  if(isset( $fb['error']['code']) ){
                     $update_social->verify    =  $fb['error']['code'];
                     $update_social->fb_data   =  json_encode($fb['error']);
                     $update_social->save();

                }
            }



        }


    }

    public function update_social()
    {
        $posts   = Post::select('id', 'like')->orderBy('id', 'DESC')->paginate(30);

        foreach($posts as $post){

            $url    =  route('aristino.hien_thi_trang_bai_du_thi',$post['id']);
            $like   =  $this->syn_facebook_share($url);

            if($like > $post['like']){
                $update_post   =   Post::find($post['id']);
                $update_post->like  = $like;
                $update_post->save();
            }
        }
    }
    
    public function syn_facebook_share($url,$token="")
    {
        $token  =   $token==""?env('FACEBOOK_USER_TOKEN'):$token;

        $Curl_Session = 'https://graph.facebook.com/v2.2/?id='. urlencode($url)."&fields=og_object{engagement}&access_token=".$token;
        $ret    =   curlfb($Curl_Session);

        if(isset($ret['og_object']['engagement']['count']))
            return $ret['og_object']['engagement']['count'];
        else
            return 0;
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function hien_thi_trang_bai_du_thi($id){
        $checkexit  =   Post::where("id",$id)->count();
        if($id>0 AND $checkexit==1){

            $post       =   Post::find($id);
            $voted      =   0;
            $user_fb    =   array();

            $pcount = UserVote::where('post_vote_id', $id)->where('status', 1)->count();
            $login_facebook_token   =   session('login_facebook_token');

            if(isset($login_facebook_token) ){

                $Curl_Session = 'https://graph.facebook.com/v2.2/me?fields=name,picture&access_token='.$login_facebook_token;
                $user_fb =   curlfb($Curl_Session);

                if(isset($user_fb['id']) ){
                    $user_vote = UserVote::where('social_user_id',$user_fb['id'])->where('post_vote_id', $id)->first();
                    if ($user_vote) {
                        $voted      =   1;
                    }
                }

                $like   = $this->syn_facebook_share(route('aristino.hien_thi_trang_bai_du_thi',$id),$login_facebook_token);

                if($like > $post['like']){
                    $update_post   =   Post::find($id);
                    $update_post->like  = $like;
                    $update_post->save();
                    $post       =   Post::find($id);
                }

            }
            if($pcount != $post['vote'])
            {
                $update_post   =   Post::find($id);
                $update_post->vote  = $pcount;
                $update_post->save();
                $post           =   Post::find($id);

            }






            return view('aristino.pages.baiduthi',compact('post','voted','user_fb','pcount'));
        }
        else {
            return redirect()->to('aristino/danh-sach-bai-thi');
        }

    }

    /**
     * @todo Tìm kiếm
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request){
        $key    = trim($request->input('search'));
        $posts     = array();
        $postTop   = Post::select('id', 'post_id', 'title', 'image2','image3', 'username', 'vote', 'like', 'share', 'bref_description')->where('status',1)->orderBy('vote', 'DESC')->orderBy('like', 'DESC')->limit(10)->get()->toArray();

        if($key !== ''){    //tìm kiếm tên, tiêu đề bài viết

            $postSearch = Post::select('id', 'post_id', 'title', 'image2','image3', 'username', 'vote', 'like', 'share', 'bref_description')->where('status',1)
                ->where(function ($query) use ($key) {
                    $query->where('title', 'LIKE', '%'.$key.'%')
                        ->orWhere('username', 'LIKE','%'.$key.'%');

                })->orderBy('id', 'DESC')->paginate($this->data_on_page);

            $totalPost  = Post::where('status',1)
                ->where(function ($query) use ($key) {
                    $query->where('title', 'LIKE', '%'.$key.'%')
                        ->orWhere('username', 'LIKE','%'.$key.'%');

            })->count();
        }
        return view('aristino.pages.danhsachbaithi', compact('postTop', 'postSearch', 'totalPost', 'key','posts'));
    }

    public function delete($id){
        Post::destroy($id);
        return redirect()->route('admin.aristino.post.getList')->with(['flash_message_danger' => 'Xóa bài dự thi thành công!']);
    }

    /**
     * @todo Hiển thị trang sửa nội dung
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id){
        $post   = Post::find($id);
        return view('admin.aristino.post.edit', compact('post'));
    }

    /**
     * @todo Thực hiện sửa nội dung bài dự thi
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(Request $request, $id){
        $post                   = Post::find($id);


        $post->title            = trim($request->txtTitle);
        $post->bref_description = trim($request->txtDescription);
        $post->content          = trim($request->txtContent);
        $post->save();
        return redirect()->route('admin.aristino.post.getList')->with(['flash_message' => 'Cập nhật bài dự thi thành công!']);
    }
}
