<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Post;
use App\SocialUser;
use App\UserVote;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Laravel\Socialite\Facades\Socialite;
use League\Flysystem\Exception;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */

    public function redirectToProvider()
    {


        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */

    public function handleProviderCallback()
    {

        //Kiểm tra lỗi khi từ chối không cấp quyền truy cập Facebook
        if (!isset($_GET['error']))
        {
            $socialUser = Socialite::driver('facebook')->stateless()->user();
            //
            $email = $socialUser->getEmail() ? $socialUser->getEmail() : "";
            $fb_user_id = $socialUser->getId() ? $socialUser->getId() : "";
            $user = SocialUser::where('facebook_id', $fb_user_id)->first();


            session(['login_facebook_token' => $socialUser->token]);



            //@todo Xem userid đã có trong bảng social_user chưa?
            //@todo Nếu chưa có => insert into table social_user
            if (!$user) {
                SocialUser::create([
                    'facebook_id' => $socialUser->getId(),
                    'name' => $socialUser->getName(),
                    'email' => $email,
                    'avarta' => $socialUser->getAvatar(),
                    'token' => $socialUser->token,
                    'refreshToken' => ($socialUser->refreshToken !== null) ? $socialUser->refreshToken : ""
                ]);
                $user = SocialUser::where('facebook_id', $fb_user_id)->first();
            }

            $ppid = $_GET['ppid'];



            if ($ppid !== "0") {

                $user_vote = UserVote::where('social_user_id', $user->facebook_id)->where('post_vote_id', $ppid)->first();

                if( time() <= strtotime(env('TIME_END'))+60*60*24-1 )
                {

                    if (!$user_vote ) {
                        UserVote::create([
                            'social_user_id' => $user->facebook_id,
                            'post_vote_id' => $ppid,
                            'remote_ip'=>$_SERVER['REMOTE_ADDR']
                        ]);
                    }


                    $pcount = UserVote::where('post_vote_id', $ppid)->where('status', 1)->count();

                    if ($pcount > 0) {
                        Post::where('id', $ppid)->update([
                            'vote' => $pcount
                        ]);
                    }
                }

                return redirect()->route('aristino.hien_thi_trang_bai_du_thi', $ppid);
            }


            return redirect()->to('/aristino/dang-ky/' . $user['facebook_id']);

        }
        else {

            $ppid = isset($_GET['ppid'])?$_GET['ppid']*1:0;

            if($ppid>0)
                return redirect()->route('aristino.hien_thi_trang_bai_du_thi', $ppid);
            else
                return redirect()->to('/aristino/trang-chu/');
        }
        //@todo Kiem tra thang user da vote cho bai co id = $ppid chua trong table user_vote?
        //@todo Neu chua thi insert vao table user_vote: ppid va userid
//        $ppid = $_GET['ppid'];
//
//        if($ppid == '0')
//        {
//            return url('http://betheman.aristino.com/aristino/tham-gia?autoclick=1');
//        }

//        $user_vote = UserVote::where('social_user_id', $user->facebook_id)->where('image_vote_id', $ppid)->first();
//        if(!$user_vote){
//            UserVote::create([
//                'social_user_id' => intval($user->facebook_id),
//                'image_vote_id' => intval($ppid)
//            ]);
//        }
//
//        $pcount = UserVote::where('image_vote_id', $ppid)->count();
//        if ($pcount > 0){
//            Image_vote::where('id', $ppid)->update([
//                'vote' => $pcount
//            ]);
//        }
//
//        return redirect()->to('/' . $ppid);
    }
}
