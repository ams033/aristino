<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\SocialUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //
    public function __construct()
    {

    }

    public function index(){
        $total_post = $this->get_count_post();
        $total_user_participation = $this->get_user_participation();
        $postVote   = $this->get_post_vote();
        $user_recent    = $this->get_user_recent();
        return view('admin.pages.dashboard', compact('total_post', 'total_user_participation', 'postVote', 'user_recent'));
    }

    public function get_count_post(){
        return Post::count();
    }

    public function get_user_participation(){
        return SocialUser::count();
    }

    public function get_post_vote(){
        return  Post::select('title', 'vote', 'like', 'fb_user_id','username', 'id')->limit(30)->orderBy('vote', 'DESC')->get();
    }

    public function get_user_recent(){
        return SocialUser::select('facebook_id', 'name', 'email')->limit(30)->orderBy('created_at', 'DESC')->get();
    }
}
