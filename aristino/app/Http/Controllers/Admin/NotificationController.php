<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    /**
     * NotificationController constructor.
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getList($userId){
        $user = User::find($userId);
        $notifications = $user->notifications()->paginate(10);
        return view('admin.notification.list', compact('notifications'));
    }

    /**
     * @param $userId
     * @param $notificationId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function oneAsRead($userId, $notificationId){
        $user = User::find($userId);
        $user->notifications()->where('id',$notificationId)->update(['read_at' => \Carbon\Carbon::now()]);
        return redirect($user->notifications()->where('id',$notificationId)->first()->data["callback"]);
    }

    /**
     * @param $userId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function allAsRead($userId){
        $user = User::find($userId);
        $user->unreadNotifications()->update(['read_at' => \Carbon\Carbon::now()]);
        return back();
    }
}
