<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Requests\RoleRequest;
use App\Http\Requests\PermissionRequest;
use App\User;
use App\Role;
use App\Permission;
use Auth;
use DB;
use Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Session;
use Illuminate\Support\Facades\Input;

class UserController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * @todo Hiển thị danh sách người dùng
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getList(){
        $users = User::select('id', 'name', 'email')->orderBy('name', 'ASC')->paginate(15);
        return view('admin.users.list', compact('users'));
    }

    /**
     * @todo Hiển thị trang thêm người dùng
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAdd(){
        $roles = Role::select('id', 'name')->orderBy('id', 'DESC')->get()->toArray();
        return view('admin.users.add', compact('roles'));
    }

    /**
     * @todo: Thêm user
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAdd(UserRequest $request){
        $user = new User();
        $user->name = trim($request->txtUser);
        $user->password = Hash::make($request->txtPass);
        $user->email = trim($request->txtMail);
        $user->remember_token = $request->_token;
        $user->birthday_at = (isset($request->txtBirthday) AND ($request->txtBirthday != null)) ? date("Y-m-d", strtotime(str_replace("/", "-", $request->txtBirthday))) : null;
        $user->gender = (isset($request->gender) AND ($request->gender != null)) ? $request->gender : 0;
        $user->address = (isset($request->txtAddress) AND ($request->txtAddress != null)) ? trim($request->txtAddress) : "";
        $user->phone = (isset($request->txtPhone) AND ($request->txtPhone)) ? trim($request->txtPhone) : '';
        if (Input::hasFile('avarta')) { //@todo: upload image user
            $image = Input::file('avarta');
            $filename = ($image->getClientOriginalName() !== null) ? (time() . '.' . $image->getClientOriginalName()) : "";
            $image->move(env('PATH_UPLOAD'), $filename);
            $user->image = $filename;
        } else {
            $user->image = '';
        }
        $user->save();
        if ($request->roles !== null) { //@todo: add roles relation
            $user->roles()->attach($request->roles);
        }

        //@todo: Gửi thông báo khởi tạo nhóm câu hỏi
        $notification = array('avarta' => 'user_registration_successful.svg', 'title' => 'Người dùng', 'message' => 'Một người dùng vừa được khởi tạo', 'created_at' => Carbon::now(), 'callback' => "admin/user/view/" . $user->id);
        send_notification($notification);

        return redirect()->route('admin.user.getList')->with(['flash_message' => 'Thêm người dùng thành công !!!']);
    }

    /**
     * @todo Hiển thị trang cập nhật thông tin người dùng
     * @param $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($userId){
        $user = User::find($userId);
        $roles = Role::select('id', 'name')->get();
        $rolesUser = $user->roles;
        return view('admin.users.edit', compact('user', 'userId', 'roles', 'rolesUser'));
    }

    /**
     * @todo Cập nhật thông tin người dùng
     * @param Request $request
     * @param $userId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEdit(Request $request, $userId){
        $user = User::find($userId);
        $user->name = $request->input('txtUser');
        if ($request->input('txtPass')) { //@todo: Validate Re-password
            $this->validate($request, ['txtRePass' => 'same:txtPass'], ['txtRePass.same' => 'Mật khẩu xác nhận không khớp!']);
            $user->password = Hash::make($request->input('txtPass'));
        }
        $user->email = $request->input('txtMail');
        $user->remember_token = $request->input('_token');

        if (Input::hasFile('avarta')) {  //@todo: upload image user
            $this->validate($request, ['avarta' => 'mimes:png,jpg'], ['avarta.mimes' => 'Hệ thống chỉ chấp nhận định dạng .png hoặc .jpg']);
            $image = Input::file('avarta');
            $filename = ($image->getClientOriginalName() !== null) ? (time() . $image->getClientOriginalName()) : "";
            $image->move(env('PATH_UPLOAD'), $filename);
            if ($user->image !== '') { //@todo: Xóa ảnh cũ
                File::delete(env('PATH_UPLOAD') . $user->image);
            }
            $user->image = $filename;
        }
        $user->birthday_at = (isset($request->txtBirthday) AND ($request->txtBirthday !== "")) ? date("Y-m-d", strtotime(str_replace("/", "-", $request->txtBirthday))) : NULL;
        $user->gender = (isset($request->gender) AND ($request->gender != null)) ? $request->gender : 0;
        $user->address = (isset($request->txtAddress) AND ($request->txtAddress != null)) ? trim($request->txtAddress) : "";
        $user->phone = (isset($request->txtPhone) AND ($request->txtPhone)) ? trim($request->txtPhone) : '';
        $user->save();

        if ($request->input('roles') !== null) { //@todo: cập nhật role relation
            $user->roles()->sync($request->input('roles'));
        } else {
            $user->roles()->detach();
        }
        return redirect()->route('admin.user.getList')->with(['flash_message' => 'Cập nhật dữ liệu thành công!']);
    }

    /**
     * @todo: Xóa user
     *
     * @param $userId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($userId){
        $user_current_login_id = Auth::user()->id;
        $user = User::find($userId);
        $roleUsers = $user->roles;
        if (count($roleUsers) > 0) { //@todo Nếu là superadmin thì không được xóa
            foreach ($roleUsers as $roleUser) {
                if ($roleUser['name'] == 'super-admin') {
                    return redirect()->route('admin.user.getList')->with(['flash_message_danger' => 'Bạn không thể xóa người dùng này!']);
                }
            }
        }
        if ($user_current_login_id == intval($userId)) { //@todo: Không được xóa chính nó
            return redirect()->route('admin.user.getList')->with(['flash_message_danger' => 'Bạn không thể xóa người dùng này!']);
        }
        $user->roles()->detach(); //@todo: xóa role relation
        $user->delete(); //@todo: xóa role relation
        return redirect()->route('admin.user.getList')->with(['flash_message' => 'Xóa người dùng thành công!']);
    }

    /**
     * @todo: Hiển thị thông tin chi tiết người dùng
     * @param $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getViewDetail($userId)
    {
        $user = User::find($userId);
        return view('admin.users.detail', compact('user'));
    }

    /**
     * @todo: Hiển thị danh sách nhóm người dùng
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRoles(){
        $roles = Role::select('id', 'name', 'display_name')->orderBy('display_name', 'ASC')->paginate(10);;
        return view('admin.user-roles.list', compact('roles'));
    }

    /**
     * Hiển thị trang thêm role
     * Truyền dữ liệu permission
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAddRole(){
        $perms = Permission::select('id', 'name')->get();
        return view('admin.user-roles.add', compact('perms'));
    }

    /**
     * Xử lý thêm Role
     * Thêm Role Relation
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAddRole(RoleRequest $request)
    {
        $role = new Role();
        $role->name = trim($request->txtRole);
        $role->display_name = trim($request->txtDisplayRole);
        $role->description = trim($request->txtDescRole);
        $role->save();
        if ($request->perms !== null) {
            $role->permissions()->attach($request->perms);
        }
        return redirect()->route('admin.user.role.getList')->with(['flash_message' => 'Thêm nhóm người dùng thành công']);
    }

    /**
     * Hiển thị trang cập nhật role
     * Truyền dữ liệu
     *
     * @param $roleId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEditRole($roleId){
        $role = Role::find($roleId);
        $perms = Permission::select('*')->get();
        $permsRoles = $role->permissions;
        return view('admin.user-roles.edit', compact('role', 'perms', 'permsRoles'));
    }

    /**
     * @todo: Cập nhật role
     * @param $roleId
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEditRole($roleId, Request $request){
        $roleFromId = Role::find($roleId);
        $roleFromId->name = trim($request->txtRole);
        $roleFromId->display_name = trim($request->txtDisRole);
        $roleFromId->description = trim($request->txtDescRole);
        $roleFromId->save();
        if ($request->perms != null) { //@todo: cập nhật permission relation
            $roleFromId->permissions()->sync($request->perms);
        } else {
            $roleFromId->permissions()->detach();
        }
        return redirect()->route('admin.user.role.getList')->with(['flash_message' => 'Cập nhật nhóm người dùng thành công']);
    }

    /**
     * Xóa role
     * @param $roleId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDeleteRole($roleId){
        $role = Role::find($roleId);
        if(count($role->permissions) > 0){
            return redirect()->route('admin.user.role.getList')->with(['flash_message_danger' => 'Không thể xóa!<br/>Nhóm người dùng này còn chứa phân hệ con']);
        }else{
            $role->permissions()->detach(); //@todo: xóa permission relation
            Role::destroy($roleId);//@todo: xóa role
            return redirect()->route('admin.user.role.getList')->with(['flash_message' => 'Xóa thành công']);
        }
    }

    /**
     *@todo: Hiển thị danh sách permission
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPerms(){
        $perms = Permission::select('id', 'display_name')->orderBy('display_name', 'ASC')->paginate(10);;
        return view('admin.user-perms.list', compact('perms'));
    }

    /**
     * Hiển thị trang thêm permission
     * Truyền dữ liệu roles
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAddPerm(){
        $roles = Role::select('id', 'name', 'display_name')->get();
        return view('admin.user-perms.add', compact('roles'));
    }

    /**
     * Xử lý thêm permission
     * Xử lý thêm role theo permission
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAddPerm(PermissionRequest $request){
        $perm = new Permission();
        $perm->name = trim($request->txtPerm);
        $perm->display_name = trim($request->txtDisPerm);
        $perm->description = trim($request->txtDescPerm);
        $perm->save();
        if ($request->roles !== null) {
            $rids = $request->roles;
            $perm->roles()->attach($rids);
        }
        return redirect()->route('admin.user.perm.getList')->with(['flash_message' => 'Thêm quyền người dùng thành công']);
    }

    /**
     * Hiển thị trang cập nhật permission
     * Truyền dữ liệu
     *
     * @param $permId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEditPerm($permId)
    {
        $perm = Permission::find($permId);
        $roles = Role::select('id', 'name', 'display_name')->get();
        $rolesPerm = $perm->roles;
        return view('admin.user-perms.edit', compact('perm', 'roles', 'rolesPerm'));
    }

    /**
     * Xử lý cập nhật permission
     *
     * @param $permId
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEditPerm($permId, Request $request)
    {
        //Cập nhật permission
        $permFromId = Permission::find($permId);
        $permFromId->name = trim($request->txtPerm);
        $permFromId->display_name = trim($request->txtDisPerm);
        $permFromId->description = trim($request->txtDescPerm);
        //Cập nhật permission relation
        if ($request->rIds != null) {
            $permFromId->roles()->sync($request->rIds);
        } else {
            $permFromId->roles()->detach();
        }
        $permFromId->save();
        return redirect()->route('admin.user.perm.getList')->with(['flash_message' => 'Cập nhật thành công']);
    }

    /**
     * Xóa permission
     * Xóa quan hệ roles
     * @param $permId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDeletePerm($permId)
    {
        $perms = Permission::find($permId);
        if(count($perms->roles) > 0){
            return redirect()->route('admin.user.perm.getList')->with(['flash_message_danger' => 'Không thể xóa!']);
        }else{
            $perms->roles()->detach(); //@todo: xóa permission relation
            $perms->delete(); //@todo: xóa role
            return redirect()->route('admin.user.perm.getList')->with(['flash_message' => 'Xóa thành công!']);
        }
    }

    /**
     * @param $roleId
     * @return array
     */
//    public function getPermsFromRoleId($roleId)
//    {
//        if (\Request::ajax()) {
//            $perms = array();
//            if (strlen($roleId) > 1) {
//                $roleId = explode(",", $roleId);
//                for ($i = 0; $i < count($roleId); $i++) {
//                    array_push($perms, Role::find($roleId[$i])->permissions);
//                }
//            } else {
//                $roleId = intval($roleId);
//                array_push($perms, Role::find($roleId)->permissions);
//            }
//            return $perms;
//        }
//    }
}
