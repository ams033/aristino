<?php

namespace App\Http\Controllers\Gogi;

use App\Http\Requests\RatingRequest;
use App\Mail\RatingMail;
use App\RatingSurvey;
use App\Restaurant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;


class RatingController extends Controller
{
    //
    protected $code;

    public function __construct()
    {
    }

    public function trangchu()
    {
        return view('gogi.rating.trangchu');
    }

    public function thamgia()
    {
        $restaurant = Restaurant::select('id', 'name')->orderBy('id', 'ASC')->get();
        return view('gogi.rating.thamgia', compact('restaurant'));
    }

    public function post_thamgia(RatingRequest $request)
    {
        if(intval($request->sure) < 6){
            $data = array(
                'restaurant_id' => intval($request->sltGogi),
                'email' => trim($request->email),
                'code'  => trim($request->code),
                'sure'  => $request->sure
            );
            return view('gogi.rating.xinloi', compact('data'));
        }else{
            $rating_survey = new RatingSurvey();
            $rating_survey->restaurant_id = intval($request->sltGogi);
            $rating_survey->email = trim($request->email);
            $rating_survey->code = trim($request->code);
            $rating_survey->sure = $request->sure;
            $rating_survey->feedback = '';
            $rating_survey->save();
            $this->code = trim($request->code);
            $this->email($request->email);
            return redirect()->route('gogi.rating.camon');
        }
    }

    public function thele()
    {
        return view('gogi.rating.thele');
    }

    public function xinloi()
    {
        return view('gogi.rating.xinloi');
    }

    public function post_xinloi(Request $request){
        $rating_survey = new RatingSurvey();
        $rating_survey->restaurant_id = intval($request->restaurant_id);
        $rating_survey->email = trim($request->email);
        $rating_survey->code = trim($request->code);
        $rating_survey->sure = $request->sure;
        $rating_survey->feedback = trim($request->feedback);
        $rating_survey->save();
        return redirect()->route('gogi.rating.camon');
    }

    public function camon()
    {
        return view('gogi.rating.camon');
    }

    public function bangxephang($month = 5)
    {
        switch ($month){
            case 7:
                $start = '2017-07-01';$end = '2017-07-31';
                break;
            case 6:
                $start = '2017-06-01';$end = '2017-06-30';
                break;
            default:
                $start = '2017-05-01';$end = '2017-05-31';
                break;
        }
        $restaurant = Restaurant::select('id')->get()->toArray();
        $top = array();
        foreach ($restaurant as $res) {
            $promoters = RatingSurvey::where('sure', '>=', 9)->where('restaurant_id', $res['id'])->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end)->count('id');
            $detractors = RatingSurvey::where('sure', '<=', 6)->where('restaurant_id', $res['id'])->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end)->count('id');
            $total = RatingSurvey::where('restaurant_id', $res['id'])->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end)->count('id');

            if ($total !== 0){
                $top[] = array("svalue"=>round(($promoters - $detractors)/$total*10000),"id"=>$res['id']);
            }
        }
        $top    =   sort_2array_desc($top,"svalue");
        return view('gogi.rating.bangxephang', compact('top', 'month'));
    }

    public function email($email){
        $id_chiendich       = 965;
        // $gift_conditional   = isset($_REQUEST['vote']) ? "":"";
        $gift_conditional   = "• Khi sử dụng dịch vụ tại nhà hàng, Khách hàng tham gia đánh giá chất lượng các nhà hàng     	  GoGi House Hà Nội và nhận evoucher 100,000 vnđ.<br>
• Evoucher 100,000 vnđ áp dụng cho mỗi chi tiêu 400,000 vnđ<br>
• Evoucher áp dụng tại hệ thống GoGi House Hà Nội và có giá trị sử dụng 30 ngày từ ngày phát 	  nhưng không quá 30/06/2017.
";

        $fieldquery = array(
            'id_chiendich' => $id_chiendich,
            'key' => "216321hdtyfsd13bdhjayfaygkdy923412312oqwhfauis8easHK21302hasd76y",
            'full_name' => "",
            'email' => $email,
            'phone' => '',
            'utm_source' => "",
            'note' => "",
            'gift_value' => "100000",
            'gift_conditional' => $gift_conditional
        );


        $Curl_Session = curl_init('http://evoucher.ggg.com.vn/reg/');
        curl_setopt($Curl_Session, CURLOPT_POST, 1);
        curl_setopt($Curl_Session, CURLOPT_POSTFIELDS, $fieldquery);
        curl_setopt($Curl_Session, CURLOPT_HEADER, 0);
        curl_setopt($Curl_Session, CURLOPT_RETURNTRANSFER, 1);

        $ret = trim(curl_exec($Curl_Session));
        $ret = json_decode($ret, true);

        if($ret['success']==1){
            Mail::to($email)->send(new RatingMail($ret['code']));
        }
        return;

    }

//    public function survey($month = 6)
//    {
//        $start = ''; $end = '';
//        switch ($month){
//            case 7:
//                $start = '2017-07-01';$end = '2017-07-31';
//                break;
//            case 8:
//                $start = '2017-08-01';$end = '2017-08-31';
//                break;
//            default:
//                $start = '2017-06-01';$end = '2017-06-30';
//                break;
//        }
//        $restaurant = Restaurant::select('id')->get()->toArray();
//        $top = array();
//        foreach ($restaurant as $res) {
//            $promoters = RatingSurvey::where('sure', '>=', 9)->where('restaurant_id', $res['id'])->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end)->count('id');
//            $detractors = RatingSurvey::where('sure', '<=', 6)->where('restaurant_id', $res['id'])->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end)->count('id');
//            $total = RatingSurvey::where('restaurant_id', $res['id'])->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end)->count('id');
//
//            if ($total !== 0){
//                $top[] = array("svalue"=>round(($promoters - $detractors)/$total*10000),"id"=>$res['id']);
//            }
//        }
//        $top    =   sort_2array_desc($top,"svalue");
//        return view('gogi.rating.bangxephang', compact('top'));
//
//    }
}
