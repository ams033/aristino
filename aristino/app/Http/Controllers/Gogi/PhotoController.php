<?php

namespace App\Http\Controllers\Gogi;

use App\Image_vote;
use App\Mail\GoGiPhotoEmail;
use App\Prize;
use App\PrizeFinal;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class PhotoController extends Controller
{
    public function trangchu()
    {
        return view('gogi.photo.trangchu');
    }

    public function thamgia()
    {

        $image_vote = Image_vote::select('id', 'image')->where("post_id", '<>',"")->orderBy('id', 'DESC')->limit(7)->get();
        return view('gogi.photo.thamgia', compact('image_vote'));
    }


    public function share_update()
    {

        $image_vote = Image_vote::select('id', 'image')->where("post_id", '<>',"")->orderBy('id', 'DESC')->paginate(30);
        foreach ($image_vote as $item) {
            # code...
            $url =  route('gogi.photo.noidungchitiet',$item['id']);
            $share_count    =   $this->syn_facebook_share($url);


            $update_share = Image_vote::find($item['id']);
            if( $share_count!== $item['share']){
                $update_share->share     =  $share_count;
                $update_share->save();
            }
        }
        return ;
    }
    public function syn_facebook_share($url)
    {

        $Curl_Session = 'https://graph.facebook.com/v2.2/?id='. urlencode($url)."&fields=og_object{engagement}";
        $ret = trim(file_get_contents($Curl_Session));
        $ret = json_decode($ret, true);
        if(isset($ret['og_object']['engagement']['count']))
            return $ret['og_object']['engagement']['count'];
        else
            return 0;
    }

    public function thele()
    {
        return view('gogi.photo.thele');
    }

    public function dangky()
    {
        return view('gogi.photo.dangky');
    }

    public function post_thamgia(Request $request)
    {
//        $image_vote = new Image_vote();
//        $image_vote->username = trim($request->username);
//        $image_vote->email = trim($request->email);
//        $image_vote->image = $request->img_upload;
//        $image_vote->post_id = '';
//        $image_vote->save();
//        $this->email(trim($request->email));
//        return redirect()->route('gogi.photo.bangxephang');

        if(\Request::ajax()){
            $image_vote = new Image_vote();
            $image_vote->username = trim($request->name);
            $image_vote->email = trim($request->email);
            $image_vote->image = trim($request->filename);
            $image_vote->post_id = '';
            $image_vote->save();
            return $image_vote;
        }else{
            return 'error';
        }
    }

    public function email($email)
    {
        $id_chiendich       = 971;
        // $gift_conditional   = isset($_REQUEST['vote']) ? "":"";
        $gift_conditional   = "- Mã code sẽ được gửi về Email khách đã đăng ký<br>
- Áp dụng trên hệ thống GoGi House miền Bắc <br>
- Chỉ áp dụng vào trưa các ngày từ thứ 2 đến thứ 6 hàng tuần <br>
- Có hạn sử dụng 30 ngày từ ngày phát nhưng không quá 31/07/2017 <br>
- Không áp dụng song song với các ưu đãi.";

        $fieldquery = array(
            'id_chiendich' => $id_chiendich,
            'key' => "216321hdtyfsd13bdhjayfaygkdy923412312oqwhfauis8easHK21302hasd76y",
            'full_name' => "",
            'email' => $email,
            'phone' => '',
            'utm_source' => "",
            'note' => "",
            'gift_value' => "60000",
            'gift_conditional' => $gift_conditional
        );


        $Curl_Session = curl_init('http://evoucher.ggg.com.vn/reg/');
        curl_setopt($Curl_Session, CURLOPT_POST, 1);
        curl_setopt($Curl_Session, CURLOPT_POSTFIELDS, $fieldquery);
        curl_setopt($Curl_Session, CURLOPT_HEADER, 0);
        curl_setopt($Curl_Session, CURLOPT_RETURNTRANSFER, 1);

        $ret = trim(curl_exec($Curl_Session));
        $ret = json_decode($ret, true);

        if($ret['success']==1){
            Mail::to($email)->send(new GoGiPhotoEmail($ret['code']));
        }
        return;
    }

    public function bangxephang($month = 5)
    {
        $image_top = Image_vote::select('id', 'image', 'vote', 'share')->orderBy('vote', 'DESC')->orderBy('share', 'DESC')->limit(5)->get();
        $contest = Prize::select('id', 'prize_type_id', 'image_vote_id')->where('month', $month)->get();
        return view('gogi.photo.bangxephang', compact('image_top', 'contest'));
    }

    public function giaichungcuoc()
    {
        $prize_final = PrizeFinal::select('id', 'prize_final_name', 'image_vote_id')->orderBy('id', 'ASC')->get();
        return view('gogi.photo.giaichungcuoc',  compact('prize_final'));
    }

    public function noidungchitiet($id)
    {
        $image_vote = Image_vote::find($id);

        if(isset($_GET['fb_post_id']))
            $this->update_post_id($id,$_GET['fb_post_id']);

        return view('gogi.photo.noidungchitiet', compact('image_vote'));
    }

    public function getPrizeList()
    {
        $prizes = Prize::select('id', 'month', 'prize_type_id', 'image_vote_id')->paginate(5);
        return view('admin.gogi.prize.list', compact('prizes'));
    }

    public function getPrizeEdit($id)
    {
        $prize = Prize::find($id);
        $image_top = Image_vote::select('id')->orderBy('like', 'DESC')->get();
        return view('admin.gogi.prize.edit', compact('prize', 'image_top'));
    }

    public function postPrizeEdit(Request $request, $id)
    {
        $prize = Prize::find($id);
        $prize->image_vote_id = intval($request->sltImageVote);
        $prize->save();
        return redirect()->route('admin.photo.prize.getList')->with(['flash_message' => 'Cập nhật thành công']);
    }

    public function getContestList()
    {
        $contests = Image_vote::select('id', 'post_id', 'image', 'username', 'email', 'vote', 'share')->orderBy('id', 'DESC')->paginate(50);
        if(isset($_GET['orderVote'])){
            $contests = Image_vote::select('id', 'post_id', 'image', 'username', 'email', 'vote', 'share')->orderBy('vote', 'DESC')->paginate(30);
        }
        if(isset($_GET['orderShare'])){
            $contests = Image_vote::select('id', 'post_id', 'image', 'username', 'email', 'vote', 'share')->orderBy('share', 'DESC')->paginate(30);
        }
        if(isset($_GET['sendemail'])){
            foreach ($contests as $item){
                $this->update_post_id($item['id'],'1000000000000_200000000000');
            }
        }
        return view('admin.gogi.contest.list', compact('contests'));
    }

    public function getContestDelete($id)
    {
        $prize = Prize::where('image_vote_id', $id)->get();
        if (count($prize) > 0) {
            return redirect()->route('admin.photo.contest.getList')->with(['flash_message_danger' => 'Bài dự thi này đã trúng thưởng.<br/>Không thể xóa!']);
        } else {
            Image_vote::destroy($id);
            return redirect()->route('admin.photo.contest.getList');
        }
    }

    public function getPrizeFinalList(){
        $prize_final = PrizeFinal::select('id', 'prize_final_name', 'image_vote_id')->get();
        return view('admin.gogi.prize_final.list', compact('prize_final'));
    }

    public function getPrizeFinalEdit($id){
        $prize_final = PrizeFinal::find($id);
        $image_top = Image_vote::select('id')->orderBy('like', 'DESC')->get();
        return view('admin.gogi.prize_final.edit', compact('prize_final', 'image_top'));
    }

    public function postPrizeFinalEdit(Request $request, $id){
        $prize_final = PrizeFinal::find($id);
        $prize_final->image_vote_id = intval($request->sltImageVote);
        $prize_final->save();
        return redirect()->route('admin.photo.prizefinal.getList')->with(['flash_message' => 'Cập nhật thành công']);
    }

    public function update_post_id($id,$post_id){
        $image_vote = Image_vote::find($id);
        if($image_vote->post_id == 0){
            $image_vote->post_id    = $post_id;
            $image_vote->save();
            $this->email($image_vote->email);
        }
    }

    //@todo Tìm kiếm
    public function search(Request $request){
        $search = trim($request->input('search'));
        if($search !== ''){
            $contests = Image_vote::select('id', 'post_id', 'image', 'username', 'email', 'vote', 'share')->orderBy('id', 'DESC')->where('email', $search)->paginate(50);
        }
        return view('admin.gogi.contest.list', compact('contests', 'search'));
    }

    public function getEmail($id){
        $this->update_post_id($id,'1000000000000_200000000000');
        return back()->with(['flash_message' => 'Gửi thành công mã code tới người dự thi này!']);
    }
}
