<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'txtUser' => 'unique:users,name',
            'txtRePass' => 'same:txtPass',
            'txtMail' => 'unique:users,email',
            'avarta'       => 'mimes:png,jpg'
        ];
    }

    public function messages()
    {
        return [
            'txtUser.unique' => 'Người dùng này đã tồn tại',
            'txtRePass.same' => 'Mật khẩu không đồng nhất',
            'txtMail.unique' => 'Email này đã được đăng ký',
            'avarta.mimes' => 'Hệ thống chỉ chấp nhận định dạng .png hoặc .jpg'
        ];
    }
}
