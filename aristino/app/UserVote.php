<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserVote extends Model
{
    //
    protected $table    = 'user_vote';
    protected $fillable = ['id', 'social_user_id', 'remote_ip','post_vote_id'];
}
