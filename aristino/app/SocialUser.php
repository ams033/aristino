<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialUser extends Model
{
    //
    protected $table = 'social_user';

    protected $fillable = ['facebook_id','fb_data','verify', 'name', 'email', 'avarta', 'token', 'refreshToken'];
}
