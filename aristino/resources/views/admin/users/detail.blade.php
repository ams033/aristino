@extends('admin.master')
@section('content')
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="col-sm-8 col-sm-offset-2 text-center">
                <div class="pricing-card">
                    <div class="pricing-card-header bg-primary">
                        <h4 class="m-y-sm">Thông tin chi tiết</h4>
                    </div>
                    <div class="pricing-card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3 " align="center">
                                        <img alt="User Pic" class="img-circle img-responsive"
                                             src="<?php echo ($user->image !== '') ? url(env('PATH_UPLOAD') . $user->image) : url('inside/img/user_avarta.jpg') ?>"
                                        >
                                    </div>
                                    <div class=" col-md-9 col-lg-9 ">
                                        <table class="table table-user-information">
                                            <tbody>
                                            <tr>
                                                <td>Họ và tên:</td>
                                                <td>{{ $user->name }}</td>
                                            </tr>

                                            <tr>
                                                <td>Ngày sinh:</td>
                                                <td>
                                                    {{ ($user->birthday_at !== null) ? date('d/m/Y', strtotime($user->birthday_at)) : 'Không có thông tin' }}
                                                </td>
                                            </tr>

                                            <tr>
                                            <tr>
                                                <td>Giới tính</td>
                                                <td>
                                                    <?php $gender = ''; if ($user->gender == 0) {
                                                        $gender = 'Không xác định';
                                                    } else if ($user->gender == 1) {
                                                        $gender = 'Nam';
                                                    } else {
                                                        $gender = 'Nữ';
                                                    }?>
                                                    {{ $gender }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Địa chỉ:</td>
                                                <td>{{ ($user->address !== '') ? $user->address : 'Không có thông tin' }}</td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                                            </tr>
                                            <td>Số điện thoại</td>
                                            <td>{{ ($user->phone !== '') ? $user->phone : 'Không có thông tin' }}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row hidden-print">
                            <a class="btn btn-primary" href="{{ url('admin/user/list') }}"><span
                                        class="icon icon-backward icon-lg icon-fw"></span>Trở về</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop