@extends('admin.users.user')
@section('user-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body panel-collapse table-flip-scroll">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tên</th>
                            <th>Email</th>
                            <th>Nhóm người dùng</th>
                            <th>Hành động</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($users) AND $users != null)
                            <?php $seri = 0; ?>
                            @foreach($users as $user)
                                <?php $seri++; ?>
                                <tr>
                                    <td>{{ $seri }}</td>
                                    <td><a href="{{ route('admin.user.getViewDetail', $user['id']) }}">{{ $user['name'] }}</a></td>
                                    <td>{{ $user['email'] }}</td>
                                    <td>
                                        <?php $rolesFromUser = App\User::find($user['id'])->roles; ?>
                                        <ul>
                                            @if($rolesFromUser !== null)
                                                @foreach ($rolesFromUser as $roleFromUser)
                                                    <li>{{ $roleFromUser['display_name'] }}</li>
                                                @endforeach
                                            @else
                                                <li><span>Không có dữ liệu</span></li>
                                            @endif
                                        </ul>
                                    </td>
                                    <td><a href="{{ route('admin.user.getEdit', $user['id']) }}" class="btn btn-primary"><span class="icon icon-pencil"></span> Cập nhật</a> <a href="{{ route('admin.user.getDelete', $user['id']) }}" class="btn btn-danger" onclick="return confirm_delete('Bạn chắc chắn muốn xóa người dùng này?')"><span class="icon icon-trash-o"></span> Xóa</a></td>
                                </tr>
                        </tbody>
                        @endforeach
                        @endif
                    </table>
                </div>
            </div>
            <!-- Pagination -->
            <div class="text-right"> {{ $users->links() }} </div>
            <!-- /.Pagination -->
        </div>
    </div>
@stop