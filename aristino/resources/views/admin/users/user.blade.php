@extends('admin.master')
@section('title', 'Aristino | Quản lý người dùng')
@section('content')
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">
                <div class="title-bar-actions">
                    <a class="btn btn-default" href="{{ route('admin.user.getAdd') }}"><span class="icon icon-plus-square"></span> Thêm mới</a>
                </div>
                <h1 class="title-bar-title">
                    <span class="d-ib">Nhân sự</span>
                </h1>
            </div>
            @yield('user-content')
        </div>
    </div>
@stop