@extends('admin.users.user')
@section('title', 'Thêm nhân sự')
@section('user-content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div>
                <a href="{{ url('admin/user/list')  }}" class="btn-toolbar"><span class="icon icon-angle-left"></span>
                    &nbsp;<span class="caption control-label">Trở về</span></a>
            </div>
            <div class="demo-form-wrapper">
                @if (count($errors) > 0)
                    <div class="alert alert-danger notification">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form data-toggle="validator" method="post" enctype="multipart/form-data">
                    <input type='hidden' name='_token' value='{!! csrf_token() !!}'>
                    <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="icon icon-info-circle icon-lg"></span>
                        <small>Thêm mới một người dùng</small>
                    </div>
                    <div class="form-group">
                        <label for="name" class="control-label">Tên người dùng *</label>
                        <input id="name" class="form-control" type="text" name="txtUser" data-rule-required="true"
                               data-msg-required="Vui lòng nhập tên người dùng!">
                        <small class="help-block">Vui lòng nhập tên của người dùng</small>
                    </div>

                    <!-- password -->
                    <div class="form-group">
                        <label for="pass" class="control-label">Mật khẩu *</label>
                        <input id="pass" class="form-control" type="password" name="txtPass" data-rule-required="true"
                               data-msg-required="Vui lòng nhập mật khẩu!">
                        <small class="help-block">Nhập vào mật khẩu</small>
                    </div>
                    <!-- /.password -->

                    <!-- Re-password -->
                    <div class="form-group">
                        <label for="repass" class="control-label">Nhập lại mật khẩu *</label>
                        <input id="repass" class="form-control" type="password" name="txtRePass"
                               data-rule-required="true"
                               data-msg-required="Vui lòng nhập lại mật khẩu!">
                        <small class="help-block">Nhập lại mật khẩu</small>
                    </div>
                    <!-- re-password -->

                    <!-- email -->
                    <div class="form-group">
                        <label for="email" class="control-label">Email *</label>
                        <input id="email" class="form-control" type="email" name="txtMail" data-rule-required="true"
                               data-msg-required="Vui lòng nhập email người dùng!" placeholder="demo@ams.net.vn">
                        <small class="help-block">Vui lòng nhập email của người dùng</small>
                    </div>
                    <!-- /.email -->

                    <!-- image -->
                    <div class="form-group">
                        <label for="image" class="control-label">Hình ảnh</label>
                        <div class="input-group image-preview">
                            <input type="text" class="form-control image-preview-filename" disabled="disabled">
                            <!-- don't give a name === doesn't send on POST/GET -->
                            <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                            <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                <span class="glyphicon glyphicon-remove"></span> Clear
                            </button>
                                <!-- image-preview-input -->
                            <div class="btn btn-primary image-preview-input">
                                <span class="glyphicon glyphicon-folder-open"></span>
                                <span class="image-preview-input-title">Browse</span>
                                <input type="file" accept="image/png, image/jpeg, image/gif" name="avarta"/>
                                <!-- rename it -->
                            </div>
                            </span>
                        </div>
                    </div>
                    <!-- /.image -->

                    <!-- phone -->
                    <div class="form-group">
                        <label for="phone" class="control-label">Số điện thoại</label>
                        <input id="phone" class="form-control" type="text" name="txtPhone" placeholder="09xxxxxxxx"
                               onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                        <small class="help-block">Vui lòng nhập số điện thoại của người dùng (không bắt buộc)</small>
                    </div>
                    <!-- phone -->

                    <!-- Address -->
                    <div class="form-group">
                        <label for="address" class="control-label">Địa chỉ</label>
                        <input id="address" class="form-control" type="text" name="txtAddress"
                               placeholder="Ba Đình - Hà Nội">
                        <small class="help-block">Vui lòng nhập địa chỉ của người dùng (không bắt buộc)</small>
                    </div>
                    <!-- /.address -->

                    <!-- gender -->
                    <div class="form-group">
                        <label for="gender" class="control-label">Giới tính</label>
                        <div>
                            <label class="custom-control custom-control-primary custom-radio">
                                <input class="custom-control-input" type="radio" name="gender" value="0">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-label">Không xác định</span>
                            </label>
                            <label class="custom-control custom-control-primary custom-radio">
                                <input class="custom-control-input" type="radio" value="1" name="gender"
                                       checked="checked">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-label">Nam</span>
                            </label>
                            <label class="custom-control custom-control-primary custom-radio">
                                <input class="custom-control-input" type="radio" name="gender" value="2">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-label">Nữ</span>
                            </label>
                        </div>
                        <small class="help-block">Vui lòng chọn giới tính của người dùng (không bắt buộc)</small>
                    </div>
                    <!-- /.gender -->

                    <!-- Birthday -->
                    <div class="form-group">
                        <label for="birthday" class="control-label">Ngày sinh</label>
                        <div class="input-with-icon">
                            <input class="form-control" type="text" name="txtBirthday" data-provide="datepicker"
                                   data-date-today-highlight="true"
                                   data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy">
                            <span class="icon icon-calendar input-icon"></span>
                        </div>
                        <small class="help-block">Vui lòng chọn ngày sinh của người dùng (không bắt buộc)</small>
                    </div>
                    <!-- /.birthday -->

                    <!-- Roles -->
                    <div class="form-group">
                        <label class="control-label">Nhóm người dùng</label><br/>
                        @if(isset($roles) AND $roles != null)
                            @foreach ($roles as $role)
                                <label class="custom-control custom-control-primary custom-checkbox">
                                    <input class="custom-control-input rolesAdd" type="checkbox" name="roles[]"
                                           value="{{ $role['id'] }}" roleId="{{ $role['id'] }}"/>
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-label">{{ $role['name'] }}</span>
                                </label>
                            @endforeach
                        @else
                            <span class="">Không có dữ liệu nhóm người dùng</span>
                        @endif
                    </div>
                    <!-- Role -->
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Thêm mới</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop