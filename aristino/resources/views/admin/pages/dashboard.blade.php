@extends('admin.master')
@section('title', 'Bảng điều khiển')
@section('content')
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">
                <h1 class="title-bar-title">
                    <span class="d-ib">Bảng điều khiển</span>

                </h1>
            </div>
            <div class="row gutter-xs">
                <div class="col-md-6 col-lg-6 col-lg-push-0">
                    <div class="card bg-primary">
                        <div class="card-body">
                            <div class="media">
                                <div class="media-middle media-left">
                      <span class="bg-primary-inverse circle sq-48">
                        <span class="icon icon-user"></span>
                      </span>
                                </div>
                                <div class="media-middle media-body">
                                    <h6 class="media-heading">Bài dự thi</h6>
                                    <h3 class="media-heading">
                                        <span class="fw-l">{{ number_format($total_post) }}</span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-lg-push-0">
                    <div class="card bg-danger">
                        <div class="card-body">
                            <div class="media">
                                <div class="media-middle media-left">
                      <span class="bg-danger-inverse circle sq-48">
                        <span class="icon icon-shopping-bag"></span>
                      </span>
                                </div>
                                <div class="media-middle media-body">
                                    <h6 class="media-heading">Người tham gia</h6>
                                    <h3 class="media-heading">
                                        <span class="fw-l">{{ number_format($total_user_participation) }}</span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row gutter-xs">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <strong>Danh sách 30 bài dự thi vote cao nhất </strong>
                        </div>
                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-12 m-y">
                                    <table class="table table-borderless table-fixed">
                                        <thead>
                                        <tr>
                                            <th class="col-xs-3 text-left">Tiêu đề</th>
                                            <th class="col-xs-3 text-right">Social</th>
                                            <th class="col-xs-3 text-right">Vote</th>
                                            <th class="col-xs-3 text-right">Facebook ID</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if(count($postVote) > 0)
                                            @foreach ($postVote as $post)
                                                <tr>
                                                    <td class="col-xs-3 text-left"><a href="{{ url('aristino/bai-du-thi/' . $post['id']) }}" target="_blank">{{ mb_strtoupper($post['title']) }}</a></td>
                                                    <td class="col-xs-3 text-right">{{ number_format($post['like']) }}</td>
                                                     <td class="col-xs-3 text-right">{{ number_format($post['vote']) }}</td>

                                                    <td class="col-xs-3 text-right"><a href="http://facebook.com/{{ $post['fb_user_id'] }}" target="_blank">{{ $post['username'] }}</a></td>
                                                </tr>
                                            @endforeach
                                        @endif

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <strong>Danh sách 30 người tham gia gần nhất</strong>
                        </div>
                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-12 m-y">
                                    <table class="table table-borderless table-fixed">
                                        <thead>
                                        <tr>
                                            <th class="col-xs-3 text-left">Facebook ID</th>
                                            <th class="col-xs-3 text-left">Username</th>
                                            <th class="col-xs-3 text-right">Email</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($user_recent) > 0)
                                                @foreach ($user_recent as $user)

                                                    <tr>
                                                        <td class="col-xs-3 text-left"><a href="http://facebook.com/{{ $user['facebook_id'] }}" target="_blank">{{ $user['facebook_id'] }}</a></td>
                                                        <td class="col-xs-3 text-left">{{ $user['name'] }}</td>
                                                        <td class="col-xs-3 text-right">{{ $user['email'] }}</td>

                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop