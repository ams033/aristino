@extends('admin.user-roles.role')
@section('role-content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div>
                <a href="{{ url('admin/user/role/list')  }}" class="btn-toolbar"><span class="icon icon-angle-left"></span>
                    &nbsp;<span class="caption control-label">Trở về</span></a>
            </div>
            <div class="demo-form-wrapper">
                @if (count($errors) > 0)
                    <div class="alert alert-danger notification">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form data-toggle="validator" method="post">
                    {{ csrf_field() }}
                    <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    <span class="icon icon-info-circle icon-lg"></span>
                    <small>Khi thêm một nhóm người dùng, người dùng có thể phân quyền cho nhóm người dùng đó.</small>
                  </div>
                  <div class="form-group">
                    <label for="roleName" class="control-label">Nhóm người dùng</label>
                    <input id="roleName" class="form-control" type="text" name="txtRole" data-rule-required="true"
                           data-msg-required="Vui lòng nhập tên nhóm người dùng!">
                    <small class="help-block">Tên mặc định của nhóm người dùng</small>
                  </div>
                  <div class="form-group">
                    <label for="displayRoleName" class="control-label">Tên hiển thị nhóm người dùng</label>
                    <input id="displayRoleName" class="form-control" type="text" name="txtDisplayRole" autocomplete="off" data-rule-required="true"
                           data-msg-required="Vui lòng tên hiển thị nhóm người dùng!">
                    <small class="help-block">Tên hiển thị của nhóm người dùng</small>
                  </div>
                  <div class="form-group">
                    <label for="biography-1" class="control-label">Mô tả</label>
                    <textarea id="biography-1" class="form-control" name="txtDescRole" rows="3"></textarea>
                    <small class="help-block">Thêm một số thông tin chi tiết về nhóm người dùng mà bạn muốn thêm (không bắt buộc).</small>
                  </div>


                <div class="form-group">
                    <label class="control-label">Phân quyền</label> <br/>


                    @if(isset($perms) AND $perms != null)
                        @foreach($perms as $perm)
                            <label class="custom-control custom-control-primary custom-checkbox">
                                <input class="custom-control-input" type="checkbox" name="perms[]" value="{{ $perm['id'] }}">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-label">{{ $perm['name'] }}</span>
                            </label>
                        @endforeach
                    @else
                        <span class="">Không có dữ liệu nhóm người dùng</span>
                    @endif
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">Lưu lại</button>
                  </div>
                </form>
            </div>
        </div>
    </div>
@stop