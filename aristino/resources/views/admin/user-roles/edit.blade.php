@extends('admin.user-roles.role')
@section('role-content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div>
                <a href="{{ url('admin/user/role/list')  }}" class="btn-toolbar"><span class="icon icon-angle-left"></span>
                    &nbsp;<span class="caption control-label">Trở về</span></a>
            </div>
            <div class="demo-form-wrapper">
                <form data-toggle="validator" method="post">
                    {{ csrf_field() }}
                    <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="icon icon-info-circle icon-lg"></span>
                        <small>Cập nhật nhóm người dùng!</small>
                    </div>
                    <div class="form-group">
                        <label for="PermName" class="control-label">Nhóm người dùng</label>
                        <input id="PermName" class="form-control" type="text" name="txtRole" value="{{ $role['name'] }}" data-rule-required="true"
                               data-msg-required="Vui lòng nhập vào tên nhóm người dùng!">
                        <small class="help-block">Tên mặc định nhóm người dùng</small>
                    </div>
                    <div class="form-group">
                        <label for="displayPermName" class="control-label">Tên hiển thị</label>
                        <input id="displayPermName" class="form-control" type="text" value="{{ $role['display_name'] }}" name="txtDisRole"
                               autocomplete="off" data-rule-required="true"
                               data-msg-required="Vui lòng nhập vào tên hiển thị!">
                        <small class="help-block">Tên hiển thị nhóm người dùng</small>
                    </div>
                    <div class="form-group">
                        <label for="biography-1" class="control-label">Mô tả</label>
                        <textarea id="biography-1" class="form-control" name="txtDescRole" rows="3">@if($role['description']){{$role['description']}}@endif</textarea>
                        <small class="help-block">Thêm một số thông tin chi tiết về nhóm người dùng mà bạn muốn thêm (không
                            bắt buộc)
                        </small>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Quyền người dùng</label><br/>
                        @if(isset($perms) AND count($perms) > 0)
                            @foreach ($perms as $perm)
                                <label class="custom-control custom-control-primary custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" name="perms[]" value="{{ $perm['id'] }}" <?php foreach($permsRoles as $permsRole){ if ($permsRole['id'] == $perm['id']){echo 'checked'; }} ?> />
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-label">{{ $perm['name'] }}</span>
                                </label>
                            @endforeach
                        @else
                            <span class="">Không có dữ liệu nhóm người dùng</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Lưu lại</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
@stop