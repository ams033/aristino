@extends('admin.user-roles.role')
@section('role-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body panel-collapse table-flip-scroll">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nhóm người dùng</th>
                            <th>Phân quyền</th>
                            <th>Hành động</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($roles) > 0)
                            <?php $seri = 0; ?>
                            @foreach ($roles as $role)
                                <?php $seri++ ?>
                                <tr>
                                    <td>{{ $seri }}</td>
                                    <td>{{ $role['display_name'] }}</td>
                                    <td>
                                        <?php
                                            $roleFromIds = App\Role::find($role['id']);
                                        ?>
                                        <ul>
                                            @if(count($roleFromIds) > 0 AND isset($roleFromIds->permissions))
                                                @foreach($roleFromIds->permissions as $perm)
                                                    <li>{{ $perm['display_name'] }}</li>
                                                @endforeach
                                            @else
                                                <li><span>Không có dữ liệu</span></li>
                                            @endif
                                        </ul>
                                    </td>
                                    <td><a href="{{ route('admin.user.role.getEdit', $role['id']) }}" class="btn btn-primary"><span class="icon icon-pencil"></span> Cập nhật</a> <a href="{{ route('admin.user.role.getDelete', $role['id']) }}" class="btn btn-danger" onclick="return confirm_delete('Bạn chắc chắn muốn xóa nhóm người dùng này?')"><span class="icon icon-trash-o"></span> Xóa</a></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4">Không có dữ liệu để hiển thị</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- Pagination -->
            <div class="text-right">
                {{ $roles->links() }}
            </div>
            <!-- /.Pagination -->
        </div>
    </div>
@stop