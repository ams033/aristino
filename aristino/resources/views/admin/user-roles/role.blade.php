@extends('admin.master')
@section('title', 'Quản lý nhóm người dùng | Laracore System')
@section('content')
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">
                <div class="title-bar-actions">
                    <a class="btn btn-default" href="{{ route('admin.user.role.getAdd') }}"><span class="icon icon-plus-square"></span> Thêm mới</a>
                </div>
                <h1 class="title-bar-title">
                    <span class="d-ib">Nhóm người dùng</span>
                </h1>
            </div>
            @yield('role-content')
        </div>
    </div>
@stop