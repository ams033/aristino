@extends('admin.aristino.post.post')
@section('post-content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div>
                <a href="{{ url('admin/aristino/post/list')  }}" class="btn-toolbar"><span class="icon icon-angle-left"></span>
                    &nbsp;<span class="caption control-label">Trở về</span></a>
            </div>
            <div class="demo-form-wrapper">
                {{-- Thông báo lỗi --}}
                @if (count($errors) > 0)
                    <div class="alert alert-danger notification">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {{-- /.Thông báo lỗi --}}
                <form data-toggle="validator" method="post" enctype="multipart/form-data">
                    <input type='hidden' name='_token' value='{!! csrf_token() !!}'>
                    <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="icon icon-info-circle icon-lg"></span>
                        <small>Cập nhật bài dự thi</small>
                    </div>
                    <div class="form-group">
                        <label for="name" class="control-label">Tiêu đề</label>
                        <textarea name="txtTitle" id="" cols="5" rows="3"  data-rule-required="true"
                                  data-msg-required="Vui lòng nhập vào tiêu đề!" class="form-control">{{ $post['title'] }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="name" class="control-label">Cảm nhận ngắn</label>
                        <textarea name="txtDescription" id="" cols="5" rows="5"  data-rule-required="true"
                                  data-msg-required="Vui lòng nhập vào cảm nhận!" class="form-control">{{ $post['bref_description'] }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="name" class="control-label">Nội dung</label>
                        <textarea name="txtContent" id="" cols="5" rows="10"  data-rule-required="true"
                                  data-msg-required="Vui lòng nhập vào nội dung!" class="form-control">{{ $post['content'] }}</textarea>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Cập nhật</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop