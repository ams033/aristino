@extends('admin.aristino.post.post')
@section('post-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body panel-collapse table-flip-scroll">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Facebook ID</th>
                            <th>Tiêu đề</th>
                            <th>Tên</th>
                            <th>Email</th>
                            <th><a href="{{ url('admin/aristino/post/list?oderSocial=s') }}">Social</a></th>
                            <th><a href="{{ url('admin/aristino/post/list?orderVote=v') }}">Vote</a></th>
                            <th>Hành động</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($posts) AND count($posts) > 0)
                            @foreach($posts as $post)
                                <tr>
                                    <td><a href="http://facebook.com/{{ $post['fb_user_id'] }}" target="_blank">{{ $post['fb_user_id'] }}</a></td>
                                    <td><a href="{{ url('aristino/bai-du-thi/' . $post['id']) }}" target="_blank">{{ $post['title'] }}</a></td>
                                    <td><a href="http://facebook.com/{{$post['fb_user_id']}}" target="_blank">{{ $post['username'] }}</a></td>
                                    <td>{{ $post['email'] }}</td>
                                    <td>{{ number_format($post['like']) }}</td>
                                    <td>{{ number_format($post['vote']) }}</td>
                                    <td>
                                        <a href="{{ route('admin.aristino.post.getUpdate', $post['id']) }}"
                                           class="btn btn-primary"><span class="icon icon-edit"></span> Cập nhật</a>
                                        <a href="{{ route('admin.aristino.post.getDelete', $post['id']) }}"
                                           class="btn btn-danger" onclick="return confirm_delete('Bạn muốn xóa bài dự thi này?')"><span class="icon icon-trash"></span> Xóa</a>
                                    </td>
                                </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8">Không có dữ liệu để hiển thị</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- Pagination -->
            <div class="text-right"> {{ $posts->links() }} </div>
            <!-- /.Pagination -->
        </div>
    </div>
@stop