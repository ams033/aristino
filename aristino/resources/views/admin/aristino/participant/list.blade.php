@extends('admin.aristino.participant.participant')

@section('participant-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body panel-collapse table-flip-scroll">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Facebook ID</th>
                            <th>Tên</th>
                            <th>Email</th>
                            <th>Số điện thoại</th>
                            <th>Ngày sinh</th>
                            <th>Số CMTND</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($participants) AND count($participants) > 0)
                            @foreach($participants as $participant)
                                <tr>
                                    <td><a href="http://facebook.com/{{ $participant['facebook_id'] }}" target="_blank">{{ $participant['facebook_id'] }}</a></td>
                                    <td>{{ $participant['name'] }}</td>
                                    <td>{{ $participant['email'] }}</td>
                                    <td>{{ $participant['phone'] }}</td>
                                    <td>{{ date('d/m/Y', strtotime($participant['birthday'])) }}</td>
                                    <td>{{ $participant['cmnd'] }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6">Không có dữ liệu để hiển thị</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- Pagination -->
            <div class="text-right"> {{ $participants->links() }} </div>
            <!-- /.Pagination -->
        </div>
    </div>
@stop
