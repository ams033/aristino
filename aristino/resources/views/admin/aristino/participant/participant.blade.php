@extends('admin.master')
@section('title', 'Aristino | Người tham gia')
@section('content')
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">
                <h1 class="title-bar-title">
                    <span class="d-ib">Người tham gia</span>
                </h1>
            </div>
            @yield('participant-content')
        </div>
    </div>
@stop