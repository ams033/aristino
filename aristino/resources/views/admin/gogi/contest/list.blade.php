@extends('admin.gogi.contest.contest')
@section('contest-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body panel-collapse table-flip-scroll">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Post_id</th>
                            <th>Hình ảnh</th>
                            <th>Người dự thi</th>
                            <th>Email</th>
                            <th><a href="{{ url('gogi/photo/contest/list?orderVote=voted') }}">Vote</a></th>
                            <th><a href="{{ url('gogi/photo/contest/list?orderShare=shared') }}">Share</a></th>
                            <th>Hành động</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($contests) AND count($contests) > 0)
                            <?php $seri = 0; ?>
                            @foreach($contests as $contest)
                                <?php $seri++; ?>
                                <tr>
                                    <td>{{ $seri }}</td>
                                    <td>{{ $contest['post_id'] }}</td>
                                    <td><a href="{{ url('gogi/photo/noi-dung-chi-tiet/' . $contest['id']) }}"
                                           target="_blank"><img
                                                    src="{{ url('gogi/assets/uploads/' . $contest['image']) }}" alt=""
                                                    style="width: 50px; height: 50px;"></a></td>
                                    <td>{{ $contest['username'] }}</td>
                                    <td>{{ $contest['email'] }}</td>
                                    <td>{{ $contest['vote'] }}</td>
                                    <td>{{ $contest['share'] }}</td>
                                    <td>
                                        <a href="{{ route('admin.photo.contest.getEmail', $contest['id'])  }}"
                                           class="btn btn-primary"><span class="icon icon-send"></span> Gửi Code</a>
                                        <a href="{{ route('admin.photo.contest.getDelete', $contest['id']) }}"
                                           class="btn btn-danger"><span class="icon icon-trash"></span> Xóa</a>
                                    </td>
                                </tr>
                        </tbody>
                        @endforeach
                        @else
                            <tr>
                                <td colspan="8">Không có dữ liệu để hiển thị cho "{{ $search  }}"</td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
            <!-- Pagination -->
            <div class="text-right"> {{ isset($contests) ? $contests->links() : '' }} </div>
            <!-- /.Pagination -->
        </div>
    </div>
@stop