@extends('admin.master')

@section('content')
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">
                <h1 class="title-bar-title">
                    <span class="d-ib">Giải thưởng chung cuộc</span>
                </h1>
            </div>
            @yield('prizefinal-content')
        </div>
    </div>
@stop