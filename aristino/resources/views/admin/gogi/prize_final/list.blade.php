@extends('admin.gogi.prize_final.prize_final')
@section('prizefinal-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body panel-collapse table-flip-scroll">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Loại giải thưởng</th>
                            <th>Bài dự thi</th>
                            <th>Hành động</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($prize_final) AND count($prize_final) > 0)
                            <?php $seri = 0; ?>
                            @foreach($prize_final as $item)
                                <?php $seri++; ?>
                                <tr>
                                    <td>{{ $seri }}</td>
                                    <td>{{ $item['prize_final_name'] }}</td>
                                    <td><a href="{{ url('gogi/photo/noi-dung-chi-tiet/' . $item['image_vote_id'])  }}" target="_blank"><img src="{{ url('gogi/assets/uploads/'. get_prize_type($item['image_vote_id'], 'image') ) }}" alt="" style="width: 50px; "></a></td>
                                    <td><a href="{{ route('admin.photo.prizefinal.getEdit', $item['id']) }}" class="btn btn-primary"><span class="icon icon-pencil"></span> Cập nhật</a></td>
                                </tr>
                        </tbody>
                        @endforeach
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop