@extends('admin.gogi.prize.prize')
@section('prize-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body panel-collapse table-flip-scroll">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tháng</th>
                            <th>Loại giải thưởng</th>
                            <th>Bài dự thi</th>
                            <th>Hành động</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($prizes) AND count($prizes) > 0)
                            <?php $seri = 0; ?>
                            @foreach($prizes as $prize)
                                <?php $seri++; ?>
                                <tr>
                                    <td>{{ $seri }}</td>
                                    <td>{{ $prize['month'] }}</td>
                                    <td>{{ get_pizetype_from_id($prize['prize_type_id'])  }}</td>
                                    <td><a href="{{ url('gogi/photo/noi-dung-chi-tiet/' . $prize['image_vote_id'])  }}" target="_blank"><img src="{{ url('gogi/assets/uploads/'. get_prize_type($prize['image_vote_id'], 'image') ) }}" alt="" style="width: 50px; "></a></td>
                                    <td><a href="{{ route('admin.photo.prize.getEdit', $prize['id']) }}" class="btn btn-primary"><span class="icon icon-pencil"></span> Cập nhật</a></td>
                                </tr>
                        </tbody>
                        @endforeach
                        @endif
                    </table>
                </div>
            </div>
            <!-- Pagination -->
            <div class="text-right"> {{ isset($prizes) ? $prizes->links() : '' }} </div>
            <!-- /.Pagination -->
        </div>
    </div>
@stop