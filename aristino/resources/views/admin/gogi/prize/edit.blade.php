@extends('admin.gogi.prize.prize')
@section('prize-content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div>
                <a href="{{ url('gogi/photo/prize/list')  }}" class="btn-toolbar"><span class="icon icon-angle-left"></span>
                    &nbsp;<span class="caption control-label">Trở về</span></a>
            </div>
            <div class="demo-form-wrapper">
                <form data-toggle="validator" method="post" enctype="multipart/form-data">
                    <input type='hidden' name='_token' value='{!! csrf_token() !!}'>
                    <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="icon icon-info-circle icon-lg"></span>
                        <small>Cập nhật giải thưởng</small>
                    </div>
                    <div class="form-group">
                        <label for="month" class="control-label">Tháng: {{ $prize['month'] }}</label>
                    </div>

                    <div class="form-group">
                        <label for="prize-type" class="control-label">Loại giải thưởng: {{  get_pizetype_from_id($prize['prize_type_id']) }}</label>
                    </div>

                    <div class="form-group">
                        <label for="post" class="control-label">Bài post</label>
                        <select class="custom-select" name="sltImageVote">
                            <option value="{{ $prize['image_vote_id'] }}" selected>
                                {{ $prize['image_vote_id']  }}
                            </option>
                            @if(isset($image_top) AND count($image_top) > 0)
                                @foreach ($image_top as $item)
                                    <option value="{{ $item['id'] }}">{{ $item['id'] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Cập nhật</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop