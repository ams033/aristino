<div class="layout-header">
    <div class="navbar navbar-default">
        <div class="navbar-header">
            <a class="navbar-brand navbar-brand-center" href="{{ env('APP_URL') }}" target="_blank">
                <img class="navbar-brand-logo" src="{{ url('inside/img/logo.png') }}" alt="">
            </a>
            <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#sidenav">
                <span class="sr-only">Toggle navigation</span>
                <span class="bars">
              <span class="bar-line bar-line-1 out"></span>
              <span class="bar-line bar-line-2 out"></span>
              <span class="bar-line bar-line-3 out"></span>
            </span>
                <span class="bars bars-x">
              <span class="bar-line bar-line-4"></span>
              <span class="bar-line bar-line-5"></span>
            </span>
            </button>
            <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="arrow-up"></span>
                <span class="ellipsis ellipsis-vertical">
                <img class="ellipsis-object" width="32" height="32" src="<?php echo (Auth::user()->image !== '') ? url(env('PATH_UPLOAD') . Auth::user()->image) :  url('inside/img/user_avarta.jpg') ?>" alt="{{ Auth::user()->name }}">
            </span>
            </button>
        </div>
        <div class="navbar-toggleable">
            <nav id="navbar" class="navbar-collapse collapse">
                <button class="sidenav-toggler hidden-xs" title="Collapse sidenav ( [ )" aria-expanded="true" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="bars">
                <span class="bar-line bar-line-1 out"></span>
                <span class="bar-line bar-line-2 out"></span>
                <span class="bar-line bar-line-3 out"></span>
                <span class="bar-line bar-line-4 in"></span>
                <span class="bar-line bar-line-5 in"></span>
                <span class="bar-line bar-line-6 in"></span>
              </span>
                </button>
                <ul class="nav navbar-nav navbar-right">
                    <li class="visible-xs-block">
                        <h4 class="navbar-text text-center">
                            Xin chào, {{ Auth::user()->name }}
                            </h4>
                    </li>
                    {{--<li class="hidden-xs hidden-sm">--}}
                        {{--<form class="navbar-search navbar-search-collapsed" id="search" method="get" action="{{ url('/arisition/tim-kiem') }}">--}}
                            {{--<div class="navbar-search-group">--}}
                                {{--<input class="navbar-search-input" type="search" placeholder="Hãy nhập vào email tìm kiếm&hellip;" name="search" onkeypress="if(event.keyCode==13) {this.submit();}">--}}
                                {{--<button class="navbar-search-toggler" title="Expand search form ( S )"--}}
                                        {{--aria-expanded="false" type="submit" id="input_submit_search">--}}
                                    {{--<span class="icon icon-search icon-lg"></span>--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</li>--}}

                    <!-- Notification -->
                    {{--<li class="dropdown">
                        <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true">
                  <span class="icon-with-child hidden-xs">
                    <span class="icon icon-bell-o icon-lg"></span>
                    <span class="badge badge-danger badge-above right">{{ count(Auth::user()->unreadNotifications) }}</span>
                  </span>
                            <span class="visible-xs-block">
                    <span class="icon icon-bell icon-lg icon-fw"></span>
                    <span class="badge badge-danger pull-right">{{ count(Auth::user()->unreadNotifications) }}</span>
                    Thông báo
                  </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                            <div class="dropdown-header">
                                <a class="dropdown-link" href="{{ route('admin.notification.allAsRead', Auth::id()) }}">Đánh dấu đã đọc tất cả</a>
                                <h5 class="dropdown-heading">Thông báo gần đây</h5>
                            </div>
                            <div class="dropdown-body">
                                <div class="list-group list-group-divided custom-scrollbar">
                                    @if(count(Auth::user()->unreadNotifications) > 0)
                                        @foreach(Auth::user()->unreadNotifications as $notification)
                                            <a class="list-group-item" href="/admin/notification/{{ Auth::id() }}/{{ $notification->id }}">
                                                <div class="">
                                                    <div class="notification-media">
                                                        @if($notification->data['avarta'] !== null)
                                                            <img class="rounded" width="40" height="40" src="{{ url('inside/img/'. $notification->data['avarta']) }}" alt="">
                                                        @endif
                                                    </div>
                                                    <div class="notification-content">
                                                        <small class="notification-timestamp">{{ \Carbon\Carbon::parse($notification->data['created_at']['date'])->diffForHumans(\Carbon\Carbon::now())  }}</small>
                                                        <h5 class="notification-heading">{{ $notification->data['title']  }}</h5>
                                                        <p class="notification-text">
                                                            <small class="truncate">{{ $notification->data['message']  }}</small>
                                                        </p>
                                                    </div>
                                                </div>
                                            </a>
                                        @endforeach
                                    @else
                                        <p class="list-group-item">Bạn không có thông báo nào gần đây</p>
                                    @endif
                                </div>
                            </div>
                            <div class="dropdown-footer">
                                <a class="dropdown-btn" href="{{ route('admin.notification.getList', Auth::id()) }}">Xem tất cả</a>
                            </div>
                        </div>
                    </li>--}}
                    <!-- /.Notification -->

                    <li class="dropdown hidden-xs">
                        <button class="navbar-account-btn" data-toggle="dropdown" aria-haspopup="true">
                            <img class="rounded" width="36" height="36" src="<?php echo (Auth::user()->image !== '') ? url(env('PATH_UPLOAD') . Auth::user()->image) :  url('inside/img/user_avarta.jpg') ?>" alt="{{ Auth::user()->name }}">
                            {{ Auth::user()->name }}
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">

                            <li><a href="{{ route('admin.user.getViewDetail', Auth::id()) }}">Xem thông tin</a></li>
                            {{--<li><a href="{{ route('admin.user.getEdit', Auth::id()) }}">Sửa thông tin</a></li>--}}
                            <li><a href="{{ url('/logout') }}">Thoát</a></li>
                        </ul>
                    </li>
                    <li class="visible-xs-block">
                        <a href="{{ url("admin/user/view/" . Auth::id()) }}">
                            <span class="icon icon-users icon-lg icon-fw"></span>
                            Xem thông tin
                        </a>
                    </li>
                    {{--<li class="visible-xs-block">--}}
                        {{--<a href="{{ route('admin.user.getEdit', Auth::id()) }}">--}}
                            {{--<span class="icon icon-user icon-lg icon-fw"></span>--}}
                            {{--Sửa thông tin--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    <li class="visible-xs-block">
                        <a href="{{ url('/logout') }}">
                            <span class="icon icon-power-off icon-lg icon-fw"></span>
                            Thoát
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>