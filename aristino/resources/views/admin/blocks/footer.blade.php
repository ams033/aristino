<div class="layout-footer">
    <div class="layout-footer-body">
        <small class="version">Version 1.0</small>
        <small class="copyright">2017 &copy; <span class="hidden-print"> <a href="http://ams.net.com/">AMS</a></span></small>
    </div>
</div>