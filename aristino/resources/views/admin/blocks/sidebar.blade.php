<div class="layout-sidebar">
    <div class="layout-sidebar-backdrop"></div>
    <div class="layout-sidebar-body">
        <div class="custom-scrollbar">
            <nav id="sidenav" class="sidenav-collapse collapse">
                <ul class="sidenav">
                    <li class="sidenav-search hidden-md hidden-lg">
                        <form class="sidenav-form" action="http://demo.naksoid.com/">
                            <div class="form-group form-group-sm">
                                <div class="input-with-icon">
                                    <input class="form-control" type="text" placeholder="Search…">
                                    <span class="icon icon-search input-icon"></span>
                                </div>
                            </div>
                        </form>
                    </li>
                    <li class="sidenav-heading">AMO Team</li>
                    <li class="sidenav-item  ">
                        <a href="{{ url('/admin/dashbroad') }}" >
                            <span class="sidenav-icon icon icon-home"></span>
                            <span class="sidenav-label">Bảng kiểm soát</span>
                        </a>
                    </li>

                    <li class="sidenav-item">
                        <a href="{{ route('admin.aristino.post.getList') }}" aria-haspopup="true">
                            <span class="sidenav-icon icon icon-image"></span>
                            <span class="sidenav-label"> Bài dự thi</span>
                        </a>

                    </li>

                    <li class="sidenav-item">
                        <a href="{{ route('admin.aristino.participant.getList') }}" aria-haspopup="true">
                            <span class="sidenav-icon icon icon-users"></span>
                            <span class="sidenav-label"> Người tham gia</span>
                        </a>
                    </li>

                    {{--<li class="sidenav-item">--}}
                        {{--<a href="#" aria-haspopup="true">--}}
                            {{--<span class="sidenav-icon icon icon-money"></span>--}}
                            {{--<span class="sidenav-label"> Giải thưởng</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}


                    {{--<li class="sidenav-item has-subnav">--}}
                        {{--<a href="#" aria-haspopup="true">--}}
                            {{--<span class="sidenav-icon icon icon-cogs"></span>--}}
                            {{--<span class="sidenav-label"> Nâng cao</span>--}}
                        {{--</a>--}}
                        {{--<ul class="sidenav-subnav collapse">--}}
                            {{--<li class=""><a href="">Logs hệ thống</a></li>--}}
                            {{--<li class=""><a href="{{ url('logout') }}">Thoát</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                </ul>
            </nav>
        </div>
    </div>
</div>