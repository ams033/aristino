@extends('admin.notification.notification')
@section('notification-content')
    <div class="card">
        <div class="card-body custom-scrollbar" style="">
            @if(isset($notifications) and count($notifications) > 0)
                <ul class="media-list">
                    @foreach($notifications as $notification)
                            <li class="media" {{ $notification->read_at === null ? 'noread' : 'read' }}>
                            <div class="media-middle media-left">
                                <a href="/admin/notification/{{ Auth::id() }}/{{ $notification->id }}">
                                    <img class="img-circle" width="48" height="48"
                                         src="{{ url('inside/img/'. $notification->data['avarta']) }}"
                                         alt="Daniel Taylor">
                                </a>
                            </div>
                            <div class="media-middle media-body">
                                <h5 class="media-heading">
                                    <a href="/admin/notification/{{ Auth::id() }}/{{ $notification->id }}">{{ $notification->data['title']  }}</a>
                                    <small>
                                        ({{ \Carbon\Carbon::parse($notification->data['created_at']['date'])->diffForHumans(\Carbon\Carbon::now())  }}
                                        )
                                    </small>
                                </h5>
                                <small>{{ $notification->data['message']  }}</small>
                            </div>
                        </li>
                    @endforeach
                </ul>
            @else
                <p class="media">Bạn không có thông báo nào</p>
            @endif
        </div>

    </div>
    <div class="text-right">
        {{ $notifications->links() }}
    </div>

@stop