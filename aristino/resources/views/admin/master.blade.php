<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name="description" content="">
    <meta property="og:url" content="http://ams.vn">
    <meta property="og:type" content="website">
    <meta property="og:title" content="The fastest way to build modern admin site for any platform, browser, or device">
    <meta property="og:description"
          content="Elephant is a front-end template created to help you build modern web applications, fast and in a professional manner.">
    <meta property="og:image" content="../img/ae165ef33d137d3f18b7707466aa774d.jpg">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@naksoid">
    <meta name="twitter:creator" content="@naksoid">
    <meta name="twitter:title"
          content="The fastest way to build modern admin site for any platform, browser, or device">
    <meta name="twitter:description"
          content="Elephant is a front-end template created to help you build modern web applications, fast and in a professional manner.">
    <meta name="twitter:image" content="../img/ae165ef33d137d3f18b7707466aa774d.jpg">
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="{{ url('inside/favicon-32x32.png') }}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ url('inside/favicon-16x16.png') }}" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#0288d1">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="{{ url('inside/css/vendor.minf9e3.css?v=1.1') }}">
    <link rel="stylesheet" href="{{ url('inside/css/elephant.minf9e3.css?v=1.1') }}">
    <link rel="stylesheet" href="{{ url('inside/css/application.minf9e3.css?v=1.1') }}">
    <link rel="stylesheet" href="{{ url('inside/css/demo.minf9e3.css?v=1.1') }}">
    <link rel="stylesheet" href="{{ url('inside/css/style.css') }}">
    <style>
        .navbar-brand {
            padding: 5px 15px;
        }

        .navbar-brand-logo {
            height: 40px;
            width: auto;
        }

        @media (max-width: 767px) {
            .layout-header {

                position: fixed;
            }
        }
    </style>

</head>
<body class="layout layout-header-fixed layout-sidebar-sticky">
<!-- header -->
@include('admin.blocks.header')
<!-- /.header -->

<div class="layout-main">
    <!-- sidebar -->
@include('admin.blocks.sidebar')
<!-- /.sidebar -->

    <!-- content -->
@yield('content')
<!-- /.content -->

    <!-- footer -->
@include('admin.blocks.footer')
<!-- /.footer -->
</div>

<!-- expand config -->
@include('admin.blocks.expand')
<!-- /.expand config -->

<!-- notification  -->
@if (session('flash_message'))
    <div id="toast-container" class="toast-top-right">
        <div class="toast toast-info notification" aria-live="polite" style="display: block; opacity: 0.939658;">
            <div class="toast-progress" style="width: 0%;"></div>
            <div class="toast-message">
                {!! session('flash_message') !!}
            </div>
        </div>
    </div>
@endif

@if (session('flash_message_danger'))
    <div id="toast-container" class="toast-top-right">
        <div class="toast toast-error notification" aria-live="polite" style="display: block; opacity: 0.939658;">
            <div class="toast-progress" style="width: 0%;"></div>
            <div class="toast-message">
                {!! session('flash_message_danger') !!}
            </div>
        </div>
    </div>
@endif
<!-- /.notification -->
<script>
    //@todo: global variable host
    var host = '<?php echo env("APP_URL") ?>';
</script>

<script src="{{ url('inside/js/vendor.minf9e3.js?v=1.1') }}"></script>
<script src="{{ url('inside/js/elephant.minf9e3.js?v=1.1') }}"></script>
<script src="{{ url('inside/js/application.minf9e3.js?v=1.1') }}"></script>
<script src="{{ url('inside/js/demo.minf9e3.js?v=1.1') }}"></script>
<script src="{{ url('inside/js/autoNumeric.js') }}"></script>
<script src="{{ url('inside/js/script.js') }}"></script>
<script>
    jQuery(document).ready(function ($) {
        $(".navbar-toggler").click(function () {
            if ($(document).Width() < 767)
                $(document).scrollTop(0);
        });
    })
</script>
</body>


<!-- Mirrored from demo.naksoid.com/elephant/materialistic-blue/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Dec 2016 07:49:16 GMT -->
</html>
