@extends('admin.user-perms.perm')
@section('perm-content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div>
                <a href="{{ url('admin/user/perm/list')  }}" class="btn-toolbar"><span class="icon icon-angle-left"></span>
                    &nbsp;<span class="caption control-label">Trở về</span></a>
            </div>
            <div class="demo-form-wrapper">
                @if (count($errors) > 0)
                    <div class="alert alert-danger notification">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form data-toggle="validator" method="post">
                    {{ csrf_field() }}
                    <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="icon icon-info-circle icon-lg"></span>
                        <small>Khi thêm quyền người dùng, người dùng có thể thêm chúng vào các nhóm người dùng</small>
                    </div>
                    <div class="form-group">
                        <label for="PermName" class="control-label">Quyền người dùng</label>
                        <input id="PermName" class="form-control" type="text" name="txtPerm" data-rule-required="true"
                               data-msg-required="Vui lòng nhập quyền người dùng!">
                        <small class="help-block">Tên mặc định quyền người dùng</small>
                    </div>
                    <div class="form-group">
                        <label for="displayPermName" class="control-label">Tên hiển thị</label>
                        <input id="displayPermName" class="form-control" type="text" name="txtDisPerm"
                               autocomplete="off" data-rule-required="true"
                               data-msg-required="Vui lòng nhập tên hiển thị quyền người dùng!">
                        <small class="help-block">Tên hiển thị quyền người dùng</small>
                    </div>
                    <div class="form-group">
                        <label for="biography-1" class="control-label">Mô tả</label>
                        <textarea id="biography-1" class="form-control" name="txtDescPerm" rows="3"></textarea>
                        <small class="help-block">Thêm một số thông tin chi tiết về quyền người dùng mà bạn muốn thêm (không
                            bắt buộc)
                        </small>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Nhóm người dùng</label><br/>
                        @if(isset($roles) AND $roles != null)
                            @foreach ($roles as $role)
                                <label class="custom-control custom-control-primary custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" name="roles[]" <?php echo $role['id'] == 1 ? 'checked' : '' ?> value="{{ $role['id'] }}">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-label">{{ $role['name'] }}</span>
                                </label>
                            @endforeach
                        @else
                            <span class="">Không có dữ liệu nhóm người dùng</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Lưu lại</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop