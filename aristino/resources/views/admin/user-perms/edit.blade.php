@extends('admin.user-perms.perm')
@section('perm-content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div>
                <a href="{{ url('admin/user/perm/list')  }}" class="btn-toolbar"><span class="icon icon-angle-left"></span>
                    &nbsp;<span class="caption control-label">Trở về</span></a>
            </div>
            <div class="demo-form-wrapper">
                <form data-toggle="validator" method="post">
                    {{ csrf_field() }}
                    <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="icon icon-info-circle icon-lg"></span>
                        <small>Cập nhật quyền người dùng</small>
                    </div>
                    <div class="form-group">
                        <label for="PermName" class="control-label">Quyền người dùng</label>
                        <input id="PermName" class="form-control" type="text" name="txtPerm" value="{{ $perm['name'] }}" data-rule-required="true"
                               data-msg-required="Vui lòng nhập vào tên quyền người dùng!">
                        <small class="help-block">Tên mặc định quyền người dùng</small>
                    </div>
                    <div class="form-group">
                        <label for="displayPermName" class="control-label">Tên hiển thị</label>
                        <input id="displayPermName" class="form-control" type="text" name="txtDisPerm"
                               autocomplete="off" value="{{ $perm['display_name'] }}" data-rule-required="true"
                               data-msg-required="Vui lòng nhập vào tên hiển thị quyền người dùng!">
                        <small class="help-block">Tên hiển thị quyền người dùng</small>
                    </div>
                    <div class="form-group">
                        <label for="biography-1" class="control-label">Description</label>
                        <textarea id="biography-1" class="form-control" name="txtDescPerm" rows="">@if($perm['description'] !== null){{ $perm['description'] }}@endif</textarea>
                        <small class="help-block">Thêm một số thông tin chi tiết về quyền người dùng mà bạn muốn thêm (không
                            bắt buộc)
                        </small>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Nhóm người dùng</label><br/>
                        @if(isset($roles) AND $roles != null)
                            @foreach($roles as $role)
                                <label class="custom-control custom-control-primary custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" name="rIds[]" value="{{ $role['id'] }}" <?php foreach($rolesPerm as $rolePerm){ if ($role['id'] === $rolePerm['id']){ echo 'checked'; }} ?> />
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-label">{{ $role['name'] }}</span>
                                </label>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Lưu lại</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop