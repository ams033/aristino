@extends('admin.master')
@section('title', 'Quản lý quyền người dùng | Laracore System')
@section('content')
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">
                <div class="title-bar-actions">
                    <a class="btn btn-default" href="{{ route('admin.user.perm.getAdd') }}"><span class="icon icon-plus-square"></span> Thêm mới</a>
                </div>
                <h1 class="title-bar-title">
                    <span class="d-ib">Phân quyền</span>
                </h1>
            </div>

            @yield('perm-content')

        </div>
    </div>
@stop