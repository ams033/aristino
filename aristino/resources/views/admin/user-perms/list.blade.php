@extends('admin.user-perms.perm')
@section('perm-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body panel-collapse table-flip-scroll">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Quyền hạn</th>
                            <th>Nhóm người dùng</th>
                            <th>Hành động</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($perms) > 0)
                            <?php $seri = 0; ?>
                            @foreach ($perms as $perm)
                                <?php $seri++; ?>
                                <tr>
                                    <td>{{ $seri }}</td>
                                    <td>{{ $perm['display_name'] }}</td>
                                    <td>
                                        <?php
                                        $permFromId = App\Permission::find($perm['id']);
                                        ?>
                                        <ul>
                                            @if($permFromId != null)
                                                @foreach($permFromId->roles as $role)
                                                    <li class="text-left">{{ $role['display_name'] }}</li>
                                                @endforeach
                                            @else
                                                <li><span>Không có dữ liệu</span></li>
                                            @endif
                                        </ul>

                                    </td>
                                    <td><a href="{{ route('admin.user.perm.getEdit', $perm['id']) }}"
                                           class="btn btn-primary"><span class="icon icon-pencil"></span> Cập nhật</a> <a
                                                href="{{ route('admin.user.perm.getDelete', $perm['id']) }}"
                                                class="btn btn-danger"
                                                onclick="return confirm_delete('Bạn chắc chắn muốn xóa quyền người dùng này?')"><span class="icon icon-trash-o"></span> Xóa</a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4">Không có dữ liệu</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- Pagination -->
            <div class="text-right">
                {{ $perms->links() }}
            </div>
            <!-- /.Pagination -->
        </div>
    </div>
@stop