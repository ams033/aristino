<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from demo.naksoid.com/elephant/materialistic-blue/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Dec 2016 07:51:23 GMT -->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Not Found &middot; Elephant Template &middot; The fastest way to build modern admin site for any platform, browser, or device</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name="description" content="Elephant is a front-end template created to help you build modern web applications, fast and in a professional manner.">
    <meta property="og:url" content="http://demo.naksoid.com/elephant">
    <meta property="og:type" content="website">
    <meta property="og:title" content="The fastest way to build modern admin site for any platform, browser, or device">
    <meta property="og:description" content="Elephant is a front-end template created to help you build modern web applications, fast and in a professional manner.">
    <meta property="og:image" content="../img/ae165ef33d137d3f18b7707466aa774d.jpg">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@naksoid">
    <meta name="twitter:creator" content="@naksoid">
    <meta name="twitter:title" content="The fastest way to build modern admin site for any platform, browser, or device">
    <meta name="twitter:description" content="Elephant is a front-end template created to help you build modern web applications, fast and in a professional manner.">
    <meta name="twitter:image" content="../img/ae165ef33d137d3f18b7707466aa774d.jpg">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('inside/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" href="{{ url('inside/favicon-32x32.png') }}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ url('inside/favicon-16x16.png') }}" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="{{ url('inside/safari-pinned-tab.svg') }}" color="#0288d1">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="{{ url('inside/css/vendor.minf9e3.css?v=1.1') }}">
    <link rel="stylesheet" href="{{ url('inside/css/elephant.minf9e3.css?v=1.1') }}">
    <link rel="stylesheet" href="{{ url('inside/css/errors.minf9e3.css?v=1.1') }}">
</head>
<body>
<div class="error">
    <div class="error-body">
        <h1 class="error-heading">403 - Forbidden Access is Denied</h1>
        <h4 class="error-subheading">You do not have permission to view this directory or page using the credentials that you supplied.</h4>
        <p><a class="btn btn-primary btn-pill btn-thick" href="{{ url('admin/dashbroad') }}">Return to homepage</a></p>
    </div>
    <div class="error-footer">
        <p>
            <small>© 2016 AMS</small>
        </p>
    </div>
</div>
<script src="{{ url('js/vendor.minf9e3.js?v=1.1') }}"></script>
<script src="{{ url('js/elephant.minf9e3.js?v=1.1') }}"></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-83990101-1', 'auto');
    ga('send', 'pageview');
</script>
</body>

<!-- Mirrored from demo.naksoid.com/elephant/materialistic-blue/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Dec 2016 07:51:24 GMT -->
</html>