<?php
    $login_facebook_token   =   session('login_facebook_token');
    $user_fb    = array('name'=>'');

    if(isset($login_facebook_token) ){
        $Curl_Session = 'https://graph.facebook.com/v2.2/me?fields=name,picture&access_token='.$login_facebook_token;
        $user_fb =   curlfb($Curl_Session);
    }


?>
<div class="row">
    <div class="col-md-12 col-xm-12 col-xs-12 header">
        <div class="row header-row">
            <div class="col-md-offset-1 col-md-3 col-sm-3 col-xs-6 logo navbar-header">
                <a href="{{ url('aristino/trang-chu') }}"><img src="{{ url('outside/images/logo.png') }}" class="left"></a>
            </div>
            <div class="col-md-8 col-sm-9 col-xs-6 menu-top text-right right">
                <nav class="navbar navbar-default">
                    <div class="container-fluid fluid-menu">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li><a href="{{ url('aristino/trang-chu') }}#video" class="alink">Trang chủ</a></li>
                                <li><a href="{{ url('aristino/danh-sach-bai-thi') }}" class="alink">Bài dự thi</a></li>
                                <li><a href="{{ url('aristino/the-le') }}" class="alink">Thể lệ</a></li>
                                <li><a href="{{ url('auth/facebook?ppid=0') }}"><img src="{{ url('outside/images/icon-user.png') }}"> {{ isset($user_fb['name'])?$user_fb['name']:"" }}</a></li>
                                <li>
                                    <div class="box-search">
                                        <form id="search1" class="search" method="get" action="{{ url('/aristino/tim-kiem') }}">
                                            <input type="submit" name="" value="" class="btn-search">
                                            <input type="search" name="search" class="txt-search" placeholder="SEARCH" value="{{ isset($key) ? $key : "" }}">
                                        </form>

                                    </div>
                                </li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>
            </div>
        </div>
    </div>
</div>






