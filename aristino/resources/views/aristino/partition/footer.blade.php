<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12 logo-footer-left">
                <a href="{{ url('aristino/trang-chu') }}">
                    <img src="{{ url('outside/images/logo-footer-left.png') }}">
                </a>
            </div>
            <div class="col-md-6 col-md-offset-0 col-xs-offset-0 col-xs-12 menu-footer">
                <ul>
                    <li><a href="tel:+18006226">Hotline: 18006226</a></li>
                    <li><a href="http://aristino.com/">About aristino</a></li>
                    <li><a href="https://www.facebook.com/ilovearistino/">fanpage</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 logo-footer-right">
                <a href="{{ url('aristino/trang-chu') }}"><img src="{{ url('outside/images/logo-footer.png') }}"></a>
            </div>
        </div>
    </div>
</div>