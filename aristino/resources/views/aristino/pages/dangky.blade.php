@extends('aristino.aristino')

@section('title', 'Đăng ký dự thi')

@section('content')
    <!-- slider -->
    @include('aristino.partition.slider')
    <!-- /.slider -->

    <!--form-->
    <div id="form-sign">
        <div class="container">
            <div class="row title">
                <span>Thông tin cá nhân</span>
            </div>
            <div class="list-input">
                <form action="" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <input type="text" name="txtUserName" class="txt-input"
                               placeholder="Nhập vào họ và tên của bạn..." value="{{ $user['name'] }}" required>
                    </div>
                    <div class="row">
                        <input type="text" name="txtEmail" class="txt-input" placeholder="Nhập vào email của bạn..."
                               value="{{ $user['email'] }}" required>
                    </div>
                    <div class="row">
                        <input type="text" name="txtPhone" class="txt-input"
                               placeholder="Nhập vào số điện thoại của bạn..." required
                               value="{{ isset($user['phone']) ? $user['phone'] : old('txtPhone') }}">
                    </div>
                    <div class="row">
                        <select class="select-infor" name="txtDate" required>
                            <option value="" selected readonly disabled>Ngày sinh</option>
                            <?php
                            $limit_date = 31;
                            for($i = 31; $i > 0; $i--){
                            ?>
                            <option value="{{ $i }}" <?php echo $i == date('d', strtotime($user['birthday'])) ? "selected" : "" ?>>{{ $i }}</option>
                            <?php
                            }
                            ?>
                        </select>
                        <select class="select-infor" name="txtMonth" required>
                            <option value="" selected readonly disabled>Tháng sinh</option>
                            <?php
                            $limit_date = 12;
                            for($i = 1; $i <= $limit_date; $i++){
                            ?>
                            <option value="{{ $i }}" <?php echo $i == date('m', strtotime($user['birthday'])) ? "selected" : "" ?>>Tháng {{ $i }}</option>
                            <?php
                            }
                            ?>
                        </select>
                        <select class="select-infor" name="txtYear" required>
                            <option value="" selected disabled>Năm sinh</option>
                            <?php
                                $start_year = 1970;
                                $end_year   = 2017;
                                for ($i = $end_year; $i >= $start_year ; $i--){
                                    ?>
                            <option value="{{ $i }}" <?php echo $i == date('Y', strtotime($user['birthday'])) ? "selected" : "" ?>>{{ $i }}</option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                    <div class="row">
                        <input type="text" name="txtCmnd" class="txt-input" placeholder="Nhập vào số CMND..."
                               value="{{ $user['cmnd'] }}">
                    </div>
                    <div class="row">
                        <input type="submit" class="btn-form" value="Đăng ký">
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!--/.form-->

    <a href="#form-sign" id="onclick" class="scrollto"></a>
@stop