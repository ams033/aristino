@extends('aristino.aristino')

@section('title', 'Danh sách thí sinh')

@section('content')

    <!-- slider -->
    @include('aristino.partition.slider')
    <!-- /.slider -->
    <!-- button sign up -->
    @include('aristino.partition.btn-register')
    <!-- /.button sign up -->

    <!-- list-content -->

    <div id="content-thi-sinh">
        <div class="container">
            <div class="list-content-thi-sinh">
                <div class="row">
                    <div class="col-md-4 col-xs-12 col-sm-6 top-vote">
                        <div class="title-vote-top">
                            <span1>Top 10 bình chọn</span1>
                        </div>
                        <div class="list">
                            @if(count($postTop) > 0)
                                <?php $stt = 0; ?>
                                @foreach($postTop as $post)
                                    <?php $stt++; ?>
                                    <div class="row line-code">
                                        <div class="top-thi-sinh">
                                            <div class="col-md-5 col-sm-5 col-xs-12 img">
                                                <a href="{{ url('aristino/bai-du-thi/' . $post['id']) }}">
                                                    <img src="{{ url('outside/assets/timthumb.php?src='. env('APP_URL') . '/outside/assets/'  . ($post['image3'] != '' ? $post['image3'] : $post['image2']) .'&h=150&w=150&zc=1')  }}">
                                                </a>
                                            </div>
                                            <div class="col-md-7 col-sm-7 col-xs-12 text">

                                                <label class="stt">#{{ $stt }}</label>


                                                <a href="{{ url('aristino/bai-du-thi/' . $post['id']) }}"  class="be-men">"{{ mb_strtoupper ($post['title']) }}"</a>
                                                <a href="{{ url('aristino/bai-du-thi/' . $post['id']) }}"  class="name">{{ mb_strtoupper ($post['username']) }}</a>
                                                <label class="number-vote">
                                                    <label1 class="lien-ket">
                                                        <div class="lienket-icon">
                                                            <img src="{{ url('outside/images/icon-lien-ket2.png') }}">
                                                        </div>
                                                        <div class="lienket-text">
                                                            {{ number_format($post['like']) }}
                                                        </div>
                                                    </label1>
                                                    <label1 class="lien-ket">
                                                        <div class="lienket-icon">
                                                            <img src="{{ url('outside/images/icon-heart2.png') }}">
                                                        </div>
                                                        <div class="lienket-text">
                                                            {{ number_format($post['vote']) }}
                                                        </div>
                                                    </label1>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12 col-sm-6 number-post">
                        @if(isset($exposts))


                            @foreach ($exposts as $post)
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="bai-thi" style="padding-top:0px;">
                                            <a href="{{ url('aristino/bai-du-thi/' . $post['id']) }}"><img src="{{ url('outside/assets/' . ($post['image3']!==""?$post['image3']:$post['image2'])) }}" class="img-responsive"></a>
                                            <div class="information">
                                                <p class="infor-1">
                                                    {{ mb_strtoupper ($post['title']) }}
                                                </p>
                                                <p class="infor-2">
                                                    <span1>{{ mb_strtoupper ($post['username']) }}</span1>
                                                </p>
                                                <p class="infor-3">
                                                    "{{ $post['bref_description'] }}"<br>
                                                    <a href="{{ url('aristino/bai-du-thi/' . $post['id']) }}">xem tiếp ></a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            @endforeach
                        @endif


                        @if(count($posts) > 0 AND !isset($postSearch))
                        <div class="row">
                            <div class="col-md-8 col-sm-12 col-xs-12 number-post-text">
                                Số lượng bài dự thi: {{ number_format($totalPost) }}
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 search-post">
                                <div class="box-search">
                                    <form id="search2" method="get" class="search" action="{{ url('/aristino/tim-kiem') }}">
                                        <input type="submit" name="" value="" class="btn-search">
                                        <input type="search" name="search" class="txt-search" placeholder="TÌM BÀI DỰ THI" value="{{ isset($key) ? $key : "" }}">
                                    </form>
                                </div>
                            </div>
                        </div>

                            @foreach ($posts as $post)
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="bai-thi">
                                            <a href="{{ url('aristino/bai-du-thi/' . $post['id']) }}"><img src="{{ url('outside/assets/' . ($post['image3']!==""?$post['image3']:$post['image2'])) }}" class="img-responsive"></a>
                                            <div class="information">
                                                <p class="infor-1">
                                                    {{ mb_strtoupper ($post['title']) }}
                                                </p>
                                                <p class="infor-2">
                                                    <span1>{{ mb_strtoupper ($post['username']) }}</span1>
                                                </p>
                                                <p class="infor-3">
                                                    "{{ $post['bref_description'] }}"<br>
                                                    <a href="{{ url('aristino/bai-du-thi/' . $post['id']) }}">xem tiếp ></a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            @endforeach
                                <div class="row text-right">
                                    {{--<a href="#" class="btn btn-view-more center-block"><span>Xem tiếp</span></a>--}}
                                    {{ $posts->links() }}
                                </div>
                        @endif

                        @if (isset($postSearch))
                                <div class="row">
                                    <div class="col-md-8 col-sm-12 col-xs-12 number-post-text">
                                        {{ number_format(count($postSearch)) }} kết quả tìm kiếm
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-xs-12 search-post">
                                        <form id="search3" method="get" class="search" action="{{ url('/aristino/tim-kiem') }}">
                                            <div class="box-search">
                                                <input type="submit" name="" value="" class="btn-search">
                                                <input type="search" name="search" class="txt-search" placeholder="TÌM BÀI DỰ THI" value="{{ isset($key) ? $key : "" }}">
                                            </div>
                                        </form>

                                    </div>
                                </div>

                                @foreach ($postSearch as $post)
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="bai-thi">
                                                <a href="{{ url('aristino/bai-du-thi/' . $post['id']) }}"><img src="{{ url('outside/assets/' . ($post['image3']!==""?$post['image3']:$post['image2'])) }}" class="img-responsive"></a>
                                                <div class="information">
                                                    <p class="infor-1">
                                                        {{ $post['title'] }}
                                                    </p>
                                                    <p class="infor-2">
                                                        <span1>{{ $post['username'] }}</span1>
                                                    </p>
                                                    <p class="infor-3">
                                                        "{{ $post['bref_description'] }}"<br>
                                                        <a href="{{ url('aristino/bai-du-thi/' . $post['id']) }}">xem tiếp ></a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="row text-right">
                                    {{--<a href="#" class="btn btn-view-more center-block"><span>Xem tiếp</span></a>--}}
                                    {{ $postSearch->links() }}
                                </div>
                        @endif

                    </div>

                </div>

            </div>
        </div>
    </div>
    <!-- /.list-content-->

    <a href="#content-thi-sinh" id="onclick" class="scrollto"></a>

@stop