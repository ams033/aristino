@extends('aristino.aristino')

@section('title', 'Thể lệ cuộc thi')
@section('header_int')


    <meta property="og:title" content="Thể lệ cuộc thi &quot;BE THE MAN&quot;" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="02 GIẢI NHẤT mỗi giải 20.000.000vnđ TIỀN MẶT + 1 voucher mua hàng ARISTINO trị giá 1.000.000vnđ. 05 giải nhì mỗi giải 01 voucher trị giá 2.000.000vnđ từ thương hiệu ARISTINO." />

@stop
@section('content')
        <!-- slider -->
    @include('aristino.partition.slider')
    <!-- /.slider -->

    <!-- button sign up -->
    @include('aristino.partition.btn-register')
    <!-- /.button sign up -->

    <div class="the-le-content" id="the_le">
        <div class="container">
            <div class="row">

                <div class="col-md-3 col-sm-3 col-xs-9 ">

                    <div class="menu-left">
                        <ul>
                            <li><a href="#be-the-man" class="active scrollto">Be the man</a></li>
                            <li><a href="#noi-dung-cuoc-thi" class="scrollto">Nội dung cuộc thi</a></li>
                            <li><a href="#doi-tuong-thi" class="scrollto">Đối tượng dự thi</a></li>
                            <li><a href="#the-le" class="scrollto">Thể lệ chung</a></li>
                            <li><a href="#giaithuong" class="scrollto">Hệ thống giải thưởng</a></li>
                            <li><a href="#du-thi" class="scrollto">Cách thức dự thi</a></li>
                            <li><a href="#tieu-chi-danh-gia" class="scrollto">Tiêu chí đánh giá</a></li>
                            <li><a href="#qui-dinh" class="scrollto">Quy định chung</a></li>
                        </ul>
                    </div>
                </div>



                <div class="col-md-9 col-sm-9 col-xs-12 content-right" id="thele-desktop">
                    <div id="be-the-man">
                        Thế nào là đàn ông thực thụ? Người đàn ông thực thụ có phải là một người đàn ông hoàn hảo mà mọi chàng trai đều muốn trở thành và mọi cô gái đều khao khát sở hữu. Thay vì tìm kiếm đâu xa, có bao giờ bạn dừng lại một giây ngắm nhìn những người đàn ông quanh mình: là bố, là anh trai, người chồng, người yêu, người bạn trai….  kề bên bạn mỗi ngày hay đơn giản là những hành động ga lăng của một người đàn ông bạn vô tình gặp trên đường. Họ không hoàn hảo nhưng họ có “chất” của một người đàn ông thực thụ theo cách riêng của họ. Những hành động yêu thương, chăm sóc hay giúp đỡ người khác chính là những hiện hữu của một “be the man”. Hãy cùng ARISTINO tôn vinh họ.

                        Tham gia và đồng hành cùng cuộc thi lớn nhất
                        trong năm của ARISTINO - <span>“BE THE MAN”</span>
                    </div>
                    <div id="noi-dung-cuoc-thi">
                        <span1 class="title">Nội dung cuộc thi</span1>
                        <div class="content-detail">
                            Chia sẻ những cảm xúc, những kỷ niệm hoặc nêu quan điểm về người đàn ông thực thụ của bạn
                            bằng 2 hình thức: Video và Hình ảnh kèm bài viết.
                        </div>
                    </div>

                    <div id="doi-tuong-thi">
                        <span1 class="title">Đối tượng dự thi</span1>
                        <div class="content-detail">
                            <ul>
                                <li>
                                    Tất cả công dân Việt Nam từ 15 tuổi trở lên hiện đang sinh sống và làm việc tại Việt
                                    Nam.
                                </li>
                                <li>
                                    Chấp nhận các yêu cầu của chương trình.
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div id="the-le">
                        <span1 class="title">Thể lệ chung</span1>
                        <div class="content-detail">
                            <ul>
                                <li>
                                    Thời gian đăng tải bài dự thi và bình chọn: từ ngày 11/07/2017 đến hết ngày
                                    31/07/2017.
                                </li>
                                <li>
                                    Kết quả sẽ được công bố vào ngày 10-15/08/2017 tại:
                                </li>
                                - Fanpage của ARISTINO Việt Nam http://www.facebook.com/ilovearistino
                                <br>
                                - Website page: http://www.aristino.com/bethemen.
                                <li>
                                    Người dự thi chỉ tham gia DUY NHẤT 01 vòng thi trực tuyến (online).
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div id="giaithuong">
                        <span1 class="title">Hệ thống giải thưởng</span1>
                        <div class="content-detail">
                            <ul>
                                <li><span>02 GIẢI NHẤT</span> mỗi giải <b>20.000.000</b>vnđ TIỀN MẶT + 1 voucher mua
                                    hàng ARISTINO trị giá <b>1.000.000</b>vnđ
                                </li>
                                <li><b>05 giải nhì</b> mỗi giải 01 voucher trị giá <b>2.000.000</b>vnđ từ thương hiệu
                                    ARISTINO.
                                </li>
                                <li><b>10 giải ba</b> mỗi giải 01 voucher trị giá <b>500.000vnđ</b> từ ARISTINO.</li>
                                <li>Những người <b>đăng ký tham gia</b> đều được 01 voucher trị giá <b>200.000</b>vnđ từ
                                    ARISTINO.
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div id="du-thi">
                        <span1 class="title">Cách thức dự thi</span1>
                        <div class="content-detail">
                            <b>Cách thức tham gia</b>
                            <ul>
                                <li><label>Bước 1</label>: Tạo tài khoản trên website: http://aristino.com/betheman</li>
                                <li><label>Bước 2</label>: Vào mục ĐĂNG BÀI DỰ THI để đăng tải bài dự thi.</li>
                                Bài dự thi dưới dạng:<br>
                                + Video kèm miêu tả tối đa 500 từ và 2 ảnh.<br>
                                + Bài viết kèm ảnh (3 ảnh) nói về người đàn ông thực thụ của bạn.<br>
                                <li><label>Bước 3</label>: Chia sẻ bài dự thi của mình về trang Faceboook cá nhân ở chế
                                    độ Public kèm Hashtag #Aristino #BeTheMan #contestbetheman
                                </li>
                                <li><label>Bước 4</label>: Kêu gọi bạn bè vote bài dự thi của bạn trên Website: http://aristino.com/betheman
                                </li>

                            </ul>
                        </div>
                    </div>

                    <div id="tieu-chi-danh-gia">
                        <span1 class="title">Tiêu chí đánh giá</span1>
                        <div class="content-detail">
                            <ul>
                                <li>30% điểm từ Ban Giám Khảo:</li>
                                + Thành phần BGK: đại diện thương hiệu ARISTINO<br>
                                + Tiêu chí: dựa trên chất lượng, sự đầu tư và nội dung clip nói về Người đàn ông thực
                                thụ của các bạn
                                <li>70% điểm từ Khán giả: được xét từ lượng vote trên website của chuơng trình
                                    (http://aristino.com/betheman)
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div id="qui-dinh">
                        <span1 class="title">Quy định chung</span1>
                        <div class="content-detail">
                            <ul>
                                <li>Định dạnh hình ảnh (JPEG, PNG,..dưới 5MB) hoặc Video (link Youtube)</li>
                                <li>BTC có toàn quyền sửa đổi và gỡ bỏ những tác phẩm có chứa nội dung nhạy cảm hoặc ảnh
                                    hưởng đến thuần phong mỹ tục và văn hoá Việt Nam. Nếu thí sinh đăng ảnh có nội dung
                                    không hợp lệ sẽ bị mất quyền tham dự cuộc thi mà không cần thông báo trước.
                                </li>
                                <li>BTC có toàn quyền sử dụng hình ảnh, tên tuổi của thí sinh cũng như Tác phẩm dự thi
                                    (nội dung và hình ảnh/video) trên các phương tiện truyền thông và không phải trả bất
                                    kì khoản phí nào liên quan đến quyền tác giả cho người dự thi.
                                </li>
                                <li>BTC có quyền sửa đổi nội dung chương trình và sẽ thông báo trước trên trang Facebook
                                    ARISTINO Việt Nam (http://www.facebook.com/ilovearistino)
                                </li>
                                <li>Hình ảnh không được vi phạm thuần phong mỹ tục Việt Nam</li>
                                <li>Các clip tham gia dự thi đều phải thuộc quyền sở hữu của cá nhân hoặc nhóm thí sinh
                                    tham gia dự thi. Tất cả những bài dự thi có kiện cáo tranh chấp về bản quyền sẽ
                                    không được Ban Giám Khảo đánh giá. Toàn bộ nội dung hình ảnh và âm nhạc sử dụng
                                    trong bài dự thi thí sinh chịu trách nhiệm hoàn toàn về bản quyền.
                                </li>
                                <li>Thí sinh đoạt giải phải đảm bảo tuân thủ thời gian và sự sắp xếp của BTC trong quá
                                    trình nhận giải.
                                </li>
                                <li>Thí sinh tham gia cần phải đọc và hiểu rõ thể lệ của chương trình và đồng ý tuân
                                    theo quy định bản thể lệ này.
                                </li>
                                <li>Trong trường hợp phát sinh tranh chấp, khiếu nại liên quan đến cuộc thi, BTC sẽ trực
                                    tiếp giải quyết và quyết định của BTC là kết quả cuối cùng.
                                </li>
                                <li>Người tham gia không được có thái độ, hành vi, lời nói tiêu cực về kết quả cuộc
                                    thi.
                                </li>
                                <li>Giải thưởng sẽ bị huỷ nếu trong thời gian quy định mà người trúng giải không liên hệ
                                    và hoàn tất thủ tục nhận giải.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="the-le-content-mobile">
        <div class="container">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#be-the-man2">Be the man</a>
                        </h4>
                    </div>
                    <div id="be-the-man2" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="content-detail">
                                Thế nào là đàn ông thực thụ? Người đàn ông thực thụ có phải là một người đàn ông hoàn hảo mà mọi chàng trai đều muốn trở thành và mọi cô gái đều khao khát sở hữu. Thay vì tìm kiếm đâu xa, có bao giờ bạn dừng lại một giây ngắm nhìn những người đàn ông quanh mình: là bố, là anh trai, người chồng, người yêu, người bạn trai….  kề bên bạn mỗi ngày hay đơn giản là những hành động ga lăng của một người đàn ông bạn vô tình gặp trên đường. Họ không hoàn hảo nhưng họ có “chất” của một người đàn ông thực thụ theo cách riêng của họ. Những hành động yêu thương, chăm sóc hay giúp đỡ người khác chính là những hiện hữu của một “be the man”. Hãy cùng ARISTINO tôn vinh họ.

                                Tham gia và đồng hành cùng cuộc thi lớn nhất
                                trong năm của ARISTINO - <span>“BE THE MAN”</span></div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#noi-dung-cuoc-thi2">Nội dung cuộc
                            thi</a>
                    </h4>
                </div>
                <div id="noi-dung-cuoc-thi2" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="content-detail">
                            Chia sẻ những cảm xúc, những kỷ niệm hoặc nêu quan điểm về người đàn ông thực thụ của bạn
                            bằng 2 hình thức: Video và Hình ảnh kèm bài viết.
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#doi-tuong-du-thi2">Đối tượng dự thi</a>
                    </h4>
                </div>
                <div id="doi-tuong-du-thi2" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="content-detail">
                            <ul>
                                <li>
                                    Tất cả công dân Việt Nam từ 15 tuổi trở lên hiện đang sinh sống và làm việc tại Việt
                                    Nam.
                                </li>
                                <li>
                                    Chấp nhận các yêu cầu của chương trình.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#the-le-chung-2">Thể lệ chung</a>
                    </h4>
                </div>
                <div id="the-le-chung-2" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="content-detail">
                            <ul>
                                <li>
                                    Thời gian đăng tải bài dự thi và bình chọn: từ ngày 11/07/2017 đến hết ngày
                                    31/07/2017.
                                </li>
                                <li>
                                    Kết quả sẽ được công bố vào ngày 10-15/08/2017 tại:
                                </li>
                                - Fanpage của ARISTINO Việt Nam http://www.facebook.com/ilovearistino
                                <br>
                                - Website: http://www.aristino.com/bethemen.
                                <li>
                                    Người dự thi chỉ tham gia DUY NHẤT 01 vòng thi trực tuyến (online).
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#he-thong-giai-thuong2">Hệ thống giải thưởng</a>
                    </h4>
                </div>
                <div id="he-thong-giai-thuong2" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="content-detail">
                            <ul>
                                <li><span>02 GIẢI NHẤT</span> mỗi giải <b>20.000.000</b>vnđ TIỀN MẶT + 1 voucher mua
                                    hàng ARISTINO trị giá <b>1.000.000</b>vnđ
                                </li>
                                <li><b>05 giải nhì</b> mỗi giải 01 voucher trị giá <b>2.000.000</b>vnđ từ thương hiệu
                                    ARISTINO.
                                </li>
                                <li><b>10 giải ba</b> mỗi giải 01 voucher trị giá <b>500.000vnđ</b> từ ARISTINO.</li>
                                <li>Những người <b>đăng ký tham gia</b> đều được 01 voucher trị giá <b>200.000</b>vnđ từ
                                    ARISTINO.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#cach-thuc-du-thi-2">Cách thức dự thi</a>
                    </h4>
                </div>
                <div id="cach-thuc-du-thi-2" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="content-detail">
                            <b>Cách thức tham gia</b>
                            <ul>
                                <li><label>Bước 1</label>: Tạo tài khoản trên website: http://aristino.com/betheman</li>
                                <li><label>Bước 2</label>: Vào mục ĐĂNG BÀI DỰ THI để đăng tải bài dự thi.</li>
                                Bài dự thi dưới dạng:<br>
                                + Video kèm miêu tả tối đa 500 từ và 2 ảnh.<br>
                                + Bài viết kèm ảnh (3 ảnh) nói về người đàn ông thực thụ của bạn.<br>
                                <li><label>Bước 3</label>: Chia sẻ bài dự thi của mình về trang Faceboook cá nhân ở chế
                                    độ Public kèm Hashtag #Aristino #BeTheMan #contestbetheman
                                </li>
                                <li><label>Bước 4</label>: Kêu gọi bạn bè vote bài dự thi của bạn trên Website: http://aristino.com/betheman
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#tieu-chi-danh-gia2">Tiêu chi đánh
                            giá</a>
                    </h4>
                </div>
                <div id="tieu-chi-danh-gia2" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="content-detail">
                            <ul>
                                <li>30% điểm từ Ban Giám Khảo:</li>
                                + Thành phần BGK: đại diện thương hiệu ARISTINO<br>
                                + Tiêu chí: dựa trên chất lượng, sự đầu tư và nội dung clip nói về Người đàn ông thực
                                thụ của các bạn
                                <li>70% điểm từ Khán giả: được xét từ lượng vote trên website của chuơng trình
                                    (http://aristino.com/betheman)
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#quy-dinh-chung">Quy định chung</a>
                    </h4>
                </div>
                <div id="quy-dinh-chung" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="content-detail">
                            <ul>
                                <li>Định dạnh hình ảnh (JPEG, PNG,..dưới 5MB) hoặc Video (link Youtube)</li>
                                <li>BTC có toàn quyền sửa đổi và gỡ bỏ những tác phẩm có chứa nội dung nhạy cảm hoặc ảnh
                                    hưởng đến thuần phong mỹ tục và văn hoá Việt Nam. Nếu thí sinh đăng ảnh có nội dung
                                    không hợp lệ sẽ bị mất quyền tham dự cuộc thi mà không cần thông báo trước.
                                </li>
                                <li>BTC có toàn quyền sử dụng hình ảnh, tên tuổi của thí sinh cũng như Tác phẩm dự thi
                                    (nội dung và hình ảnh/video) trên các phương tiện truyền thông và không phải trả bất
                                    kì khoản phí nào liên quan đến quyền tác giả cho người dự thi.
                                </li>
                                <li>BTC có quyền sửa đổi nội dung chương trình và sẽ thông báo trước trên trang Facebook
                                    ARISTINO Việt Nam (http://www.facebook.com/ilovearistino)
                                </li>
                                <li>Hình ảnh không được vi phạm thuần phong mỹ tục Việt Nam</li>
                                <li>Các clip tham gia dự thi đều phải thuộc quyền sở hữu của cá nhân hoặc nhóm thí sinh
                                    tham gia dự thi. Tất cả những bài dự thi có kiện cáo tranh chấp về bản quyền sẽ
                                    không được Ban Giám Khảo đánh giá. Toàn bộ nội dung hình ảnh và âm nhạc sử dụng
                                    trong bài dự thi thí sinh chịu trách nhiệm hoàn toàn về bản quyền.
                                </li>
                                <li>Thí sinh đoạt giải phải đảm bảo tuân thủ thời gian và sự sắp xếp của BTC trong quá
                                    trình nhận giải.
                                </li>
                                <li>Thí sinh tham gia cần phải đọc và hiểu rõ thể lệ của chương trình và đồng ý tuân
                                    theo quy định bản thể lệ này.
                                </li>
                                <li>Trong trường hợp phát sinh tranh chấp, khiếu nại liên quan đến cuộc thi, BTC sẽ trực
                                    tiếp giải quyết và quyết định của BTC là kết quả cuối cùng.
                                </li>
                                <li>Người tham gia không được có thái độ, hành vi, lời nói tiêu cực về kết quả cuộc
                                    thi.
                                </li>
                                <li>Giải thưởng sẽ bị huỷ nếu trong thời gian quy định mà người trúng giải không liên hệ
                                    và hoàn tất thủ tục nhận giải.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <a href="#the_le" id="onclick" class="scrollto"></a>

@stop