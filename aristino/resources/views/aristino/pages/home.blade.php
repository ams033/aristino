@extends('aristino.aristino')

@section('title', 'Trang chủ')
@section('header_int')


    <meta property="og:title" content="Cuộc thi &quot;BE THE MAN&quot; - ARISTINO" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content=" HỒ QUANG HIẾU   &quot; Đối với Hiếu, người đàn ông thực thụ là người đàn ông có khả năng mang lại hạnh phúc cho gia đình và mọi người. &quot; " />

@stop
@section('content')

    <!-- slider -->
    @include('aristino.partition.slider')
    <!-- /.slider -->


    <!-- button sign up -->
    @include('aristino.partition.btn-register')
    <!-- /.button sign up -->

    <!-- home-content -->


    <!--
    <div>

        <img src="{{ url('outside/images/home_body.jpg') }}" class="img-responsive">
    </div> -->
    <div class="container" id="home_content">
        <div class="row">
            <div class="center-block cont-1">
                <p>Theo bạn như nào là người đàn ông thực thụ?</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2 cont-2">
                <p>Chia sẻ về người đàn ông thực thụ của bạn <a href="{{ url('auth/facebook?ppid=0') }}" class="color-main">tại đây</a></p>
            </div>
        </div>
        <div class="row act-item" id="ho_quan_hieu">

            <div class="col-md-5 col-sm-5 col-xs-12 hqh_txt">
                <div class="row">
                    <div class="col-xs-4 ">
                        <img src="{{ url('outside/images/_vr.png') }}" class="img-responsive">
                    </div>
                </div>
                <p class="hqh_txt_content">
                    “ Đối với Hiếu, người đàn ông thực thụ là người đàn ông có khả năng mang lại hạnh phúc cho gia đình và mọi người.”
                </p>
                <p class='text-right' style="font-family: 'UTM Bebas'; font-size: 30px; margin-top: -20px;">- HỒ QUANG HIẾU -</p>
            </div>
            <div class="col-md-offset-1 col-md-6 col-sm-offset-1 col-sm-6 col-xs-offset-0 col-xs-12">
                <img src="{{ url('outside/images/ho_quang_hieu.png') }}" class="img-responsive">
            </div>
        </div>

        <div class="row act-item" id="duong_khac_linh">

            <div class="col-md-offset-1 col-md-5 col-sm-offset-1 col-sm-5 col-xs-12 hqh_txt pull-right">
                <div>
                    <img src="{{ url('outside/images/_vl.png') }}" class="img-responsive">
                </div>
                <p class="">
                    “ Một người đàn ông thực thụ là một người có trách nhiệm và biết chăm sóc cho gia đình.”
                </p>
                <p class='text-right' style="font-family: 'UTM Bebas'; font-size: 30px; margin-top: -20px;">- DƯƠNG KHẮC LINH -</p>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 pull-left">
                <img src="{{ url('outside/images/duong_khac_linh.png') }}" class="img-responsive">
            </div>
        </div>

        <div class="row act-item" id="hien_trinh">

            <div class="col-md-5 col-sm-5 col-xs-12 hqh_txt">
                <div>
                    <img src="{{ url('outside/images/_vr.png') }}" class="img-responsive">
                </div>
                <p class="hqh_txt_content">
                    “Người đàn ông thực thụ với Trinh là người biết yêu chiều phụ nữ, ăn mặc đẹp và “ga-lăng”.
                </p>
                <p class='text-right' style="font-family: 'UTM Bebas'; font-size: 30px; margin-top: -20px;">- HIỀN TRINH S-GIRLS -</p>
            </div>
            <div class="col-md-offset-1 col-md-6 col-sm-offset-1 col-sm-6 col-xs-12 col-xs-offset-0  ">
                <img src="{{ url('outside/images/huyen_trinh.png') }}" class="img-responsive">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 text-center ">
                <p>
                    Còn bạn thì sao?<br>
                    Hãy chia sẻ cảm nhận về người đàn ông thực thụ của bạn <a href="{{ url('auth/facebook?ppid=0') }}"  class="color-main">tại đây</a></p>
            </div>

        </div>



    </div>
    <!-- /.home-content -->

    <!-- video -->
    <a name="video"></a>
    <div class="container-fluid" id="video"  style="margin-top: 35px;">
        <div class="video-content">

            <div class="row">
                <div class="col-md-offset-1 col-md-10" >
                    <div class="embed-responsive embed-responsive-16by9 video">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/CbdT0-Tf8xE?rel=0&amp;controls=0&amp;showinfo=0&amp;enablejsapi=1"></iframe>
                    </div>
                    <p class="video-txt text-center">BE THE MAN</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <div class="embed-responsive embed-responsive-16by9 video" style="">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/OWuCVvPO_C4?rel=0&amp;controls=0&amp;showinfo=0&amp;enablejsapi=1"></iframe>
                    </div>
                    <p class="video-txt text-center">Bộ Sưu Tập Xuân Hè 2017</p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <div class="embed-responsive embed-responsive-16by9 video">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/uKZMLDcFVpw?rel=0&amp;controls=0&amp;showinfo=0&amp;enablejsapi=1"></iframe>
                    </div>
                    <p class="video-txt text-center">BE THE MAN - HỒ QUANG HIẾU</p>
                </div>
            </div>
        </div>
    </div>
    <!-- /.video -->


@stop