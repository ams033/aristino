@extends('aristino.aristino')

@section('title', 'Bài dự thi')

@section('header_int')


    <meta property="og:title" content="{{ mb_strtoupper($post['title'] )}}" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="{{ url('outside/assets/' . ($post['image3'] !== '' ? $post['image3']:$post['image2'])) }}"/>
    <meta property="og:url" content="{{ url('aristino/bai-du-thi/' . $post['id']) }}" />
    <meta property="og:description" content="{{ str_replace('"',"'",$post['bref_description']) }}" />


@stop

@section('content')

    <!-- slider -->
    @include('aristino.partition.slider')
    <!-- /.slider -->

    <!-- button sign up -->
    @include('aristino.partition.btn-register')
    <!-- /.button sign up -->

    <!--Bài dự thi-->
    <div id="bai-du-thi">
        <div class="container">
            <div class="row title-exam">
                <label>{{ $post['title'] }}</label>
            </div>
            <div class="row">
                <p class="cam-nhan text-center">“{{ $post['bref_description']}} ”</p>
            </div>
            <div id="video-cam-nhan">
                <div class="row">
                    @if($post['link_video'] !== '')
                        <div class="col-xs-12 col-xs-offset-0 col-sm-offset-2 col-sm-8">
                            <div class="embed-responsive embed-responsive-16by9 ">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $post['link_video'] }}?rel=0&amp;controls=1&amp;showinfo=0&amp;autoplay=1"></iframe>
                            </div>
                        </div>

                    @else
                        <div class="col-xs-12 col-xs-offset-0 col-sm-offset-2 col-sm-8 text-center">
                                <img src="{{ url('outside/assets/' . $post['image3']) }}" alt="" class="center-block">
                        </div>

                    @endif

                </div>
            </div>
            <div class="content-exam">
                <div class="row content-1">
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <div class="img">
                            <img src="{{ url('outside/assets/' . $post['image1']) }}">
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-7 col-xs-12">
                        <div class="text">
                            <p class="big-text">{{ $post['bref_description'] }}</p>
                            <!-- post-part1 -->
                            <?php


                                $arr_post   = explode(" ", $post['content']);
                                $post1      = "";
                                $post2      = "";

                                $arr_post_line   = explode("\n", $post['content']);
                                $countkeyword    = 0;
                                $limitchar       = 1050;

                                if(count($arr_post_line)>1){
                                    foreach($arr_post_line as $l => $line){

                                            $countkeyword   =   $countkeyword + mb_strlen($line);
                                            if($countkeyword <= $limitchar)
                                            {
                                                $post1 .= "<p>".$line . " </p>";
                                            }
                                            else {
                                                $post2 .= "<p>".$line . " </p>";
                                            }
                                    }
                                }
                                else {
                                    //X
                                    $arr_post_line   = explode(".", $post['content']);
                                    foreach($arr_post_line as $l => $line){

                                        $countkeyword   =   $countkeyword + mb_strlen($line);
                                        if($countkeyword <= $limitchar)
                                        {
                                            $post1 .= "".$line . ".";
                                        }
                                        else {
                                            $post2 .= "".$line . ".";
                                        }

                                    }
                                }




                                echo  $post1;
                            ?>
                            <!-- /.post-part1 -->
                        </div>
                    </div>
                </div>
                <div class="row content-2">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <div class="img">
                                <img src="{{ url('outside/assets/' . $post['image2']) }}" style="max-width: 85%;">
                        </div>

                    </div>
                    <div class="col-md-10">
                        <div class="bg-image2">
                            <img src="{{ url("outside/images/bg_border2.png") }}" alt="">
                        </div>
                    </div>
                    <div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8 col-xs-offset-0 col-xs-12 text">
                        <!-- post-part2 -->
                        {!! $post2 !!}
                        <!-- /.post-part2 -->
                    </div>
                </div>

                <div class="row name">
                    <div class="row title-name">
                        <label>{{ $post['username'] }}</label>
                    </div>
                </div>

                <div class="row thong-ke">
                    <div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-offset-0 col-xs-12">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6 text-center">
                                <p>Chia sẻ</p>
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(route('aristino.hien_thi_trang_bai_du_thi',$post['id'])) }}"  alt="Click vào để chia sẻ bài viết"  title="Click vào để chia sẻ bài viết"  target="_blank">
                                    <div class="icon">

                                        <img src="{{ url('outside/images/icon-lien-ket.png') }}">
                                    </div>
                                    <div class="number-thong-ke">
                                        {{ $post['like'] }}
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6  text-center">


                                @if(time() > strtotime(env('TIME_END'))+60*60*24-1)

                                        <p>Kết quả bình chọn</p>
                                        <div class="icon">
                                            <img src= "{{ url('outside/images/icon-heart.png') }}" >
                                        </div>
                                        <div class="number-thong-ke">
                                            {{ $pcount }}
                                        </div>

                                @elseif($voted==1)

                                    <p>Bình chọn</p>
                                   <div class="icon">
                                            <img src= "{{url('outside/images/icon-heart-voted.png')}} " >
                                   </div>
                                   <div class="number-thong-ke">
                                            {{ $pcount }}
                                   </div>
                                @else
                                    <p>Bình chọn</p>
                                    <a href="{{ url('/auth/facebook?ppid='. $post['id']) }}" title="Click vào để bình chọn cho bài viết"  title="Click vào để bình chọn cho bài viết">
                                        <div class="icon">
                                            <img src= "{{ url('outside/images/icon-heart.png') }}" >
                                        </div>
                                        <div class="number-thong-ke">
                                            {{ $pcount}}
                                        </div>
                                    </a>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-xs-offset-0 col-sm-offset-2 col-sm-8">
                        <div class="fb-comments" data-href="{{route('aristino.hien_thi_trang_bai_du_thi',$post['id']) }}" data-width="100%" data-numposts="10"></div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <!-- /.Bài dự thi-->
    <a href="#bai-du-thi" id="onclick"  class="scrollto"></a>

@stop