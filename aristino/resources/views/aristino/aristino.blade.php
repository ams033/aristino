<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Aristino | Be the man | @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('outside/css/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.1.1/jquery-confirm.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('outside/js/slickjs/slick/slick-theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('outside/js/slickjs/slick/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('outside/js/slickjs/slick/slick-custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('outside/inc/crop-avatar/css/cropper.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('outside/inc/crop-avatar/css/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('outside/css/style.css') }}">
    @yield('header_int')

    <script type="text/javascript">
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-102376588-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
<div id="fb-root"></div>
<script type="text/javascript">(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.9&appId=1873159176338612";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<div id="main" class="container-fluid">
    <!-- header -->
    @include('aristino.partition.header')
    <!-- /.header -->

    <!-- scroll to top -->
    <div id="scroll_to_top">
        <a href="#top" class="scrolltotop">
            <img src="{{ url('outside/images/btn-top.jpg') }}" alt="Scroll to top">
        </a>
    </div>
    <!-- /. scroll to top -->

    <!-- content -->
    @yield('content')
    <!-- /.content -->


    <!-- Footer -->
    @include('aristino.partition.footer')
    <!-- /.Footer -->
</div>

<script>
    var APP_URL = '<?php echo env("APP_URL") ?>';
</script>

<script type="text/javascript" src="{{ url('outside/js/jquery/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ url('outside/css/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ url('outside/js/jquery/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.1.1/jquery-confirm.min.js"></script>
<script type="text/javascript" src="{{ url('outside/js/slickjs/slick/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ url('outside/inc/crop-avatar/js/cropper.js') }}"></script>
<script type="text/javascript" src="{{ url('outside/inc/crop-avatar/js/main.js') }}"></script>
<script type="text/javascript" src="{{ url('outside/inc/crop-avatar/js/main2.js') }}"></script>
<script type="text/javascript" src="{{ url('outside/inc/crop-avatar/js/main3.js') }}"></script>
<script type="text/javascript" src="{{ url('outside/js/script.js') }}"></script>

</body>
</html>