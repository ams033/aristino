<?php

//@todo:  Defining Constants
const PIVOT_PART_POST   = 110;

//Set mặc định time zone Việt Nam
date_default_timezone_set('Asia/Ho_Chi_Minh');

//@todo: Authentication
Auth::routes();
Route::get('/logout', function(){
    Auth::logout();
    return redirect('/login');
});
Route::get('/register', function () {
    return redirect('/login');
});





//@todo: inside
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){

    Route::get('/', 'Admin\DashboardController@index');
    Route::get('/dashbroad', 'Admin\DashboardController@index');

    Route::group(['prefix' => 'notification'], function(){ //@todo: Notification
        Route::get('/create', function(){
            return view('admin.pages.dashbroad');
        });
        Route::get('/list/{id}', ['as' => 'admin.notification.getList', 'uses' => 'Admin\NotificationController@getList']);
        Route::get('/{id}/{notificationId}', 'Admin\NotificationController@oneAsRead');
        Route::get('/{id}', ['as' => 'admin.notification.allAsRead', 'uses' => 'Admin\NotificationController@allAsRead']);
    });

    Route::group(['prefix' => 'user', 'middleware' => ['role:admin']], function(){ //@todo: Users
        Route::get('/list', ['as' => 'admin.user.getList', 'uses' => 'Admin\UserController@getList']);
        Route::get('/add', ['as' => 'admin.user.getAdd', 'uses' => 'Admin\UserController@getAdd']);
        Route::post('/add', ['as' => 'admin.user.postAdd', 'uses' => 'Admin\UserController@postAdd']);
        Route::get('/edit/{userId}', ['as' => 'admin.user.getEdit', 'uses' => 'Admin\UserController@getEdit']);
        Route::post('/edit/{userId}', ['as' => 'admin.user.postEdit', 'uses' => 'Admin\UserController@postEdit']);
        Route::get('/delete/{userId}', ['as' => 'admin.user.getDelete', 'uses' => 'Admin\UserController@getDelete']);
        Route::get('/view/{userId}', ['as' => 'admin.user.getViewDetail', 'uses' => 'Admin\UserController@getViewDetail']);

        Route::group(['prefix' => 'role'], function(){ //@todo: Roles
            Route::get('/list', ['as' => 'admin.user.role.getList', 'uses' => 'Admin\UserController@getRoles']);
            Route::get('/add', ['as' => 'admin.user.role.getAdd', 'uses' => 'Admin\UserController@getAddRole']);
            Route::post('/add', ['as' => 'admin.user.role.postAdd', 'uses' => 'Admin\UserController@postAddRole']);
            Route::get('/edit/{roleId}', ['as' => 'admin.user.role.getEdit', 'uses' => 'Admin\UserController@getEditRole']);
            Route::post('/edit/{roleId}', ['as' => 'admin.user.role.postEdit', 'uses' => 'Admin\UserController@postEditRole']);
            Route::get('/delete/{roleId}', ['as' => 'admin.user.role.getDelete', 'uses' => 'Admin\UserController@getDeleteRole']);
            Route::get('/getPerms/{rolesId}', 'Admin\UserController@getPermsFromRoleId');
        });

        Route::group(['prefix' => 'perm'], function(){ //@todo: Permissions
            Route::get('/list', ['as' => 'admin.user.perm.getList', 'uses' => 'Admin\UserController@getPerms']);
            Route::get('/add', ['as' => 'admin.user.perm.getAdd', 'uses' => 'Admin\UserController@getAddPerm']);
            Route::post('/add', ['as' => 'admin.user.perm.postAdd', 'uses' => 'Admin\UserController@postAddPerm']);
            Route::get('/edit/{permId}', ['as' => 'admin.user.perm.getEdit', 'uses' => 'Admin\UserController@getEditPerm']);
            Route::post('/edit/{permId}', ['as' => 'admin.user.perm.postEdit', 'uses' => 'Admin\UserController@postEditPerm']);
            Route::get('/delete/{permId}', ['as' => 'admin.user.perm.getDelete', 'uses' => 'Admin\UserController@getDeletePerm']);
        });
    });
    Route::group(['prefix' => 'aristino'], function(){
        Route::group(['prefix' => 'post'], function(){
            Route::get('/list', ['as' => 'admin.aristino.post.getList', 'uses' => 'Aristino\AristinoController@get_list_post']);
            Route::get('/delete/{id}', ['as' => 'admin.aristino.post.getDelete','uses' =>  'Aristino\AristinoController@delete']);
            Route::get('/update/{id}', ['as' => 'admin.aristino.post.getUpdate', 'uses' => 'Aristino\AristinoController@getUpdate']);
            Route::post('/update/{id}', ['as' => 'admin.aristino.post.postUpdate', 'uses' => 'Aristino\AristinoController@postUpdate']);
        });

        Route::group(['prefix' => 'participant'], function(){
            Route::get('/list', ['as' => 'admin.aristino.participant.getList', 'uses' => 'Aristino\AristinoController@getParticipantList']);
        });
    });
    //@todo: logs system
    Route::group(['prefix' => 'logs', 'middleware' => ['role:admin']], function(){

    });
});

//@todo: outside
Route::get('/', 'Aristino\AristinoController@hien_thi_trang_chu');
Route::group(['prefix' => 'aristino'], function(){
    Route::get('/', 'Aristino\AristinoController@hien_thi_trang_chu');
    Route::get('/trang-chu', 'Aristino\AristinoController@hien_thi_trang_chu');
    Route::get('/dang-ky/{userId}', 'Aristino\AristinoController@hien_thi_trang_dang_ky');
    Route::post('/dang-ky/{userId}', 'Aristino\AristinoController@dang_ky');
    Route::get('/danh-sach-bai-thi', 'Aristino\AristinoController@hien_thi_trang_danh_sach_bai_thi');
    Route::get('/bai-du-thi/{id}', ['as' => 'aristino.hien_thi_trang_bai_du_thi', 'uses' => 'Aristino\AristinoController@hien_thi_trang_bai_du_thi']);
    Route::get('/tham-gia', 'Aristino\AristinoController@hien_thi_trang_tham_gia');
    Route::post('/tham-gia', 'Aristino\AristinoController@xu_ly_tham_gia');
    Route::get('/the-le', 'Aristino\AristinoController@hien_thi_trang_the_le');
    Route::get('tim-kiem', 'Aristino\AristinoController@search');

    Route::get('/update_social', 'Aristino\AristinoController@update_social');

});

//@todo Facebook API
Route::get('auth/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('auth/facebook/callback', 'Auth\LoginController@handleProviderCallback');
//Route::get('auth/facebook/callback', 'Aristino\AristinoController@hien_thi_trang_dang_ky');



Route::get('check_user_social/{postId?}', 'Aristino\AristinoController@check_social_user');


//@todo: redirect url to 404 page
Route::any('{all?}', function() {
    return view('admin.pages.404');
})->where('all', '(.*)');
