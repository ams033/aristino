//================= inc ================
//@todo: khai báo biến js toàn cục
var today = new Date();

//@todo: delay 3s hiện thông báo
$("div.notification").delay(3000).slideUp();

//@todo: xác nhận trước khi xóa
function confirm_delete(msg) {
    if (window.confirm(msg)) {
        return true;
    }
    return false;
}

//@todo: Cộng ngày
function add_days(theDate, days) {
    return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
}

//@todo: Định dạng lại date
function format_date(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
}

//@todo: auto numeric
function update_autonumeric() {
    jQuery('.autonumeric').autoNumeric("init", {
        aSep: '.',
        aDec: ',',
        aPad: false
    });
}
jQuery(function ($) {
    update_autonumeric();
});

//@todo: script for upload avarta
$(document).on('click', '#close-preview', function () {
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
            $('.image-preview').popover('show');
        },
        function () {
            $('.image-preview').popover('hide');
        }
    );
});
$(function () {
    // Create the close button
    var closebtn = $('<button/>', {
        type: "button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class", "close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger: 'manual',
        html: true,
        title: "<strong>Preview</strong>" + $(closebtn)[0].outerHTML,
        content: "There's no image",
        placement: 'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function () {
        $('.image-preview').attr("data-content", "").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("Browse");
    });
    // Create the preview image
    $(".image-preview-input input:file").change(function () {
        var img = $('<img/>', {
            id: 'dynamic',
            width: 250,
            height: 200
        });
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("Change");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content", $(img)[0].outerHTML).popover("show");
        }
        reader.readAsDataURL(file);
    });
});

//@todo: hiển thị dạng select mulitple
$(function (a) {
    function ww() {
        a(".select").select2();
    }
    ww();
});

//script for submit form search
$(document).ready(function(){
    $("input[type='search']").keypress(function(event) {
        if (event.which == 13) {
            event.preventDefault();
            $("#search").submit();
        }
    });
});

