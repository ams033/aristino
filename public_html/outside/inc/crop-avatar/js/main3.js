var proportion3  = 730/412;

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as anonymous module.
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node / CommonJS
        factory(require('jquery'));
    } else {
        // Browser globals.
        factory(jQuery);
    }
})(function ($) {

    'use strict';

    var console = window.console || { log: function () {} };

    function CropAvatar($element) {
        this.$container = $element;

        this.$avatarView3 = this.$container.find('.avatar-view3');
        this.$avatar3 = this.$avatarView3.find('img');
        this.$avatarModal3 = this.$container.find('#avatar-modal3');
        this.$loading3 = this.$container.find('.loading3');

        this.$avatarForm3 = this.$avatarModal3.find('.avatar-form3');
        this.$avatarUpload3 = this.$avatarForm3.find('.avatar-upload3');
        this.$avatarSrc3 = this.$avatarForm3.find('.avatar-src3');
        this.$avatarData3 = this.$avatarForm3.find('.avatar-data3');
        this.$avatarInput3 = this.$avatarForm3.find('.avatar-input3');
        this.$avatarSave3 = this.$avatarForm3.find('.avatar-save3');
        this.$avatarBtns3 = this.$avatarForm3.find('.avatar-btns3');

        this.$avatarWrapper3 = this.$avatarModal3.find('.avatar-wrapper3');
        this.$avatarPreview3 = this.$avatarModal3.find('.avatar-preview3');

        this.init();
    }

    CropAvatar.prototype = {
        constructor: CropAvatar,

        support: {
            fileList: !!$('<input type="file">').prop('files'),
            blobURLs: !!window.URL && URL.createObjectURL,
            formData: !!window.FormData
        },

        init: function () {
            this.support.datauri = this.support.fileList && this.support.blobURLs;

            if (!this.support.formData) {
                this.initIframe();
            }

            this.initTooltip();
            this.initModal();
            this.addListener();
        },

        addListener: function () {
            this.$avatarView3.on('click', $.proxy(this.click, this));
            this.$avatarInput3.on('change', $.proxy(this.change, this));
            this.$avatarForm3.on('submit', $.proxy(this.submit, this));
            this.$avatarBtns3.on('click', $.proxy(this.rotate, this));
        },

        initTooltip: function () {
            this.$avatarView3.tooltip({
                placement: 'bottom'
            });
        },

        initModal: function () {
            this.$avatarModal3.modal({
                show: false
            });
        },

        initPreview: function () {
            var url = this.$avatar3.attr('src');

            this.$avatarPreview3.html('<img src="' + url + '">');
        },

        initIframe: function () {
            var target = 'upload-iframe-' + (new Date()).getTime();
            var $iframe = $('<iframe>').attr({
                name: target,
                src: ''
            });
            var _this = this;

            // Ready ifrmae
            $iframe.one('load', function () {

                // respond response
                $iframe.on('load', function () {
                    var data;

                    try {
                        data = $(this).contents().find('body').text();
                    } catch (e) {
                        console.log(e.message);
                    }

                    if (data) {
                        try {
                            data = $.parseJSON(data);
                        } catch (e) {
                            console.log(e.message);
                        }

                        _this.submitDone(data);
                    } else {
                        _this.submitFail('Image upload failed!');
                    }

                    _this.submitEnd();

                });
            });

            this.$iframe = $iframe;
            this.$avatarForm3.attr('target', target).after($iframe.hide());
        },

        click: function () {
            this.$avatarModal3.modal('show');
            this.initPreview();
        },

        change: function () {
            var files;
            var file;

            if (this.support.datauri) {
                files = this.$avatarInput3.prop('files');

                if (files.length > 0) {
                    file = files[0];

                    if (this.isImageFile(file)) {
                        if (this.url) {
                            URL.revokeObjectURL(this.url); // Revoke the old one
                        }

                        this.url = URL.createObjectURL(file);
                        this.startCropper();
                    }
                }
            } else {
                file = this.$avatarInput3.val();

                if (this.isImageFile(file)) {
                    this.syncUpload();
                }
            }
        },

        submit: function () {
            if (!this.$avatarSrc3.val() && !this.$avatarInput3.val()) {
                return false;
            }

            if (this.support.formData) {
                this.ajaxUpload();
                return false;
            }
        },

        rotate: function (e) {
            var data;

            if (this.active) {
                data = $(e.target).data();

                if (data.method) {
                    this.$img.cropper(data.method, data.option);
                }
            }
        },

        isImageFile: function (file) {
            if (file.type) {
                return /^image\/\w+$/.test(file.type);
            } else {
                return /\.(jpg|jpeg|png|gif)$/.test(file);
            }
        },

        startCropper: function () {
            var _this = this;

            if (this.active) {
                this.$img.cropper('replace', this.url);
            } else {
                this.$img = $('<img src="' + this.url + '">');
                this.$avatarWrapper3.empty().html(this.$img);
                this.$img.cropper({
                    aspectRatio: proportion3,
                    preview: this.$avatarPreview3.selector,
                    crop: function (e) {
                        var json = [
                            '{"x":' + e.x,
                            '"y":' + e.y,
                            '"height":' + e.height,
                            '"width":' + e.width,
                            '"rotate":' + e.rotate + '}'
                        ].join();

                        _this.$avatarData3.val(json);
                    }
                });

                this.active = true;
            }

            this.$avatarModal3.one('hidden.bs.modal', function () {
                _this.$avatarPreview3.empty();
                _this.stopCropper();
            });
        },

        stopCropper: function () {
            if (this.active) {
                this.$img.cropper('destroy');
                this.$img.remove();
                this.active = false;
            }
        },

        ajaxUpload: function () {
            var url = this.$avatarForm3.attr('action');
            var data = new FormData(this.$avatarForm3[0]);
            var _this = this;

            $.ajax(url, {
                type: 'post',
                data: data,
                dataType: 'json',
                processData: false,
                contentType: false,

                beforeSend: function () {
                    _this.submitStart();
                },

                success: function (data) {
                    _this.submitDone(data);
                },

                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    _this.submitFail(textStatus || errorThrown);
                },

                complete: function () {
                    _this.submitEnd();
                }
            });
        },

        syncUpload: function () {
            this.$avatarSave3.click();
        },

        submitStart: function () {
            this.$loading3.fadeIn();
        },

        submitDone: function (data) {
            console.log(data);

            if ($.isPlainObject(data) && data.state === 200) {
                if (data.result) {
                    this.url = data.result;

                    if (this.support.datauri || this.uploaded) {
                        this.uploaded = false;
                        this.cropDone();
                    } else {
                        this.uploaded = true;
                        this.$avatarSrc3.val(this.url);
                        this.startCropper();
                    }

                    this.$avatarInput3.val('');
                    $("#khung_img_video img").attr("src", 'http://betheman.aristino.com/outside/assets/'+this.url);
                    $("#hidden_crop_avatar3").val(this.url);
                } else if (data.message) {
                    this.alert(data.message);
                }
            } else {
                this.alert('Failed to response');
            }
        },

        submitFail: function (msg) {
            this.alert(msg);
        },

        submitEnd: function () {
            this.$loading3.fadeOut();
        },

        cropDone: function () {
            this.$avatarForm3.get(0).reset();
            this.$avatar3.attr('src', this.url);
            this.stopCropper();
            this.$avatarModal3.modal('hide');
        },

        alert: function (msg) {
            var $alert = [
                '<div class="alert alert-danger avatar-alert alert-dismissable">',
                '<button type="button" class="close" data-dismiss="alert">&times;</button>',
                msg,
                '</div>'
            ].join('');

            this.$avatarUpload3.after($alert);
        }
    };

    $(function () {
        return new CropAvatar($('#crop-avatar3'));
    });

});
