<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Aristino | Trang chủ</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../js/slickjs/slick/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="../js/slickjs/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="../js/slickjs/slick/slick-custom.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
</head>
<body>
<div id="main" class="container-fluid">
    <!-- header -->
    <?php include_once '../partition/header.php' ?>
    <!-- /.header -->

    <!-- slider -->
    <?php include_once '../partition/slider.php' ?>
    <!-- /.slider -->
    <!-- button sign up -->
    <div class="row">
        <a href="#" class="btn btn-register center-block"><span>ĐĂNG BÀI DỰ THI</span></a>
    </div>
    <!-- /.button sign up -->

    <!-- list-content -->
    <div id="content-thi-sinh">
        <div class="container">
            <div class="list-content-thi-sinh">
                <div class="row">
                    <div class="col-md-4 col-xs-12 col-sm-6 top-vote">
                        <div class="title-vote-top">
                            <span1>Top 10 bình chọn</span1>
                        </div>
                        <div class="list">
                            <div class="row line-code">

                                <div class="top-thi-sinh">
                                    <div class="col-md-6 col-sm-6 col-xs-12 img">
                                        <img src="../images/danh-sach-thi-sinh.jpg">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 text">
                                        <label class="stt">#1</label>
                                        <label class="be-men">Be the men</label>
                                        <label class="name">Nguyễn Văn A</label>
                                        <label class="number-vote">
                                            <label1 class="lien-ket">
                                                <div class="lienket-icon">
                                                    <img src="../images/icon-lien-ket2.png">
                                                </div>
                                                <div class="lienket-text">
                                                    67000
                                                </div>
                                            </label1>
                                            <label1 class="lien-ket">
                                                <div class="lienket-icon">
                                                    <img src="../images/icon-heart2.png">
                                                </div>
                                                <div class="lienket-text">
                                                    %&G""""
                                                </div>
                                            </label1>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row line-code">
                                <div class="top-thi-sinh">
                                    <div class="img">
                                        <img src="../images/danh-sach-thi-sinh.jpg">
                                    </div>
                                    <div class="text">
                                        <label class="stt">#1</label>
                                        <label class="be-men">Be the men</label>
                                        <label class="name">Nguyễn Văn A</label>
                                        <label class="number-vote">
                                            <label1 class="lien-ket">
                                                <div class="lienket-icon">
                                                    <img src="../images/icon-lien-ket2.png">
                                                </div>
                                                <div class="lienket-text">
                                                    67000
                                                </div>
                                            </label1>
                                            <label1 class="lien-ket">
                                                <div class="lienket-icon">
                                                    <img src="../images/icon-heart2.png">
                                                </div>
                                                <div class="lienket-text">
                                                    %&G""""
                                                </div>
                                            </label1>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row line-code">
                                <div class="top-thi-sinh">
                                    <div class="img">
                                        <img src="../images/danh-sach-thi-sinh.jpg">
                                    </div>
                                    <div class="text">
                                        <label class="stt">#1</label>
                                        <label class="be-men">Be the men</label>
                                        <label class="name">Nguyễn Văn A</label>
                                        <label class="number-vote">
                                            <label1 class="lien-ket">
                                                <div class="lienket-icon">
                                                    <img src="../images/icon-lien-ket2.png">
                                                </div>
                                                <div class="lienket-text">
                                                    67000
                                                </div>
                                            </label1>
                                            <label1 class="lien-ket">
                                                <div class="lienket-icon">
                                                    <img src="../images/icon-heart2.png">
                                                </div>
                                                <div class="lienket-text">
                                                    %&G""""
                                                </div>
                                            </label1>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row line-code">
                                <div class="top-thi-sinh">
                                    <div class="img">
                                        <img src="../images/danh-sach-thi-sinh.jpg">
                                    </div>
                                    <div class="text">
                                        <label class="stt">#1</label>
                                        <label class="be-men">Be the men</label>
                                        <label class="name">Nguyễn Văn A</label>
                                        <label class="number-vote">
                                            <label1 class="lien-ket">
                                                <div class="lienket-icon">
                                                    <img src="../images/icon-lien-ket2.png">
                                                </div>
                                                <div class="lienket-text">
                                                    67000
                                                </div>
                                            </label1>
                                            <label1 class="lien-ket">
                                                <div class="lienket-icon">
                                                    <img src="../images/icon-heart2.png">
                                                </div>
                                                <div class="lienket-text">
                                                    %&G""""
                                                </div>
                                            </label1>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 col-xs-12 col-sm-6 number-post">
                        <div class="row">
                            <div class="col-md-8 col-sm-12 col-xs-12 number-post-text">
                                Số lượng bài dự thi: 10000
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 search-post">
                                <div class="box-search">
                                    <input type="submit" name="" value="" class="btn-search">
                                    <input type="text" name="" class="txt-search" placeholder="TÌM BÀI DỰ THI">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="bai-thi">
                                <img src="../images/danh-sach-thi-sinh.jpg">
                                <div class="information">
                                    <p class="infor-1">
                                        !@#$%^&***
                                    </p>
                                    <p class="infor-2">
                                        <span1>Hồ Quang Hiếu</span1>
                                    </p>
                                    <p class="infor-3">
                                        "Anh ấy trong tôi không phải là một con người hoàn hảo mà là sự khuyết điểm để
                                        trở thành người đàn ông của đời tôi...."<br>
                                        xem tiếp >
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="bai-thi">
                                <img src="../images/danh-sach-thi-sinh.jpg">
                                <div class="information">
                                    <p class="infor-1">
                                        !@#$%^&***
                                    </p>
                                    <p class="infor-2">
                                        <span1>Hồ Quang Hiếu</span1>
                                    </p>
                                    <p class="infor-3">
                                        "Anh ấy trong tôi không phải là một con người hoàn hảo mà là sự khuyết điểm để
                                        trở thành người đàn ông của đời tôi...."<br>
                                        xem tiếp >
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="bai-thi">
                                <img src="../images/danh-sach-thi-sinh.jpg">
                                <div class="information">
                                    <p class="infor-1">
                                        !@#$%^&***
                                    </p>
                                    <p class="infor-2">
                                        <span1>Hồ Quang Hiếu</span1>
                                    </p>
                                    <p class="infor-3">
                                        "Anh ấy trong tôi không phải là một con người hoàn hảo mà là sự khuyết điểm để
                                        trở thành người đàn ông của đời tôi...."<br>
                                        xem tiếp >
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <a href="#" class="btn btn-view-more center-block"><span>Xem tiếp</span></a>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <!-- /.list-content-->

    <!-- scroll to top -->
    <div id="scroll_to_top">
        <a href="#top" class="scrolltotop">
            <img src="../images/btn-top.jpg" alt="Scroll to top">
        </a>
    </div>
    <!-- /. scroll to top -->

    <!-- Footer -->
    <?php include_once '../partition/footer.php'; ?>
    <!-- /.Footer -->
</div>

<script type="text/javascript" src="../js/jquery/jquery.min.js"></script>
<script type="text/javascript" src="../css/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/slickjs/slick/slick.min.js"></script>
<script type="text/javascript" src="../js/script.js"></script>
</body>
</html>