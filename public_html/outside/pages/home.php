<?php
    include_once '../index.php';
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Aristino | Trang chủ</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../js/slickjs/slick/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="../js/slickjs/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="../js/slickjs/slick/slick-custom.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
<div id="main" class="container-fluid">
    <!-- header -->
    <?php include_once '../partition/header.php' ?>
    <!-- /.header -->

    <!-- slider -->
    <?php include_once '../partition/slider.php' ?>
    <!-- /.slider -->

    <!-- button sign up -->
    <div class="row">
        <a href="#" class="btn btn-register center-block"><span>ĐĂNG BÀI DỰ THI</span></a>
    </div>
    <!-- /.button sign up -->

    <!-- video -->
    <div class="container" >
        <div class="video-content row">
            <div class="col-md-4">
                <div class="embed-responsive embed-responsive-16by9 video">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/CFqD14ybRr0"></iframe>
                </div>
                <p class="video-txt text-center">MV ÂM NHẠC</p>
            </div>
            <div class="col-md-4">
                <div class="embed-responsive embed-responsive-16by9 video">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/CFqD14ybRr0"></iframe>
                </div>
                <p class="video-txt text-center">VIRAL VIDEO</p>
            </div>
            <div class="col-md-4">
                <div class="embed-responsive embed-responsive-16by9 video">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/CFqD14ybRr0"></iframe>
                </div>
                <p class="video-txt text-center">MV ÂM NHẠC</p>
            </div>
        </div>
    </div>
    <!-- /.video -->

    <!-- scroll to top -->
    <div id="scroll_to_top">
        <a href="#top" class="scrolltotop">
            <img src="../images/btn-top.jpg" alt="Scroll to top">
        </a>
    </div>
    <!-- /. scroll to top -->
</div>

<!-- Footer -->
<?php include_once '../partition/footer.php'; ?>
<!-- /.Footer -->

<script type="text/javascript" src="../js/jquery/jquery.min.js"></script>
<script type="text/javascript" src="../css/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/slickjs/slick/slick.min.js"></script>
<script type="text/javascript" src="../js/script.js"></script>

</body>
</html>