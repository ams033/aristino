<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Aristino | Đăng ký dự thi</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../js/slickjs/slick/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="../js/slickjs/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="../js/slickjs/slick/slick-custom.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
	<div id="main" class="container-fluid">
	    <!-- header -->
	    <?php include_once '../partition/header.php' ?>
	    <!-- /.header -->

	    <div class="row">
            <div class="slider">
                <!-- Wrapper for slides -->
                <div class="item active">
                    <img src="../images/banner-slider.jpg" alt="...">
                    <div class="carousel-caption">
                    </div>
                </div>

                <div class="item active">
                    <img src="../images/banner-slider.jpg" alt="...">
                    <div class="carousel-caption">
                    </div>
                </div>


                <div class="item active">
                    <img src="../images/banner-slider.jpg" alt="...">
                    <div class="carousel-caption">
                    </div>
                </div>

                <div class="item active">
                    <img src="../images/banner-slider.jpg" alt="...">
                    <div class="carousel-caption">
                    </div>
                </div>

                <div class="item active">
                    <img src="../images/banner-slider.jpg" alt="...">
                    <div class="carousel-caption">
                    </div>
                </div>
            </div>
        </div>
	    <!--form-->	
	    <div id="form-sign">
	    	<div class="container">
	    		<div class="row title">
	    			<span>Thông tin cá nhân</span>
	    		</div>
	    		<div class="list-input">
	    			<div class="row">
	    				<input type="text" name="" class="txt-input" placeholder="họ và tên">
	    			</div>
	    			<div class="row">
	    				<input type="text" name="" class="txt-input" placeholder="Điện thoại">
	    			</div>
	    			<div class="row">
	    				<input type="text" name="" class="txt-input" placeholder="Email">
	    			</div>
	    			<div class="row">
	    				<select class="select-infor">
	    					<option>Ngày sinh</option>
	    				</select>
	    				<select class="select-infor">
	    					<option>Tháng</option>
	    				</select>
	    				<select class="select-infor">
	    					<option>Năm</option>
	    				</select>
	    			</div>
	    			<div class="row">
	    				<input type="text" name="" class="txt-input" placeholder="Số cmnd">
	    			</div>
	    			<div class="row">
	    				<input type="submit" name="" class="btn-form" value="Đăng ký">
	    			</div>
	    		</div>
	    	</div>
	    </div>
	    <!--/.form-->

        <!-- scroll to top -->
        <div id="scroll_to_top">
            <a href="#top" class="scrolltotop">
                <img src="../images/btn-top.jpg" alt="Scroll to top">
            </a>
        </div>
        <!-- /. scroll to top -->

	    <!-- Footer -->
	    <?php include_once '../partition/footer.php'; ?>
	    <!-- /.Footer -->
	</div>

	<script type="text/javascript" src="../js/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../css/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/slickjs/slick/slick.min.js"></script>
	<script type="text/javascript" src="../js/script.js"></script>
</body>
</html>