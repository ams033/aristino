function count_words_in_string(str) {
    return str.trim().split(/\s+/).length;
}
function hasEmptyElement(array) {
    $check = false;
    for (var i = 0; i < array.length; i++) {
        if (array[i] == '') {
            $check = true;
            break;
        }
    }
    return $check;
}
function is_url(str) {
    regexp = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
    if (regexp.test(str)) {
        return true;
    } else {
        return false;
    }
}

function YouTubeGetID(url){
    var ID = '';
    url = url.replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
    if(url[2] !== undefined) {
        ID = url[2].split(/[^0-9a-z_\-]/i);
        ID = ID[0];
    }
    else {
        ID = url;
    }


    return ID;
}

//================================================================

var number_pivot_partition_post = 110;
$(document).ready(function ($) {

    //@todo Slider run
    $(".slider").slick({
        dots: false,
        autoplay: true,
        slidesToScroll: 1,
        arrows: true,
    });

    //@todo visited link
    $('.menu-left ul li a').click(function () {
        $('.menu-left ul li a').removeClass('active');
        $(this).addClass('active');
    });


    //@todo input data form tham gia
    $("#title_content").keypress(function (event) {
        $('.has-error').empty();
        var title = $('#title_content').val().trim();
        if (title.length > 60) {  //@todo Nếu title > 60 ký tự => error
            event.preventDefault();
            $('.has-error').text('Tiêu đề của bạn đã vượt qua 60 ký tự!');
            return false;
        }
    });
    $("#btn_insert_title").click(function () {    //@todo Insert title
        var title = $('#title_content').val().trim();
        if (title.length == 0) { //@todo Không để tiêu đề trống
            $('.has-error').text('Vui lòng nhập vào tiêu đề của bạn');
            return false;
        }
        $('.setTitle').text(title);
        $('#setTitle').val(title);
        $('.has-error').empty();
        $('.btn-close').click();

    });

    //@todo insert feel short
    $("#feel_short_content").keypress(function (event) {
        $('.has-error').empty();
        var feel_short_content = $("#feel_short_content").val().trim();
        if (count_words_in_string(feel_short_content) > 50) {
            event.preventDefault();
            $('.has-error').text('Cảm nhận của bạn không vượt quá 50 từ ~ 3 câu!');
            return false;
        }
    });
    $("#btn_insert_feel_short").click(function () {
        var feel_short_content = $("#feel_short_content").val().trim();
        if (feel_short_content.length == 0) {
            $('.has-error').text('Vui lòng nhập vào cảm nhận của bạn!');
            return false;
        }
        $('._feelShort').text(feel_short_content);
        $('#set_feel_short').val(feel_short_content);
        $('.has-error').empty();
        $('.btn-close').click();
    });

    //@todo insert post
    $("#post_content").keypress(function (event) {
        $(".has-error").empty();
        var post_content = $("#post_content").val().trim();

        if (count_words_in_string(post_content) > 500) {
            event.preventDefault();
            $('.has-error').text('Bài viết của bạn không vượt quá 500 từ!');
            return false;
        }
    });
    $("#btn_insert_post").on('click', function () {
        var post_content = $("#post_content").val().trim();
        var arr_post = post_content.split(" ");
        var arr_post_line = post_content.split("\n");
        var post1 = '';
        var post2 = '';

        if (post_content.length == 0) {
            $('.has-error').text('Vui lòng nhập vào bài viết của bạn!');
            return false;
        }
        $("#set_post").val(post_content);

        if (count_words_in_string(post_content) > number_pivot_partition_post) {
            if (hasEmptyElement(arr_post_line)) {
                // if (arr_post_line[0].length > number_pivot_partition_post) {
                //     for (var i = 0; i < number_pivot_partition_post; i++) {
                //         post1 += (arr_post[i] + ' ');
                //     }
                //     $("#post1").text(post1 + '...');
                //     for (var j = number_pivot_partition_post; j < arr_post.length; j++) {
                //         post2 += (arr_post[j] + ' ');
                //     }
                //     $("#post2").text(post2);
                // } else {
                //     $("#post1").text(arr_post_line[0] + '...');
                //     for (var i = 2; i < arr_post_line.length; i++) {
                //         post2 += (arr_post_line[i] + ' ');
                //     }
                //     $("#post2").text(post2);
                // }

                $("#post1").text(arr_post_line[0]);
                for (var i = 2; i < arr_post_line.length; i++) {
                    post2 += (arr_post_line[i] + ' ');
                }
                $("#post2").text(post2);
            } else {
                for (var i = 0; i < number_pivot_partition_post; i++) {
                    post1 += (arr_post[i] + ' ');
                }
                $("#post1").text(post1);
                for (var j = number_pivot_partition_post; j < arr_post.length; j++) {
                    post2 += (arr_post[j] + ' ');
                }
                $("#post2").text(post2);
            }
        } else {
            $("#post1").text(post_content);
        }
        $('.btn-close').click();
    });

    //@todo Upload Image
    $("#btn_upload_img1").click(function () {
        $(".avatar-view").click();
        $(".avatar-upload .avatar-input").click();
    });
    $("#btn_upload_img2").click(function () {
        $(".avatar-view2").click();
        $(".avatar-upload2 .avatar-input2").click();
    });
    $("#btn_upload_img3").click(function () {
        $(".btn-close-img3").click();
        $(".avatar-view3").click();
        $(".avatar-upload3 .avatar-input3").click();
    });
    $("#btn_upload_video").click(function () {
        $(".btn-close-img3").click();
    });
    $("#btn_insert_link_video").click(function () {
        var link = $("#video_link").val();
        var youtubeid   =   YouTubeGetID(link);
        if (link.length == 0) {
            $(".has-error").text("Vui lòng nhập vào link video của bạn!");
            return false;
        }
        if (!is_url(link)) {
            $(".has-error").text("Vui lòng nhập vào một URL hợp lệ!");
            return false;
        }
        $("#khung_img_video").empty().append('<div class="embed-responsive embed-responsive-16by9">' +
            '<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'+youtubeid+'" allowfullscreen></iframe>' +
            '</div>');
        $(".btn-close-link-video").click();
        $(".has-error").empty();
    });

});

// Show or hide the sticky footer button
$(window).scroll(function () {
    if ($(this).scrollTop() > 200) {
        $('.scrolltotop').fadeIn(200);
    } else {
        $('.scrolltotop').fadeOut(200);
    }
});
// Animate the scroll to top
$('.scrolltotop').click(function (event) {
    event.preventDefault();
    $('html, body').animate({scrollTop: 0}, 500);
});

$('.scrollto').click(function () {
    var $target = $(this.hash);
    $("html, body").animate({scrollTop: $target.offset().top - 65}, 1000);
    return false;
});

jQuery(document).ready(function($) {
    //$('#youtube1').mediaelementplayer();


});









