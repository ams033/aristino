<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Aristino | Thể lệ cuộc thi</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../js/slickjs/slick/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="../js/slickjs/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="../js/slickjs/slick/slick-custom.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
<div id="main" class="container-fluid">
    <!-- header -->
    <?php include_once '../partition/header.php' ?>
    <!-- /.header -->


    <!-- slider -->
    <?php include_once '../partition/slider.php' ?>
    <!-- /.slider -->
    <!-- button sign up -->
    <div class="row">
        <a href="#" class="btn btn-register center-block"><span>ĐĂNG BÀI DỰ THI</span></a>
    </div>
    <!-- /.button sign up -->

    <!-- scroll to top -->
    <div id="scroll_to_top">
        <a href="#top" class="scrolltotop">
            <img src="../images/btn-top.jpg" alt="Scroll to top">
        </a>
    </div>
    <!-- /. scroll to top -->

    <!-- The le-->
    <div class="the-le-content">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 menu-left">
                    <ul>
                        <li><a href="#be-the-man" class="active scrollto">Be the men</a></li>
                        <li><a href="#noi-dung-cuoc-thi" class="scrollto">Nội dung cuộc thi</a></li>
                        <li><a href="#doi-tuong-thi" class="scrollto">Đối tượng dự thi</a></li>
                        <li><a href="#the-le" class="scrollto">Thể lệ chung</a></li>
                        <li><a href="#giaithuong" class="scrollto">Hệ thống giải thưởng</a></li>
                        <li><a href="#du-thi" class="scrollto">Cách thức dự thi</a></li>
                        <li><a href="#qui-dinh" class="scrollto">Qui định chung</a></li>
                    </ul>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-9 content-right">
                    <div id="be-the-man">
                        Thế nào là đàn ông thực thụ? Người đàn ông thực thụ có phải là một người đàn ông hoàn hảo mà mọi
                        chàng trai đều muốn trở thành và mọi cô gái đều khao khát sở hữu.
                        Ngừng mơ mộng và đừng tìm kiếm đâu xa bởi chúng tôi nhìn thấy người đàn ông thực thụ đang ở ngay
                        bên cạnh bạn. Đó chính là người cha, người anh trai, em trai, anh người yêu, thằng bạn thân,
                        …hay một người đàn ông xa lạ nào đó đã giúp đỡ bạn hoặc giúp đỡ người khác mà bạn chứng kiến. Họ
                        không hoàn hảo nhưng họ có cái “chất” của một người đàn ông thực thụ theo cách riêng của họ.
                        Những hành động yêu thương, chăm sóc hay giúp đỡ người khác chính là những hiện hữu của một “be
                        the man”.
                        Hãy cùng ARISTINO tôn vinh họ. Tham gia và đồng hành cùng cuộc thi lớn nhất và duy nhất trong
                        năm của ARISTINO - <span>“BE THE MAN”</span>
                    </div>
                    <div id="noi-dung-cuoc-thi">
                        <span1 class="title">Nội dung cuộc thi</span1>
                        <div class="content-detail">
                            Chia sẻ những cảm xúc, những kỷ niệm hoặc nêu quan điểm về người đàn ông thực thụ của bạn
                            bằng 2 hình thức: Video và Hình ảnh kèm bài viết.
                        </div>
                    </div>

                    <div id="doi-tuong-thi">
                        <span1 class="title">Đối tượng dự thi</span1>
                        <div class="content-detail">
                            <ul>
                                <li>
                                    Tất cả công dân Việt Nam từ 18 tuổi trở lên hiện đang sinh sống và làm việc tại Việt
                                    Nam.
                                </li>
                                <li>
                                    Chấp nhận các yêu cầu của chương trình.
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div id="the-le">
                        <span1 class="title">Thể lệ chung</span1>
                        <div class="content-detail">
                            <ul>
                                <li>
                                    Thời gian đăng tải bài dự thi và bình chọn: từ ngày 08/07/2013 đến hết ngày
                                    30/07/2017.
                                </li>
                                <li>
                                    Kết quả sẽ được công bố vào ngày 10-15/08/2017 tại:
                                </li>
                                - Fanpage của ARISTINO Việt Nam https://www.facebook.com/ilovearistino
                                <br>
                                - Landing page: https://www.aristino.com/bethemen.
                                <li>
                                    Người dự thi chỉ tham gia DUY NHẤT 01 vòng thi trực tuyến (online).
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div id="giaithuong">
                        <span1 class="title">Hệ thống giải thưởng</span1>
                        <div class="content-detail">
                            <ul>
                                <li><span>02 GIẢI NHẤT</span> mỗi giải <b>20.000.000</b>vnđ TIỀN MẶT + 1 voucher mua
                                    hàng ARISTINO trị giá <b>1.000.000</b>vnđ
                                </li>
                                <li><b>05 giải nhì</b> mỗi giải 01 voucher trị giá <b>2.000.000</b>vnđ từ thương hiệu
                                    ARISTINO.
                                </li>
                                <li><b>10 giải ba</b> mỗi giải 01 voucher trị giá <b>500.000vnđ</b> từ ARISTINO.</li>
                                <li>Những người <b>đăng ký tham gia</b> đều được 01 voucher trị giá <b>200.000</b>vnđ từ
                                    ARISTINO.
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div id="du-thi">
                        <span1 class="title">Cách thức dự thi</span1>
                        <div class="content-detail">
                            <b>Cách thức tham gia</b>
                            <ul>
                                <li><label>Bước 1</label>: Tạo tài khoản trên website: http://aristino.com/betheman</li>
                                <li><label>Bước 2</label>: Vào mục ĐĂNG BÀI DỰ THI để đăng tải bài dự thi.</li>
                                Bài dự thi dưới dạng:<br>
                                + Video kèm miêu tả tối đa 500 từ và 2 ảnh.<br>
                                + Bài viết kèm ảnh (3 ảnh) nói về người đàn ông thực thụ của bạn.<br>
                                <li><label>Bước 3</label>: Chia sẻ bài dự thi của mình về trang Faceboook cá nhân ở chế
                                    độ Public kèm Hashtag #Aristino #BeTheMan #contestbetheman
                                </li>
                                <li><label>Bước 4</label>: Kêu gọi bạn bè vote bài dự thi của bạn trên trang landing
                                    page: http://aristino.com/betheman
                                </li>
                                <b>Tiêu chí đánh giá</b>
                                <li>30% điểm từ Ban Giám Khảo:</li>
                                + Thành phần BGK: đại diện thương hiệu ARISTINO<br>
                                + Tiêu chí: dựa trên chất lượng, sự đầu tư và nội dung clip nói về Người đàn ông thực
                                thụ của các bạn<br>
                                <li>70% điểm từ Khán giả: được xét từ lượng vote trên landing page của chuơng trình
                                    (http://aristino.com/betheman)
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div id="tieu-chi-danh-gia">
                        <span1 class="title">Tiêu chí đánh giá</span1>
                        <div class="content-detail">
                            <ul>
                                <li>30% điểm từ Ban Giám Khảo:</li>
                                + Thành phần BGK: đại diện thương hiệu ARISTINO<br>
                                + Tiêu chí: dựa trên chất lượng, sự đầu tư và nội dung clip nói về Người đàn ông thực
                                thụ của các bạn
                                <li>70% điểm từ Khán giả: được xét từ lượng vote trên landing page của chuơng trình
                                    (http://aristino.com/betheman)
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div id="qui-dinh">
                        <span1 class="title">Quy định chung</span1>
                        <div class="content-detail">
                            <ul>
                                <li>Định dạnh hình ảnh (JPEG, PNG,..dưới 5MB) hoặc Video (link Youtube)</li>
                                <li>BTC có toàn quyền sửa đổi và gỡ bỏ những tác phẩm có chứa nội dung nhạy cảm hoặc ảnh
                                    hưởng đến thuần phong mỹ tục và văn hoá Việt Nam. Nếu thí sinh đăng ảnh có nội dung
                                    không hợp lệ sẽ bị mất quyền tham dự cuộc thi mà không cần thông báo trước.
                                </li>
                                <li>BTC có toàn quyền sử dụng hình ảnh, tên tuổi của thí sinh cũng như Tác phẩm dự thi
                                    (nội dung và hình ảnh/video) trên các phương tiện truyền thông mà không phải trả bất
                                    kì khoản phí nào liên quan đến quyền tác giả cho người dự thi.
                                </li>
                                <li>BTC có quyền sửa đổi nội dung chương trình và sẽ thông báo trước trên trang Facebook
                                    ARISTINO Việt Nam (https://www.facebook.com/ilovearistino)
                                </li>
                                <li>Hình ảnh không được vi phạm thuần phong mỹ tục Việt Nam</li>
                                <li>Các clip tham gia dự thi đều phải thuộc quyền sở hữu của cá nhân hoặc nhóm thí sinh
                                    tham gia dự thi. Tất cả những bài dự thi có kiện cáo tranh chấp về bản quyền sẽ
                                    không được Ban Giám Khảo đánh giá. Toàn bộ nội dung hình ảnh và âm nhạc sử dụng
                                    trong bài dự thi thí sinh chịu trách nhiệm hoàn toàn về bản quyền.
                                </li>
                                <li>Thí sinh đoạt giải phải đảm bảo tuân thủ thời gian và sự sắp xếp của BTC trong quá
                                    trình nhận giải.
                                </li>
                                <li>Thí sinh tham gia cần phải đọc và hiểu rõ thể lệ của chương trình và đồng ý tuân
                                    theo quy định bản thể lệ này.
                                </li>
                                <li>Trong trường hợp phát sinh tranh chấp, khiếu nại liên quan đến cuộc thi, BTC sẽ trực
                                    tiếp giải quyết và quyết định của BTC là kết quả cuối cùng.
                                </li>
                                <li>Người tham gia không được có thái độ, hành vi, lời nói tiêu cực về kết quả cuộc
                                    thi.
                                </li>
                                <li>Giải thưởng sẽ bị huỷ nếu trong thời gian quy định mà người trúng giải không liên hệ
                                    và hoàn tất thủ tục nhận giải.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="the-le-content-mobile">
        <div class="container">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#be-the-man2">Be the men</a>
                        </h4>
                    </div>
                    <div id="be-the-man2" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="content-detail">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                Thế nào là đàn ông thực thụ? Người đàn ông thực thụ có phải là một người đàn ông hoàn
                                hảo mà mọi chàng trai đều muốn trở thành và mọi cô gái đều khao khát sở hữu.
                                Ngừng mơ mộng và đừng tìm kiếm đâu xa bởi chúng tôi nhìn thấy người đàn ông thực thụ
                                đang ở ngay bên cạnh bạn. Đó chính là người cha, người anh trai, em trai, anh người yêu,
                                thằng bạn thân, …hay một người đàn ông xa lạ nào đó đã giúp đỡ bạn hoặc giúp đỡ người
                                khác mà bạn chứng kiến. Họ không hoàn hảo nhưng họ có cái “chất” của một người đàn ông
                                thực thụ theo cách riêng của họ. Những hành động yêu thương, chăm sóc hay giúp đỡ người
                                khác chính là những hiện hữu của một “be the man”.
                                Hãy cùng ARISTINO tôn vinh họ. Tham gia và đồng hành cùng cuộc thi lớn nhất và duy nhất
                                trong năm của ARISTINO - <span>“BE THE MAN”</span></div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#noi-dung-cuoc-thi2">Nội dung cuộc
                            thi</a>
                    </h4>
                </div>
                <div id="noi-dung-cuoc-thi2" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="content-detail">
                            Chia sẻ những cảm xúc, những kỷ niệm hoặc nêu quan điểm về người đàn ông thực thụ của bạn
                            bằng 2 hình thức: Video và Hình ảnh kèm bài viết.
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#doi-tuong-du-thi2">Đối tượng dự thi</a>
                    </h4>
                </div>
                <div id="doi-tuong-du-thi2" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="content-detail">
                            <ul>
                                <li>
                                    Tất cả công dân Việt Nam từ 18 tuổi trở lên hiện đang sinh sống và làm việc tại Việt
                                    Nam.
                                </li>
                                <li>
                                    Chấp nhận các yêu cầu của chương trình.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#the-le-chung-2">Thể lệ chung</a>
                    </h4>
                </div>
                <div id="the-le-chung-2" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="content-detail">
                            <ul>
                                <li>
                                    Thời gian đăng tải bài dự thi và bình chọn: từ ngày 08/07/2013 đến hết ngày
                                    30/07/2017.
                                </li>
                                <li>
                                    Kết quả sẽ được công bố vào ngày 10-15/08/2017 tại:
                                </li>
                                - Fanpage của ARISTINO Việt Nam https://www.facebook.com/ilovearistino
                                <br>
                                - Landing page: https://www.aristino.com/bethemen.
                                <li>
                                    Người dự thi chỉ tham gia DUY NHẤT 01 vòng thi trực tuyến (online).
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#he-thong-giai-thuong2">Hệ thống giải thưởng</a>
                    </h4>
                </div>
                <div id="he-thong-giai-thuong2" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="content-detail">
                            <ul>
                                <li><span>02 GIẢI NHẤT</span> mỗi giải <b>20.000.000</b>vnđ TIỀN MẶT + 1 voucher mua
                                    hàng ARISTINO trị giá <b>1.000.000</b>vnđ
                                </li>
                                <li><b>05 giải nhì</b> mỗi giải 01 voucher trị giá <b>2.000.000</b>vnđ từ thương hiệu
                                    ARISTINO.
                                </li>
                                <li><b>10 giải ba</b> mỗi giải 01 voucher trị giá <b>500.000vnđ</b> từ ARISTINO.</li>
                                <li>Những người <b>đăng ký tham gia</b> đều được 01 voucher trị giá <b>200.000</b>vnđ từ
                                    ARISTINO.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#cach-thuc-du-thi-2">Cách thức dự thi</a>
                    </h4>
                </div>
                <div id="cach-thuc-du-thi-2" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="content-detail">
                            <b>Cách thức tham gia</b>
                            <ul>
                                <li><label>Bước 1</label>: Tạo tài khoản trên website: http://aristino.com/betheman</li>
                                <li><label>Bước 2</label>: Vào mục ĐĂNG BÀI DỰ THI để đăng tải bài dự thi.</li>
                                Bài dự thi dưới dạng:<br>
                                + Video kèm miêu tả tối đa 500 từ và 2 ảnh.<br>
                                + Bài viết kèm ảnh (3 ảnh) nói về người đàn ông thực thụ của bạn.<br>
                                <li><label>Bước 3</label>: Chia sẻ bài dự thi của mình về trang Faceboook cá nhân ở chế
                                    độ Public kèm Hashtag #Aristino #BeTheMan #contestbetheman
                                </li>
                                <li><label>Bước 4</label>: Kêu gọi bạn bè vote bài dự thi của bạn trên trang landing
                                    page: http://aristino.com/betheman
                                </li>
                                <b>Tiêu chí đánh giá</b>
                                <li>30% điểm từ Ban Giám Khảo:</li>
                                + Thành phần BGK: đại diện thương hiệu ARISTINO<br>
                                + Tiêu chí: dựa trên chất lượng, sự đầu tư và nội dung clip nói về Người đàn ông thực
                                thụ của các bạn<br>
                                <li>70% điểm từ Khán giả: được xét từ lượng vote trên landing page của chuơng trình
                                    (http://aristino.com/betheman)
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#tieu-chi-danh-gia2">Tiêu chi đánh
                            giá</a>
                    </h4>
                </div>
                <div id="tieu-chi-danh-gia2" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="content-detail">
                            <ul>
                                <li>30% điểm từ Ban Giám Khảo:</li>
                                + Thành phần BGK: đại diện thương hiệu ARISTINO<br>
                                + Tiêu chí: dựa trên chất lượng, sự đầu tư và nội dung clip nói về Người đàn ông thực
                                thụ của các bạn
                                <li>70% điểm từ Khán giả: được xét từ lượng vote trên landing page của chuơng trình
                                    (http://aristino.com/betheman)
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#quy-dinh-chung">Quy định chung</a>
                    </h4>
                </div>
                <div id="quy-dinh-chung" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="content-detail">
                            <ul>
                                <li>Định dạnh hình ảnh (JPEG, PNG,..dưới 5MB) hoặc Video (link Youtube)</li>
                                <li>BTC có toàn quyền sửa đổi và gỡ bỏ những tác phẩm có chứa nội dung nhạy cảm hoặc ảnh
                                    hưởng đến thuần phong mỹ tục và văn hoá Việt Nam. Nếu thí sinh đăng ảnh có nội dung
                                    không hợp lệ sẽ bị mất quyền tham dự cuộc thi mà không cần thông báo trước.
                                </li>
                                <li>BTC có toàn quyền sử dụng hình ảnh, tên tuổi của thí sinh cũng như Tác phẩm dự thi
                                    (nội dung và hình ảnh/video) trên các phương tiện truyền thông mà không phải trả bất
                                    kì khoản phí nào liên quan đến quyền tác giả cho người dự thi.
                                </li>
                                <li>BTC có quyền sửa đổi nội dung chương trình và sẽ thông báo trước trên trang Facebook
                                    ARISTINO Việt Nam (https://www.facebook.com/ilovearistino)
                                </li>
                                <li>Hình ảnh không được vi phạm thuần phong mỹ tục Việt Nam</li>
                                <li>Các clip tham gia dự thi đều phải thuộc quyền sở hữu của cá nhân hoặc nhóm thí sinh
                                    tham gia dự thi. Tất cả những bài dự thi có kiện cáo tranh chấp về bản quyền sẽ
                                    không được Ban Giám Khảo đánh giá. Toàn bộ nội dung hình ảnh và âm nhạc sử dụng
                                    trong bài dự thi thí sinh chịu trách nhiệm hoàn toàn về bản quyền.
                                </li>
                                <li>Thí sinh đoạt giải phải đảm bảo tuân thủ thời gian và sự sắp xếp của BTC trong quá
                                    trình nhận giải.
                                </li>
                                <li>Thí sinh tham gia cần phải đọc và hiểu rõ thể lệ của chương trình và đồng ý tuân
                                    theo quy định bản thể lệ này.
                                </li>
                                <li>Trong trường hợp phát sinh tranh chấp, khiếu nại liên quan đến cuộc thi, BTC sẽ trực
                                    tiếp giải quyết và quyết định của BTC là kết quả cuối cùng.
                                </li>
                                <li>Người tham gia không được có thái độ, hành vi, lời nói tiêu cực về kết quả cuộc
                                    thi.
                                </li>
                                <li>Giải thưởng sẽ bị huỷ nếu trong thời gian quy định mà người trúng giải không liên hệ
                                    và hoàn tất thủ tục nhận giải.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php include_once '../partition/footer.php'; ?>
<!-- /.Footer -->

<script type="text/javascript" src="../js/jquery/jquery.min.js"></script>
<script type="text/javascript" src="../css/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/slickjs/slick/slick.min.js"></script>
<script type="text/javascript" src="../js/script.js"></script>
</body>
</html>