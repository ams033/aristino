<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Aristino | Bài dự thi</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../js/slickjs/slick/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="../js/slickjs/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="../js/slickjs/slick/slick-custom.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
    <div id="main" class="container-fluid">
        <!-- header -->
        <?php include_once '../partition/header.php' ?>
        <!-- /.header -->

        <!-- slider -->
        <?php include_once '../partition/slider.php' ?>
        <!-- /.slider -->
        <!-- button sign up -->
        <div class="row">
            <a href="#" class="btn btn-register center-block"><span>ĐĂNG BÀI DỰ THI</span></a>
        </div>
        <!-- /.button sign up -->
        <!--Bài dự thi-->
        <div id="bai-du-thi">
            <div class="container">
                <div class="row title-exam">
                    <label>Be the men</label>
                </div>
                <div class="row">
                    <p class="cam-nhan text-center">“Anh ấy trong tôi là một con người không phải hoàn hảo mà là một sự khuyết điểm hoàn hảo để trở thành người đàn ông của đời tôi... ”</p>
                </div>
                <div id="video-cam-nhan">
                    <div class="row">
                        <div class="embed-responsive embed-responsive-16by9 video">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/CFqD14ybRr0"></iframe>
                        </div>
                    </div>
                </div>
                <div class="content-exam">
                    <div class="row content-1">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <div class="img">
                                <img src="../images/banner1.png">
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <div class="text">
                                <p class="big-text">LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISCING ELIT.
                                    QUAE CUM MAGNIFICE PRIMO DICI VIDERENTUR, CONSIDERATA</p>

                                minus probabantur. Aliud est enim poëtarum more verba fundere, aliud ea, quae dicas, ratione et arte distinguere. Duo Reges: constructio interrete. Hoc mihi cum tuo fratre convenit. Conferam avum tuum Drusum cum C. Ita fit cum gravior, tum etiam splendidior oratio. Illis videtur, qui illud non dubitant bonum dicere -; Non potes, nisi retexueris illa. Si longus, levis dictata sunt. Ego vero volo in virtute vim esse quam maximam;

                                Quo modo autem optimum, si bonum praeterea nullum est? Peccata paria. Negare non possum. Quarum ambarum rerum cum medicinam pollicetur, luxuriae licentiam pollicetur. Ne in odium veniam, si amicum destitero tueri. Ergo illi intellegunt quid Epicurus dicat, ego non intellego? Aliter homines, aliter philosophos loqui putas oportere? An est aliquid, quod te sua sponte delectet?

                                Nulla profecto est, quin suam vim retineat a primo ad extremum. Non autem hoc: igitur ne illud quidem. Intellegi quidem, ut propter aliam quampiam rem, verbi gratia propter voluptatem, nos amemus; Contineo me ab exemplis. Bestiarum vero nullum iudicium puto.

                                Sed mehercule pergrata mihi oratio tua. Cur igitur, cum de re conveniat, non malumus usitate loqui? Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur? Multoque hoc melius nos veriusque quam Stoici. Naturales divitias dixit parabiles esse, quod parvo esset natura contenta. Non autem hoc: igitur ne illud quidem. Nondum autem explanatum satis, erat, quid maxime natura vellet.

                                Sed in rebus apertissimis nimium longi sumus. Apparet statim, quae sint officia, quae actiones. At certe gravius. Quid igitur dubitamus in tota eius natura quaerere quid sit effectum? Qui non moveatur et offensione turpitudinis et comprobatione honestatis? Quid turpius quam sapientis vitam ex insipientium sermone pendere? Bonum negas esse divitias, praeposìtum esse dicis? Illum mallem levares, quo optimum atque humanissimum virum, Cn. In schola desinis. Nam quid possumus facere melius? Illud dico, ea, quae dicat, praeclare inter se cohaerere. Sic vester sapiens magno aliquo emolumento commotus cicuta, si opus erit, dimicabit.
                            </div>
                        </div>
                    </div>
                    <div class="row content-2">
                        <div class="col-md-12 col-xs-12 col-sm-12 img">
                            <img src="../images/banner2.png">
                        </div>
                        <div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8 col-xs-offset-0 col-xs-12 text">
                            minus probabantur. Aliud est enim poëtarum more verba fundere, aliud ea, quae dicas, ratione et arte distinguere. Duo Reges: constructio interrete. Hoc mihi cum tuo fratre convenit. Conferam avum tuum Drusum cum C. Ita fit cum gravior, tum etiam splendidior oratio. Illis videtur, qui illud non dubitant bonum dicere -; Non potes, nisi retexueris illa. Si longus, levis dictata sunt. Ego vero volo in virtute vim esse quam maximam;

                            Quo modo autem optimum, si bonum praeterea nullum est? Peccata paria. Negare non possum. Quarum ambarum rerum cum medicinam pollicetur, luxuriae licentiam pollicetur. Ne in odium veniam, si amicum destitero tueri. Ergo illi intellegunt quid Epicurus dicat, ego non intellego? Aliter homines, aliter philosophos loqui putas oportere? An est aliquid, quod te sua sponte delectet?

                            Nulla profecto est, quin suam vim retineat a primo ad extremum. Non autem hoc: igitur ne illud quidem. Intellegi quidem, ut propter aliam quampiam rem, verbi gratia propter voluptatem, nos amemus; Contineo me ab exemplis. Bestiarum vero nullum iudicium puto.

                            Sed mehercule pergrata mihi oratio tua. Cur igitur, cum de re conveniat, non malumus usitate loqui? Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur? Multoque hoc melius nos veriusque quam Stoici. Naturales divitias dixit parabiles esse, quod parvo esset natura contenta. Non autem hoc: igitur ne illud quidem. Nondum autem explanatum satis, erat, quid maxime natura vellet.

                            Sed in rebus apertissimis nimium longi sumus. Apparet statim, quae sint officia, quae actiones. At certe gravius. Quid igitur dubitamus in tota eius natura quaerere quid sit effectum? Qui non moveatur et offensione turpitudinis et comprobatione honestatis? Quid turpius quam sapientis vitam ex insipientium sermone pendere? Bonum negas esse divitias, praeposìtum esse dicis? Illum mallem levares, quo optimum atque humanissimum virum, Cn. In schola desinis. Nam quid possumus facere melius? Illud dico, ea, quae dicat, praeclare inter se cohaerere. Sic vester sapiens magno aliquo emolumento commotus cicuta, si opus erit, dimicabit.
                        </div>
                    </div>

                    <div class="row name">
                        <div class="row title-name">
                            <label>Nguyễn Văn A</label>
                        </div>
                    </div>

                    <div class="row thong-ke">
                        <div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-offset-0 col-xs-12">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="icon">
                                    <img src="../images/icon-lien-ket.png">
                                </div>
                                <div class="number-thong-ke">
                                    67000
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="icon">
                                    <img src= "../images/icon-heart.png">
                                </div>
                                <div class="number-thong-ke">
                                    547000
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.Bài dự thi-->
        <!-- scroll to top -->
        <div id="scroll_to_top">
            <a href="#top" class="scrolltotop">
                <img src="../images/btn-top.jpg" alt="Scroll to top">
            </a>
        </div>
        <!-- /. scroll to top -->
        <!-- Footer -->
        <?php include_once '../partition/footer.php'; ?>
        <!-- /.Footer -->
    </div>

    <script type="text/javascript" src="../js/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../css/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/slickjs/slick/slick.min.js"></script>
    <script type="text/javascript" src="../js/script.js"></script>
</body>
</html>