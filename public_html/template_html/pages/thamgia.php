<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Aristino | Tham gia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../js/slickjs/slick/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="../js/slickjs/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="../js/slickjs/slick/slick-custom.css">
    <link rel="stylesheet" type="text/css" href="../inc/crop-avatar/css/cropper.css">
    <link rel="stylesheet" type="text/css" href="../inc/crop-avatar/css/main.css">
    <link rel="stylesheet" type="text/css" href="../inc/mediaelement/css/mediaelementplayer.min.css" />
    <link rel="stylesheet" type="text/css" href="../css/style.css">

</head>
<body>
<div id="main" class="container-fluid">
    <!-- header -->
    <?php include_once '../partition/header.php' ?>
    <!-- /.header -->

    <!-- slider -->
    <?php include_once '../partition/slider.php' ?>
    <!-- /.slider -->
    <!-- button sign up -->
    <div class="row">
        <a href="#" class="btn btn-register center-block"><span>ĐĂNG BÀI DỰ THI</span></a>
    </div>
    <!-- /.button sign up -->
    <!--Bài dự thi-->
    <div id="bai-du-thi">
        <div class="container">
            <form action="" method="">
                <div class="row title-exam">
                    <!-- titile -->
                    <label class="setTitle" data-toggle="modal" data-target="#input_title">Tiêu đề của bạn abc</label>
                    <input type="hidden" name="txtTitle" id="setTitle">
                    <div class="modal fade" id="input_title" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h2 class="modal-title">Nhập tiêu đề của bạn</h2>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="5" id="title_content"
                                                  placeholder="Tiêu đề không vượt quá 60 ký tự"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <p class="has-error text-left"></p>
                                    </div>
                                    <div>
                                        <button type="button" class="btn btn-primary btn-insert" id="btn_insert_title">
                                            Thêm
                                        </button>
                                        <button type="button" class="btn btn-default btn-close" data-dismiss="modal">
                                            Đóng
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.title -->

                    <!-- cảm nhận ngắn -->
                    <p class="_feelShort"  data-toggle="modal" data-target="#input_feel_short">Viết cảm nhận ngắn của bạn
                        tại đây (tối đa 50 từ ~ 3 câu) abc</p>
                    <input type="hidden" name="txtFeelShort" id="set_feel_short">
                    <div class="modal fade" id="input_feel_short" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h2 class="modal-title">Cảm nhận ngắn của bạn</h2>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="5" id="feel_short_content"
                                                  placeholder="Cảm nhận của bạn tối đa 50 từ ~ 3 câu."></textarea>
                                    </div>
                                    <div class="form-group">
                                        <p class="has-error text-left"></p>
                                    </div>
                                    <div>
                                        <button type="button" class="btn btn-primary btn-insert"
                                                id="btn_insert_feel_short">Thêm
                                        </button>
                                        <button type="button" class="btn btn-default btn-close" data-dismiss="modal">
                                            Đóng
                                        </button>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <!-- /. cảm nhận ngắn -->
                </div>
                <div id="video-cam-nhan">
                    <div class="row" >
                        <div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">

                            <div class=" khung-video" id="khung_img_video">
                                <img src="../images/khung-video.jpg" alt="" class="center-block" id="img_video"  data-toggle="modal" data-target="#select_img_video">

                            </div>
                        </div>

                    </div>


                    <div class="modal fade" id="select_img_video" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content center">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h2 class="modal-title text-center">Lựa chọn của bạn:</h2>
                                </div>
                                <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a id="btn_upload_img3">
                                                    <i class="fa fa-file-image-o" aria-hidden="true"></i>&nbsp;Image
                                                </a>
                                            </div>
                                            <div class="col-md-6">
                                                <a id="btn_upload_video" data-toggle="modal" data-target="#insert_link_video">
                                                    <i class="fa fa-file-video-o" aria-hidden="true"></i>&nbsp;Video
                                                </a>
                                            </div>
                                        </div>

                                </div>
                                <div class="modal-footer center">

                                    <button type="button" class="btn btn-default btn-close-img3" data-dismiss="modal">
                                        Đóng
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-exam">
                    <div class="row content-1">
                        <div class="col-md-offset-1 col-md-5 col-sm-5 col-xs-12">
                            <a class="img" id="btn_upload_img1">
                                <img src="../images/khung-1.png">
                            </a>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <div class="text">
                                <p class="_feelShort big-text-khung-anh" data-toggle="modal"
                                   data-target="#input_feel_short">VIẾT CẢM NHẬN NGẮN CỦA BẠN TẠI ĐÂY (TỐI ĐA 50 TỪ ~ 3
                                    CÂU)</p>
                                <!-- đăng bài viết - part1 -->
                                <div class="input-content" data-toggle="modal" data-target="#input_post">
                                    <p class="_input_post" id="post1">Đăng bài viết của bạn (tối đa 500 từ)</p>
                                </div>
                                <input type="hidden" name="txtPost" id="set_post">
                                <div class="modal fade" id="input_post" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close"
                                                        data-dismiss="modal">&times;</button>
                                                <h2 class="modal-title text-center">Bài viết của bạn</h2>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="15" id="post_content"
                                                              placeholder="Bài viết của bạn tối đa 500 từ"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <p class="has-error text-left"></p>
                                                </div>
                                                <div class="text-center">
                                                    <button type="button" class="btn btn-primary btn-insert"
                                                            id="btn_insert_post">Thêm
                                                    </button>
                                                    <button type="button" class="btn btn-default btn-close"
                                                            data-dismiss="modal">Đóng
                                                    </button>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <!-- /.đăng bài viết - part 1 -->
                            </div>
                        </div>
                    </div>
                    <div class="row content-2">
                        <div class="col-md-12 col-xs-12 col-sm-12 img">
                            <a id="btn_upload_img2">
                                <img src="../images/khung-2.png">
                            </a>
                        </div>
                        <!-- đăng bài viết - part 2 -->
                        <div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8 col-xs-offset-0 col-xs-12 text">
                            <p class="_input_post" id="post2"></p>
                        </div>
                        <!-- đăng bài viết - part 2 -->
                    </div>
                    <div class="row name">
                        <input type="submit" class="btn btn-upload" value="ĐĂNG BÀI">
                    </div>
                </div>
            </form>

        </div>
    </div>
    <!-- /.Bài dự thi-->

    <!-- scroll to top -->
    <div id="scroll_to_top">
        <a href="#top" class="scrolltotop">
            <img src="../images/btn-top.jpg" alt="Scroll to top">
        </a>
    </div>
    <!-- /. scroll to top -->

    <!-- Footer -->
    <?php include_once '../partition/footer.php'; ?>
    <!-- /.Footer -->

    <!-- crop-avarta -->
    <div id="crop-avatar">
        <!-- Current avatar -->
        <div class="avatar-view" style="display:none">
        </div>

        <!-- Cropping modal -->
        <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog"
             tabindex="-1">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form class="avatar-form" action="http://betheman.aristino.com/template_html/assets/crop1.php"
                          type="multipart/form-data"
                          method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="avatar-modal-label">Tài ảnh dự thi</h4>
                        </div>
                        <div class="modal-body">
                            <div class="avatar-body">

                                <!-- Upload image and data -->
                                <div class="avatar-upload">
                                    <input type="hidden" class="avatar-src" name="avatar_src">
                                    <input type="hidden" class="avatar-data" name="avatar_data">
                                    <label for="avatarInput">Chọn ảnh</label>
                                    <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
                                </div>

                                <!-- Crop and preview -->

                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="avatar-wrapper"></div>
                                    </div>
                                    <div class="col-md-3" id="div-avatar-preview">
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <h4>Xem thử</h4>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="avatar-preview preview-lg"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row avatar-btns">
                                    <div class="col-xs-8 col-sm-9">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="-90" title="Rotate -90 degrees">Xoay trái
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="90" title="Rotate 90 degrees">Xoay phải
                                            </button>
                                        </div>

                                        <div class="btn-group" style="display: none">
                                            <div class="avatar-preview preview-md"></div>
                                            <div class="avatar-preview preview-sm"></div>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="15">15deg
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="30">30deg
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="45">45deg
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="-15">-15deg
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="-30">-30deg
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="-45">-45deg
                                            </button>
                                        </div>
                                    </div>

                                    <div class="col-xs-4  col-sm-3">
                                        <button type="submit" class="btn btn-primary btn-block avatar-save">Hoàn tất
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div> -->
                    </form>
                </div>
            </div>
        </div><!-- /.modal -->
        <!-- Loading state -->
        <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
    </div>
    <!-- /.crop-avarta -->

    <!-- crop-avarta2 -->
    <div id="crop-avatar2">
        <!-- Current avatar -->
        <div class="avatar-view2" style="display:none">
        </div>

        <!-- Cropping modal -->
        <div class="modal fade" id="avatar-modal2" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog"
             tabindex="-1">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form class="avatar-form2" action="http://betheman.aristino.com/template_html/assets/crop2.php"
                          type="multipart/form-data"
                          method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="avatar-modal-label2">Tài ảnh dự thi</h4>
                        </div>
                        <div class="modal-body">
                            <div class="avatar-body">

                                <!-- Upload image and data -->
                                <div class="avatar-upload2">
                                    <input type="hidden" class="avatar-src2" name="avatar_src2">
                                    <input type="hidden" class="avatar-data2" name="avatar_data2">
                                    <label for="avatarInput2">Chọn ảnh</label>
                                    <input type="file" class="avatar-input2" id="avatarInput2" name="avatar_file2">
                                </div>

                                <!-- Crop and preview -->

                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="avatar-wrapper2"></div>
                                    </div>
                                    <div class="col-md-3" id="div-avatar-preview2">
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <h4>Xem thử</h4>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="avatar-preview2 preview-lg"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row avatar-btns2">
                                    <div class="col-xs-8 col-sm-9">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="-90" title="Rotate -90 degrees">Xoay trái
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="90" title="Rotate 90 degrees">Xoay phải
                                            </button>
                                        </div>

                                        <div class="btn-group" style="display: none">
                                            <div class="avatar-preview preview-md"></div>
                                            <div class="avatar-preview preview-sm"></div>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="15">15deg
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="30">30deg
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="45">45deg
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="-15">-15deg
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="-30">-30deg
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="-45">-45deg
                                            </button>
                                        </div>
                                    </div>

                                    <div class="col-xs-4  col-sm-3">
                                        <button type="submit" class="btn btn-primary btn-block avatar-save3">Hoàn tất
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div> -->
                    </form>
                </div>
            </div>
        </div><!-- /.modal -->
        <!-- Loading state -->
        <div class="loading2" aria-label="Loading" role="img" tabindex="-1"></div>
    </div>
    <!-- /.crop-avarta -->

    <!-- crop-avarta3 -->
    <div id="crop-avatar3">
        <!-- Current avatar -->
        <div class="avatar-view3" style="display:none">
        </div>

        <!-- Cropping modal -->
        <div class="modal fade" id="avatar-modal3" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog"
             tabindex="-1">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form class="avatar-form3" action="http://betheman.aristino.com/template_html/assets/crop3.php"
                          type="multipart/form-data"
                          method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="avatar-modal-label3">Tài ảnh dự thi</h4>
                        </div>
                        <div class="modal-body">
                            <div class="avatar-body">

                                <!-- Upload image and data -->
                                <div class="avatar-upload3">
                                    <input type="hidden" class="avatar-src3" name="avatar_src3">
                                    <input type="hidden" class="avatar-data3" name="avatar_data3">
                                    <label for="avatarInput3">Chọn ảnh</label>
                                    <input type="file" class="avatar-input3" id="avatarInput3" name="avatar_file3">
                                </div>

                                <!-- Crop and preview -->
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="avatar-wrapper3"></div>
                                    </div>
                                    <div class="col-md-3" id="div-avatar-preview3">
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <h4>Xem thử</h4>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="avatar-preview3 preview-lg"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row avatar-btns3">
                                    <div class="col-xs-8 col-sm-9">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="-90" title="Rotate -90 degrees">Xoay trái
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="90" title="Rotate 90 degrees">Xoay phải
                                            </button>
                                        </div>

                                        <div class="btn-group" style="display: none">
                                            <div class="avatar-preview preview-md"></div>
                                            <div class="avatar-preview preview-sm"></div>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="15">15deg
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="30">30deg
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="45">45deg
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="-15">-15deg
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="-30">-30deg
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="rotate"
                                                    data-option="-45">-45deg
                                            </button>
                                        </div>
                                    </div>

                                    <div class="col-xs-4  col-sm-3">
                                        <button type="submit" class="btn btn-primary btn-block avatar-save3">Hoàn tất
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div> -->
                    </form>
                </div>
            </div>
        </div><!-- /.modal -->
        <!-- Loading state -->
        <div class="loading3" aria-label="Loading" role="img" tabindex="-1"></div>
    </div>
    <!-- /.crop-avarta3 -->

    <!-- insert_video_youtube -->
    <div class="modal fade" id="insert_link_video" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content center">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title text-center">Vui lòng nhập vào link video của bạn:</h2>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" value="" placeholder="https://youtube.com" id="video_link">
                    </div>
                    <div class="has-error text-left"></div>
                </div>
                <div class="modal-footer center">
                    <button type="button" class="btn btn-primary btn-insert"
                            id="btn_insert_link_video">Thêm
                    </button>
                    <button type="button" class="btn btn-default btn-close-link-video" data-dismiss="modal">
                        Đóng
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.insert_video_youtube -->
</div>

<script type="text/javascript" src="../js/jquery/jquery.min.js"></script>
<script type="text/javascript" src="../css/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/jquery/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="../js/slickjs/slick/slick.min.js"></script>
<script type="text/javascript" src="../inc/crop-avatar/js/cropper.js"></script>
<script type="text/javascript" src="../inc/crop-avatar/js/main.js"></script>
<script type="text/javascript" src="../inc/crop-avatar/js/main2.js"></script>
<script type="text/javascript" src="../inc/crop-avatar/js/main3.js"></script>
<script type="text/javascript" src="../inc/mediaelement/js/mediaelement-and-player.min.js"></script>
<script type="text/javascript" src="../js/script.js"></script>
</body>
</html>