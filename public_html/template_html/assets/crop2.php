<?php
include_once 'CropAvatar.php';

$crop2 = new CropAvatar(
    isset($_POST['avatar_src2']) ? $_POST['avatar_src2'] : null,
    isset($_POST['avatar_data2']) ? $_POST['avatar_data2'] : null,
    isset($_FILES['avatar_file2']) ? $_FILES['avatar_file2'] : null
);


$response = array(
    'state'  => 200,
    'message' => $crop2 -> getMsg(),
    'filename' => $crop2 ->filename,
    'result' => $crop2 -> getResult()
);

echo json_encode($response);
