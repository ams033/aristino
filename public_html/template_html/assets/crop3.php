<?php
include_once 'CropAvatar.php';

$crop3 = new CropAvatar(
    isset($_POST['avatar_src3']) ? $_POST['avatar_src3'] : null,
    isset($_POST['avatar_data3']) ? $_POST['avatar_data3'] : null,
    isset($_FILES['avatar_file3']) ? $_FILES['avatar_file3'] : null
);


$response = array(
    'state'  => 200,
    'message' => $crop3 -> getMsg(),
    'filename' => $crop3 ->filename,
    'result' => $crop3 -> getResult()
);

echo json_encode($response);
